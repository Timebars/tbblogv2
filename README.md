# Jim Blog App - 

## Initial Details


### Prototype Front end Site URL

https://www.rlan.ca

This site "tbBlogV1" is based on indexedDB data storage. IndexedDB was used to prototype CRUD operations and site functionality and look and feel. 


### The "tbBlogV1" repo propritary code is at this URL. 

git clone https://Timebars@bitbucket.org/Timebars/tbblogv1.git




### Backend URL (Per code from Ahmed A deployed to Digital Ocean)

 https://api.rlan.ca


# Our Goal
```
I wish for this App - tbBlogV1 - to persist data based on Mongo db via my API backend in the cloud. 

```
# Details
There is a file called scripts/fetchpatterns.js, I use this in another app of mine to fetch data from a REST API (Oracle ORDS).
I also have older patterns, See the file scripts/userAccountLoginProfile.js, it has decent patterns for fetch opeerations,  I would like for you to uses these or for you to improve upon them. I dont want to use Axios.

The code you provide me would have to be readable and understandable to me this is why i have produced the patterns, nice and simple. 

Of course you can be creative and change what you need to to suit best practices but not crazy complex "one function do all" stuff. 

The work on this project will lead to another App afterwards that i am working on at https://www.timebars.com

# Mongo Instance in Digital Ocean

I am using MongoDB shell version v3.6.3
connecting to: mongodb://127.0.0.1:27017
MongoDB server version: 3.6.3
{
        "authInfo" : {
                "authenticatedUsers" : [ ],
                "authenticatedUserRoles" : [ ]
        },
        "ok" : 1
}

If you recommend an upgrade of Mongo, please let me know.


# Node Apps Running

NodeJS Prod Versions: v8.10.0

Run pm2 ls to get a list of apps running on the box.

root@TimebarsVM:/var/www/api.rlan.ca/config# pm2 ls
⇆  PM2+ activated | Web: https://app.pm2.io/#/r/atbo25bi2q6i20m | Server: TimebarsVM | Conn: Websocket
┌───────────────┬────┬─────────┬──────┬───────┬────────┬─────────┬────────┬─────┬───────────┬──────┬──────────┐
│ App name      │ id │ version │ mode │ pid   │ status │ restart │ uptime │ cpu │ mem       │ user │ watching │
├───────────────┼────┼─────────┼──────┼───────┼────────┼─────────┼────────┼─────┼───────────┼──────┼──────────┤
│ ApiRlanCaNode │ 1  │ 1.0.0   │ fork │ 5129  │ online │ 15      │ 18D    │ 0%  │ 84.6 MB   │ root │ disabled │
│ AppRlanCaNode │ 0  │ 1.0.0   │ fork │ 16010 │ online │ 1       │ 2M     │ 0%  │ 95.8 MB   │ root │ disabled │
└───────────────┴────┴─────────┴──────┴───────┴────────┴─────────┴────────┴─────┴───────────┴──────┴──────────┘




# pm2 Configuration and set up
cd /var/www/api.rlan.ca  note: this is where the sites files reside, Where you must start nodejs using pm2
Set up PM2 to watch api.rlan.ca and restart automatically on reboot etc
cd /var/www/api.rlan.ca
pm2 start server.js -n ApiRlanCaNode2 (do not use watch command)
confirm that your app started by running: pm2 list
To stop it enter pm2 stop id 
start our app on system startup. Run: pm2 startup ubuntu
Freeze a process list on reboot via: $ pm2 save
Now it says this: Writing init configuration in /etc/systemd/system/pm2-root.service
Restart nginx: sudo service nginx restart
Pm2 ls Note: our node apps are running now	

Troubleshooting
pm2 kill  (stopps all node apps)
Pm2 show (id) to see where logs are stored, then sudo nano log path
/root/.pm2/logs




# Reverse Proxy configurations

A Reverse Proxy with Nginx is set up already that maps https://api.rlan.ca to mongodb://127.0.0.1:27017
Therefore you can hit the Mongo db with a fetch call to https://api.rlan.ca
This is accomplished via /var/www/api.rlan.ca/config/config.env



# Materialize Theme

Envato Theme Forest https://themeforest.net/ 
This is the one i bought, the files are in the repo under ./app-assets
https://themeforest.net/item/materialize-material-design-admin-template/11446068

Local version
file:///home/jcox/Documents/MatlTheme/Feb2020Release/materialize-admin-template-7.0/index.html


# Deployment

Login to "TimebarsVM" From Linux VSCode Terminal to digital ocean
ssh -i /home/jcox/Sites/ssh/id_rsa root@159.203.28.178
cd /var/www the ls to see list of sites, we use:
api.rlan.ca and www.rlan.ca directories

It will be necessary to change and deploy code to both front end and back end as things change. I think you guys will need access to Digital Ocean to deploy the changes. I use FileZilla for this over SSH to deploy my code manually to both. I will provide the ssh key and passwords later. Maybe we can start with me doing the deployments, then switch to you guys taking care of it. You may want to automate deployment away from FileZilla, let me know.


# Typical REST call to API with REST Client

Note: all calls require https

@host = api.rlan.ca
@tbID = {{getAll.response.body.data[0]._id}}
@token = Bearer {{login.response.body.token}}

@name login
POST https://{{host}}/api/v1/auth/login/
Content-Type: application/json

{
  "email": "user@gmail.com",
  "password": "123456"
}





