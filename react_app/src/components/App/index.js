import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import { Header, LeftSidebar } from 'components/utils/';
import { Home, Articles, BlogGrid, ContactUs } from 'components/pages/';

export default function App() {
  return (
    <Router>
      <Header />
      <LeftSidebar />
      <div id='main'>
        <div className='row'>
          <div className='col s12'>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route exact path='/articles' component={Articles} />
              <Route exact path='/blog-grid' component={BlogGrid} />
              <Route exact path='/contact-us' component={ContactUs} />
              <Route path='*' render={() => <Redirect to='/' />} />
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}
