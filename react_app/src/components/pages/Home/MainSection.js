import React from 'react';

export default function HomePageMainSection() {
  return (
    <div className='tbHomePage'>
      <div className='parallax-container'>
        <div className='parallax'>
          <img
            src='images/TimebarsMainNewColors.png'
            alt='Unsplashed background img 1'
          />
        </div>
      </div>
      <div className='container'>
        <div className='section'>
          {/* <!--   Icon Section   --> */}
          <div className='row'>
            <div className='col s12 m4'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>flash_on</i>
                </h2>
                <h5 className='center'>What is it?</h5>
                <p className='light'>
                  Advanced browser based planning & scheduling tools with a
                  proper scheduling engine, designed to automate and simplify
                  planning and scheduling scenarios.
                </p>
                <p>
                  Our products fill the gaps left behind by ERP & PPM enterprise
                  systems and related desktop applications such as MS Project.
                </p>
                <h5>See 10 minute introductory presentation. </h5>
                <a href='https://youtu.be/BcZebcMJHjM' target='_blank' rel="noopener noreferrer">
                  Watch on Youtube!
                </a>
              </div>
            </div>
            <div className='col s12 m4'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>group</i>
                </h2>
                <h5 className='center'>What problems do we solve? </h5>

                <p className='light'>
                  Eliminates end user frustration caused by hidden rules baked
                  into legacy planning and scheduling software.
                </p>
                <p className='light'>
                  Saves you time: touch, drag, drop, to plan and schedule your
                  initiatives. Its a new approach, without forms to fill in,
                  with a scheduling engine calculating correct costs and hours.
                </p>
                <p className='light'>
                  Use your favourite spreadsheet program for data entry, but
                  schedule your work in Timebars.
                </p>

                <p className='light'>
                  You drag bars on the timescale and when you drop them,the
                  scheduling engine calculates actual & forecast dates, actual &
                  forecast work, actual & forecst costs, and then calculates
                  resource supply and demand.
                </p>

                <p className='light'>
                  Timebars will send your scheduling results back to the
                  spreadsheet where you gain back control of your data.
                </p>
              </div>
            </div>

            <div className='col s12 m4'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>settings</i>
                </h2>
                <h5 className='center'>No software to install!</h5>
                <p className='light'>
                  <strong>Do not depend on the IT Department!</strong>
                  <br />
                  Become instantly productive with our tools without training
                  and without IT support.
                  <br />
                  Timebars is a web page, its as simple to use as use as paying
                  a bill on your banking web site.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className='parallax-container valign-wrapper'>
        <div className='section no-pad-bot'>
          <div className='container'>
            <div className='row center'>
              <h5 className='header col s12 light'>A modern Scheduling Tool</h5>
            </div>
          </div>
        </div>
        <div className='parallax'>
          <img
            src='images/background21.jpg'
            alt='Unsplashed background img 2'
          />
        </div>
      </div>
      <div className='container'>
        <div className='section'>
          {/* <!-- jim start 1 --> */}
          {/* <!--   Icon Section   --> */}
          <div className='row'>
            <div className='col s6'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>fiber_new</i>
                </h2>
                <h5 className='center'>Try Timebars!</h5>

                <p className='center light'>
                  We make planning and scheduling fun again with dramatic
                  productivy improvements!
                </p>

                <p className='center dark'>
                  Try the public version free!{' '}
                  <a href='https://www.timebars.com/GettingStarted.html'>
                    {' '}
                    www.timebars.com
                  </a>
                </p>
              </div>
            </div>

            <div className='col s6'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>flag</i>
                </h2>
                <h5 className='center'>
                  Digital transformation is taking too long!
                </h5>
                <p className='center light'>
                  {' '}
                  Speed up with Timebars Ltd, project controls specialists and
                  with
                  <a href='http://creativeis.ca/'>Creative IS</a>, visionary
                  software developers.
                </p>
                {/* <!-- <h4 className="center">Contact Us now!</h4> --> */}

                <div className='launchContactUs waves-effect waves-light btn-large'>
                  Contact Us now!
                  <i className='material-icons large'>arrow_forward</i>
                </div>
              </div>
            </div>
          </div>
          {/* <!-- jim 1 end --> */}
        </div>
      </div>
      {/* <!-- jim 2 start --> */}
      <div className='parallax-container valign-wrapper'>
        <div className='section no-pad-bot'>
          <div className='container'>
            <div className='row center'>
              {/* <!-- <h5 className="header col s12 light">A modern responsive front-end framework based on Material Design</h5> --> */}
            </div>
          </div>
        </div>
        {/* <!-- <div className="parallax"><img src="images/background31.jpg" alt="Unsplashed background img 3"></div> --> */}
        <div className='parallax'>
          <img
            src='images/TimebarsMainNewColorsMedium.png'
            alt='Unsplashed background img 3'
          />
        </div>
      </div>
      <div className='container'>
        <div className='section'>
          {/* <!--   Icon Section   --> */}
          <div className='row'>
            <div className='col s12 m4'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>timeline</i>
                </h2>
                <h5 className='center'>Resource Planner & Scheduler </h5>

                <p className='light'>
                  This tool is for you if: You currently use a Spreadsheet to
                  track your people. You wish it could figure out who is over
                  allocated, who can take on more work, and advise if need to
                  hire.{' '}
                </p>
                <p>
                  You are a manager of people, the organization has demand for
                  their talent and you are unsure of their availability for
                  committments to the demand. You wish or have the need to
                  visualize this demand on a dynamic timescale and perform what
                  if scenarios.
                </p>
              </div>
            </div>

            <div className='col s12 m4'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>timelapse</i>
                </h2>
                <h5 className='center'>Sprint Planner & Scheduler</h5>

                <p className='light'>
                  This tool is for you if: Your organization invested heavily in
                  ERP, PPM and ticketing systems to manage work. You know Agile
                  development is the way forward for you to manage work and you
                  are stuck,your Teams productivity is down. You feel there must
                  be a simple tool to pull in a backlog, plan a sprint, track
                  progress and produce burn-down charts from data in these
                  systems.
                </p>
              </div>
            </div>

            <div className='col s12 m4'>
              <div className='icon-block'>
                <h2 className='center black-text'>
                  <i className='material-icons'>access_time</i>
                </h2>
                <h5 className='center'>Agile Kanban Board</h5>
                <p className='light'>
                  You are managing several smaller initiatives and have adhoc
                  stand up meetings with the team using a white board but would
                  like to begin using a software approach. You would like to
                  swap out your methodology mid stream. You wish you could
                  change between Kanban, Sprints or Waterfall seamlessly in one
                  tool.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <!-- jim 3 end --> */}
      <div className='parallax-container valign-wrapper'>
        <div className='section no-pad-bot'>
          <div className='container'>
            <div className='row center'>
              {/* <!-- <h5 className="header col s12 light">A modern responsive front-end framework based on Material Design</h5> --> */}
            </div>
          </div>
        </div>
        <div className='parallax'>
          <img
            src='images/background31.jpg'
            alt='Unsplashed background img 3'
          />
        </div>
      </div>
    </div>
  );
}
