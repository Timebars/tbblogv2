import React from 'react';

import { BreadCrump } from 'components/utils/';
import MainSection from './MainSection';

export default function Home() {
  return (
    <React.Fragment>
      <BreadCrump title='Blog Home Page' l1='1' l2='2' l3='3' />
      <MainSection />
    </React.Fragment>
  );
}
