import React from 'react';

import Sidebar from './Sidebar';

export default function ContactUs() {
  return (
    // <!-- BEGIN: Page Main-->
    <div id='divContactMailPageL1'>
      <div className='row'>
        <div className='content-wrapper-before blue-grey lighten-5'></div>
        <div className='col s12'>
          <div className='container'>
            {/* <!-- Contact Us --> */}
            <div id='contact-us' className='section'>
              <div className='app-wrapper'>
                <div className='contact-header' style={{ marginTop: 10 }}>
                  <div className='row contact-us'>
                    <div className='col s12 m12 l4 sidebar-title'>
                      <h5 className='m-0'>
                        <i className='material-icons contact-icon vertical-text-top'>
                          mail_outline
                        </i>
                        Contact Us
                      </h5>
                      <p className='m-0 font-weight-500 mt-6 hide-on-med-and-down text-ellipsis'>
                        Looking to purchase Timebars?
                      </p>
                      <span className='social-icons hide-on-med-and-down'>
                        <i className='fab fa-behance'></i>
                        <i className='fab fa-dribbble ml-5'></i>
                        <i className='fab fa-facebook-f ml-5'></i>
                        <i className='fab fa-instagram ml-5'></i>
                      </span>
                    </div>
                    <div className='col s12 m12 l8 form-header'>
                      <h6 className='form-header-text'>
                        <i className='material-icons'> mail_outline </i> Write
                        us a few words about your needs.
                      </h6>
                    </div>
                  </div>
                </div>

                {/* <!-- Contact Sidenav --> */}
                <div id='sidebar-list' className='row contact-sidenav'>
                  <div className='col s12 m12 l4'>
                    {/* <!-- Sidebar Area Starts --> */}
                    <div className='sidebar-left sidebar-fixed'>
                      <div className='sidebar'>
                        <div className='sidebar-content'>
                          <div className='sidebar-menu list-group position-relative'>
                            <div
                              className='sidebar-list-padding app-sidebar contact-app-sidebar'
                              id='contact-sidenav'>
                              <ul className='contact-list display-grid'>
                                <li>
                                  <h5 className='m-0'>
                                    What will be next step?
                                  </h5>
                                </li>
                                <li>
                                  <h6 className='mt-5 line-height'>
                                    Try it out, there is no cost and no software
                                    to install.
                                    <a
                                      href='https://www.timebars.com'
                                      className=''>
                                      Try Timebars.
                                    </a>
                                  </h6>
                                </li>
                                <li>
                                  <hr className='mt-5' />
                                </li>
                              </ul>
                              <div className='row'>
                                {/* <!-- Place --> */}
                                <div className='col s12 place mt-4 p-0'>
                                  <div className='col s2 m2 l2'>
                                    <i className='material-icons'> place </i>
                                  </div>
                                  <div className='col s10 m10 l10'>
                                    <p className='m-0'>
                                      360 King street, <br /> Feasterville
                                      Trevose, PA 19053
                                    </p>
                                  </div>
                                </div>
                                {/* <!-- Phone --> */}
                                <div className='col s12 phone mt-4 p-0'>
                                  <div className='col s2 m2 l2'>
                                    <i className='material-icons'> call </i>
                                  </div>
                                  <div className='col s10 m10 l10'>
                                    <p className='m-0'>(800) 900-200-333</p>
                                  </div>
                                </div>
                                {/* <!-- Mail --> */}
                                <div className='col s12 mail mt-4 p-0'>
                                  <div className='col s2 m2 l2'>
                                    <i className='material-icons'>
                                      mail_outline
                                    </i>
                                  </div>
                                  <div className='col s10 m10 l10'>
                                    <p className='m-0'>info@domain.com</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <a
                            href='/#'
                            data-target='contact-sidenav'
                            className='sidenav-trigger'>
                            <i className='material-icons'>menu</i>
                          </a>
                        </div>
                      </div>
                    </div>
                    {/* <!-- Sidebar Area Ends --> */}
                  </div>
                  <div className='col s12 m12 l8 contact-form margin-top-contact'>
                    <div className='row'>
                      <form className='col s12'>
                        <div className='row'>
                          <div className='input-field col m6 s12'>
                            <input
                              id='mfName'
                              type='text'
                              className='validate'
                            />
                            <label htmlFor='name'>Your Name</label>
                          </div>
                          <div className='input-field col m6 s12'>
                            <input
                              id='mfEmail'
                              type='text'
                              className='validate'
                            />
                            <label htmlFor='email'>Your e-mail</label>
                          </div>
                        </div>
                        <div className='row'>
                          <div className='input-field col m6 s12'>
                            <input
                              id='mfCompany'
                              type='text'
                              className='validate'
                            />
                            <label htmlFor='company'>Company</label>
                          </div>
                          <div className='input-field col m6 s12'>
                            <input
                              id='mfPhoneNo'
                              type='text'
                              className='validate'
                            />
                            <label htmlFor='mfPhoneNo'>Phone No.</label>
                          </div>
                          <div className='input-field col s12 width-100'>
                            <textarea
                              id='mfNeeds'
                              className='materialize-textarea'></textarea>
                            <label htmlFor='mfNeeds'>
                              Enter your needs here!
                            </label>
                            <div
                              id='mfSendFormDataEmail'
                              className='waves-effect waves-light btn'>
                              Send
                            </div>
                          </div>
                        </div>
                        <div className='row'>
                          <div className='row mt-5'>
                            Click the Unsubscribe link to be removed from this
                            mailing list!
                          </div>
                          <div
                            id='xxx'
                            className=''
                            // contentEditable
                          >
                            <a href='https://app.rlan.ca/api/subscriber/deletebyid/5cabb473bbfe685c96c45fe1'>
                              Unsubscribe!
                            </a>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* <!-- START RIGHT SIDEBAR NAV --> */}
            <Sidebar />
            {/* <!-- END RIGHT SIDEBAR NAV --> */}
          </div>
        </div>
      </div>
    </div>
    //   {/* <!-- END: Page Main--> */}
  );
}
