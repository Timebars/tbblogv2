import React from 'react';

export default function TableRow({
  tbDocID,
  tbDocName,
  tbDocL1,
  tbDocL2,
  tbDocL3,
  tbDocPurpose,
  tbDocOwner,
  tbDocStatus,
  tbDocSortOrder,
  tbDocPopular,
  tbDocWrittenBy,
}) {
  return (
    <tr id={`listItem_${tbDocID}`} key={tbDocID}>
      <td>
        <a
          title='Edit this document'
          id={`edit_${tbDocID}`}
          href='/blog-grid/#'>
          <i className='material-icons tiny'>edit</i>
        </a>
      </td>
      <td id={`tbDocID-${tbDocID}`}>
        <div>{tbDocID}</div>
      </td>
      <td id={`tbDocName-${tbDocID}`}>
        <div style={{ width: 200 }}>{tbDocName}</div>
      </td>
      <td id={`tbDocL1-${tbDocID}`}>
        <div>{tbDocL1}</div>
      </td>
      <td id={`tbDocL2-${tbDocID}`}>
        <div>{tbDocL2}</div>
      </td>
      <td id={`tbDocL3-${tbDocID}`}>
        <div>{tbDocL3}</div>
      </td>
      <td id={`tbDocPurpose-${tbDocID}`}>
        <div style={{ width: 75 }}>{tbDocPurpose}</div>
      </td>
      <td id={`tbDocOwner-${tbDocID}`}>
        <div style={{ width: 75 }}>{tbDocOwner}</div>
      </td>
      <td id={`tbDocStatus-${tbDocID}`}>
        <div style={{ width: 50 }}>{tbDocStatus}</div>
      </td>
      <td id={`tbDocSortOrder-${tbDocID}`}>
        <div style={{ width: 50 }}>{tbDocSortOrder}</div>
      </td>
      <td id={`tbDocPopular-${tbDocID}`}>
        <div style={{ width: 40 }}>{tbDocPopular}</div>
      </td>
      <td id={`tbDocWrittenBy-${tbDocID}`}>
        <div style={{ width: 75 }}>{tbDocWrittenBy}</div>
      </td>

      <td>
        <button
          title='Delete this Document'
          type='button'
          className='btn-flat'
          id={`delete_${tbDocID}`}>
          <i className='material-icons'>delete_forever</i>
        </button>
      </td>
    </tr>
  );
}
