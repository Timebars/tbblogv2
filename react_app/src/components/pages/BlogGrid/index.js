import React from 'react';

import { DataTable } from 'components/utils/';
// import TableRow from './TableRow';
import grid from './grid.json';

export default class BlogGrid extends React.Component {
  state = { data: [] };

  componentDidMount() {
    fetch('https://api.rlan.ca/api/v1/tbdocuments')
      .then(res => res.json())
      .then(({ data }) => {
        this.setState({ data: data || [] });
      })
      .catch(console.error);
  }
  render() {
    const { data } = this.state;
    return (
      <div id='dataTableDocumentsBlogL1'>
        <div id='docUploadDropArea' style={{ marginTop: 5, fontSize: 24 }}>
          Blog Article Listing
          <span style={{ marginLeft: 300, fontSize: 12 }}>
            Find and Edit Blog Articles
          </span>
        </div>
        {!!data.length && (
          <DataTable
            data={data}
            columns={grid.columns}
            options={grid.options}
          />
        )}
      </div>
    );
  }
}
