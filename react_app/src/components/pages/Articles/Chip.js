import React from 'react';

export default function Chip({ chipName, qty, chipID }) {
  return (
    <div
      style={{ marginTop: 2, cursor: 'pointer' }}
      id={`${chipID}`}
      key={`chip-${chipID}`}
      className='chip hoverable'
      //   data-chipname='${chipName}'
    >
      {chipName} ({qty})
    </div>
  );
}
