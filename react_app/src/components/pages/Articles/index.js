import React from 'react';

import { BreadCrump } from 'components/utils/';
import BlogCard from './BlogCard';
import Chip from './Chip';

// the following imports are temp (for simulating an api response data)
import TempCardImg1 from 'assets/images/cards/applewatch.png';
import TempCardImg2 from 'assets/images/cards/cameras.png';

const dataFromApi = {
  chips: [
    { chipName: 'general', qty: 0, chipID: 0 },
    { chipName: 'health', qty: 1, chipID: 1 },
    { chipName: 'ahmed', qty: 2, chipID: 2 },
    { chipName: 'sawsan', qty: 3, chipID: 3 },
    { chipName: 'why', qty: 4, chipID: 4 },
    { chipName: 'purchase', qty: 5, chipID: 5 },
    { chipName: 'products', qty: 6, chipID: 6 },
    { chipName: 'backup', qty: 7, chipID: 7 },
  ],
  approvedDocs: [
    {
      tbDocID: 0,
      tbDocCardImage: TempCardImg1,
      tbDocBriefDescription:
        'To confirm the completeness and feasibility of the detailed project plan and definition of requirements.',
      tbDocName: 'Try Timebars',
      tbDocOwner: 'Jim Cox',
    },
    {
      tbDocID: 1,
      tbDocCardImage: TempCardImg2,
      tbDocBriefDescription:
        'To confirm the completeness and feasibility of the detailed project plan and definition of requirements.',
      tbDocName: 'Learn More about Timebars',
      tbDocOwner: 'Jim Cox',
    },
  ],
};

export default function Articles() {
  return (
    <React.Fragment>
      {dataFromApi.chips.map(Chip)}
      <BreadCrump title='Timebars Blog' l1='1' l2='2' l3='3' />
      {dataFromApi.approvedDocs.map(BlogCard)}
    </React.Fragment>
  );
}
