import React from 'react';
import { Link } from 'react-router-dom';

import User9 from 'assets/images/user/9.jpg';

export default function BlogCard({
  tbDocID,
  tbDocCardImage,
  tbDocBriefDescription,
  tbDocName,
  tbDocOwner,
}) {
  return (
    <div className='col s12 m6 l4 card-width tbcards' key={`card-${tbDocID}`}>
      <div className='card-panel border-radius-6 mt-10 card-animation-1'>
        <img
          className='responsive-img border-radius-8 z-depth-4 image-n-margin'
          src={tbDocCardImage}
          alt={tbDocBriefDescription}
        />
        <p></p>
        <Link
          data-tbid={tbDocID}
          to='#'
          className='launchTbBlogCardFormNew mt-5'>
          {tbDocName}
        </Link>
        <p>{tbDocBriefDescription}</p>
        <div className='row mt-4'>
          <div className='col s2'>
            <Link to='#'>
              <img
                src={User9}
                alt='user'
                // alt='fashion' // duplicate
                width='40'
                className='circle responsive-img mr-3'
              />
            </Link>
          </div>
          <Link to='#'>
            <div className='col s3 p-0 mt-1'>
              <span className='pt-2'>{tbDocOwner}</span>
            </div>
          </Link>
          <div className='col s7 mt-1 right-align'>
            <Link to='#'>
              <span className='material-icons'>favorite_border</span>
            </Link>
            <span className='ml-3 vertical-align-top'>340</span>
            <Link to='#'>
              <span className='material-icons ml-10'>chat_bubble_outline</span>
            </Link>
            <span className='ml-3 vertical-align-top'>80</span>
          </div>
        </div>
      </div>
    </div>
  );
}
