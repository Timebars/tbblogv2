import React from 'react';

export default function LoginForm({ closeModal }) {
  return (
    <div id='LoginFormModal'>
      <div className='modal-content'>
        <form className='login-form'>
          <div className='row'>
            <div className='col s6 m6 l6'>
              <h5 className='ml-4'>Sign in</h5>
            </div>
            <div className='col s6 m6 l6'>
              <a
                onClick={closeModal}
                className='modal-close right'
                title='Close without logging in!'
                href='#!'>
                <i className='material-icons'>close</i>
              </a>
            </div>
          </div>
          <div className='row margin'>
            <div className='input-field col s12'>
              <i className='material-icons prefix pt-2'>person_outline</i>
              <input id='email2' type='text' />
              <label for='email2' className='center-align'>
                Email Address
              </label>
            </div>
          </div>
          <div className='row margin'>
            <div className='input-field col s12'>
              <i className='material-icons prefix pt-2'>lock_outline</i>
              <input id='password2' type='password' />
              <label for='password2'>Password</label>
            </div>
          </div>
          <div className='row'>
            <div className='col s12 m12 l12 ml-2 mt-1'></div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>
              <a
                id='performLogin'
                href='!#'
                className='performLogin btn waves-effect waves-light border-round col s12'>
                Login
              </a>
            </div>
          </div>
          <div className='row'></div>

          <div className='row'>
            <div className='input-field col s6 m6 l6'>
              <p className='margin medium-small'>
                <a className='launchRegisterForm' href='#!'>
                  Register Now!
                </a>
              </p>
            </div>
            <div className='input-field col s6 m6 l6'>
              <p className='margin right-align medium-small'>
                <a className='launchPasswordManagement' href='#!'>
                  Forgot password?
                </a>
              </p>
            </div>
          </div>
          <div className='row'>
            {/* <div id='curLoggedInUser' className='col s6 m6 l6 medium-small'>
              Current logged in user:
            </div> */}
            <div className='col s6 m6 l6 right-align'>
              <a href='#!' className='performLogout medium-small'>
                Log Out Now!
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
