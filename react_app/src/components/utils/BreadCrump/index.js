import React from 'react';

export default function BreadCrump({ title, l1, l2, l3 }) {
  return (
    <div className='pt-0 pb-1' id='breadcrumbs-wrapper'>
      {/* <!-- Search for small screen--> */}
      <div className='container'>
        <div className='row'>
          <div className='col s12 m6 l6'>
            <h5 className='breadcrumbs-title'>{title || ''}</h5>
          </div>
          <div className='col s12 m6 l6 right-align-md'>
            <ol className='breadcrumbs mb-0'>
              <li className='breadcrumb-item'>
                <a href='#!'>{l1 || '1'}</a>
              </li>
              <li className='breadcrumb-item'>
                <a href='#!'>{l2 || '2'}</a>
              </li>
              <li className='breadcrumb-item'>
                <a href='#!'>{l3 || '3'}</a>
              </li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  );
}
