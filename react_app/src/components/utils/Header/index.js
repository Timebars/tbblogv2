import React from 'react';

import MainNav from './MainNav';
import SubNav from './SubNav';

export default function Header() {
  return (
    <header className='page-topbar' id='header'>
      <div className='navbar navbar-fixed'>
        <MainNav />
        <SubNav />
      </div>
    </header>
  );
}
