import React from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';

import { toggleFullScreen } from 'utils/';
import { LoginForm } from 'components/utils/';
import Avatar7 from 'assets/images/avatar/avatar-7.png';
import LogoGreen from 'assets/images/logo/timebarslogogreen.png';

const customStyles = {
  content: {
    top: '47%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function MainNav() {
  const [loginModalIsOpen, triggerLoginModal] = React.useState(false);
  return (
    <nav className='navbar-main navbar-color nav-collapsible sideNav-lock navbar-dark black'>
      <div className='nav-wrapper'>
        <ul className='left'>
          <li>
            <h1 className='logo-wrapper'>
              <Link className='brand-logo darken-1' to='/'>
                <img src={LogoGreen} alt='Timebars Ltd. logo' />
                <span className='logo-text hide-on-med-and-down'></span>
              </Link>
            </h1>
          </li>
        </ul>
        <div className='header-search-wrapper hide-on-med-and-down'>
          <i className='material-icons'>search</i>
          <input
            className='header-search-input z-depth-2'
            type='text'
            name='Search'
            placeholder='Explore Blog Items'
          />
        </div>
        <ul className='navbar-list right'>
          <li className='hide-on-med-and-down'>
            <Link
              className='waves-effect waves-block waves-light toggle-fullscreen'
              onClick={toggleFullScreen}
              to='#'>
              <i className='material-icons'>settings_overscan </i>
            </Link>
          </li>

          <li>
            <Link
              style={{ fontSize: 18 }}
              onClick={() => triggerLoginModal(!loginModalIsOpen)}
              className='launchlLoginForm hoverable'
              to='/#'>
              Login!
            </Link>
            <Modal
              isOpen={loginModalIsOpen}
              style={customStyles}
              contentLabel='Login Form'>
              <LoginForm closeModal={() => triggerLoginModal(false)} />
            </Modal>
          </li>

          <li>
            <Link
              className='waves-effect waves-block waves-light profile-button'
              //   href='javascript:void(0);'
              to='/#'
              data-target='profile-dropdown'>
              <span className='avatar-status avatar-online'>
                <img src={Avatar7} alt='avatar' />
                <i></i>
              </span>
            </Link>
          </li>
          <li>
            <Link
              className='waves-effect waves-block waves-light sidenav-trigger'
              to='#'
              data-target='slide-out-right'>
              <i className='material-icons'>format_indent_increase</i>
            </Link>
          </li>
        </ul>

        {/* <!-- notifications-dropdown--> */}
        <ul className='dropdown-content' id='notifications-dropdown'>
          <li style={{ marginTop: 65 }}>
            <h6>
              NOTIFICATIONS<span className='new badge'>1</span>
            </h6>
          </li>
          <li className=' divider'></li>
          <li>
            <Link className='grey-text text-darken-2' to='#!'>
              <span className='material-icons icon-bg-circle cyan small'>
                add_shopping_cart
              </span>
              A new order has been placed!
            </Link>
            <time className='media-meta' dateTime='2015-06-12T20:50:48+08:00'>
              2 hours ago
            </time>
          </li>
        </ul>

        {/* <!-- profile-dropdown--> */}
        <ul className='dropdown-content' id='profile-dropdown'>
          <li style={{ marginTop: 10 }}>
            <Link className='grey-text text-darken-1 launchRegisterForm' to='#'>
              <i className='material-icons'>settings</i>Register
            </Link>
          </li>
          <li style={{ marginTop: 0 }}>
            <Link className='launchProfilePage grey-text text-darken-1' to='#'>
              <i className='material-icons'>face</i> Profile
            </Link>
          </li>
          <li>
            <Link className='grey-text text-darken-1 launchlLoginForm' to='#'>
              <i className='material-icons'>settings</i>Manage Account
            </Link>
          </li>

          {/* <!--                   <li><a className="grey-text text-darken-1 launchlLoginForm" href="#"><i
                         className="material-icons">keyboard_tab</i>Log In</a> 
                </li>--> */}
          {/* <!--                   <li><a className="grey-text text-darken-1 launchPasswordManagement" href="#"><i
                         className="material-icons">lock_outline</i> Password Management</a> 
                </li>--> */}
          <li>
            <Link className='performLogout grey-text text-darken-1' to='#'>
              <i className='material-icons'>keyboard_tab</i> Logout
            </Link>
          </li>
          <li>
            <Link className='grey-text text-darken-1' to='#'>
              <i className='material-icons'>live_help</i> Help
            </Link>
          </li>
          <li className='divider'></li>
        </ul>
      </div>
    </nav>
  );
}
