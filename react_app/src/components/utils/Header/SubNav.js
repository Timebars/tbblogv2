import React from 'react';
import { Link } from 'react-router-dom';
import Modal from 'react-modal';

import { NewDocForm } from 'components/utils/';

const customStyles = {
  content: {
    top: '47%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
  },
};

export default function SubNav() {
  const [newDocModalIsOpen, triggerNewDocModal] = React.useState(false);
  return (
    <nav className='white hide-on-med-and-down' id='horizontal-nav'>
      <div className='nav-wrapper'>
        <ul
          className='left hide-on-med-and-down'
          id='ul-horizontal-nav'
          data-menu='menu-navigation'>
          {/* <!-- adding lines here adds more icons to menu bar, one per line --> */}
          <li>
            <Link to='/'>
              <i className='material-icons'>home</i>
              <span>Blog Home</span>
            </Link>
          </li>

          <li>
            <Link to='/articles'>
              <i className='material-icons'>import_contacts</i>
              <span>Articles</span>
            </Link>
          </li>

          <li>
            <Link to='/blog-grid'>
              <i className='material-icons'>grid_on</i>
              <span>Blog Grid</span>
            </Link>
          </li>
          <li>
            <Link to='/#' onClick={() => triggerNewDocModal(true)}>
              <i className='material-icons'>fiber_new</i>
              <span>New</span>
            </Link>
            <Modal
              isOpen={newDocModalIsOpen}
              style={customStyles}
              contentLabel='Login Form'>
              <NewDocForm closeModal={() => triggerNewDocModal(false)} />
            </Modal>
          </li>

          <li>
            <Link to='/contact-us'>
              <i className='material-icons'>border_color</i>
              <span>Contact Us</span>
            </Link>
          </li>

          {/* <!-- <li><a href="#!" className="launchDropifyXlsx"><i
                     className="material-icons">import_export</i><span>Import</span></a></li> --> */}

          {/* <!-- App List menu dropdown, second button on horiz menu --> */}
          <li>
            <Link className='dropdown-menu' to='/#' data-target='adminMenuList'>
              <i className='material-icons'>photo_filter</i>
              <span>
                Admin
                <i className='material-icons right'>keyboard_arrow_down</i>
              </span>
            </Link>
            <ul
              className='dropdown-content dropdown-horizontal-list'
              id='adminMenuList'>
              <li className='launchDropifyXlsx' data-menu=''>
                <Link to='/#'>Import/Backup</Link>
              </li>
              <li className='launchTbResources' data-menu=''>
                <Link to='/#'>People Grid</Link>
              </li>
              <li className='launchTbTags' data-menu=''>
                <Link to='/#'>Tags Grid</Link>
              </li>
            </ul>
          </li>
        </ul>
        {/* <!-- END: Horizontal nav--> */}
      </div>
    </nav>
  );
}
