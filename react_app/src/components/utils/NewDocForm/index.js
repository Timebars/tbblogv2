import React from 'react';

export default function NewDocForm({ closeModal }) {
  return (
    <div id='launchDocumentsForm'>
      <div
        className='modal-content'
        data-backdrop='static'
        data-keyboard='false'>
        {/* <!-- Left Half --> */}
        <div className='containerzzz'>
          <form className='zzz'>
            <div className='row'>
              <div className='input-field col s8'>
                <h5 className='valign-wrapper'>
                  {' '}
                  <i className='material-icons medium indigo-text valign-wrapper'>
                    control_point
                  </i>
                  Timebars CMS Form
                </h5>
              </div>
              <div className='input-field col s2'>
                <input disabled id='tbDocID' type='text' className='validate' />
                <label htmlFor='tbDocID'>Unique ID</label>
              </div>
              <div className='input-field col s2'>
                <a
                  href='#!'
                  title='Save to the database'
                  className='btnSaveFormDataDocument waves-effect waves-blue btn-flat'>
                  Save
                </a>
                <a
                  onClick={closeModal}
                  title='Close without saving!'
                  className='closeDocForm modal-close waves-effect waves-blue btn-flat'>
                  Close
                </a>
              </div>
            </div>

            <div className='row'>
              <div className='input-field col s12'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocName'
                  type='text'
                  className='validate'
                  required=''
                />
                <label htmlFor='tbDocName'>Document Name:</label>
                <span
                  className='helper-text'
                  data-error='You must enter a value!'
                  data-success='ok'></span>
              </div>
            </div>
            <div className='row' style={{ marginBottom: 20 }}>
              <div className='col s12'>
                <div
                  id='tbDocHTMLContent'
                  className='updatableStd'
                  data-formkey='md-blog-na'></div>
              </div>
            </div>

            <div className='row'>
              <div className='input-field col s12'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <textarea
                  id='tbDocBriefDescription'
                  className='materialize-textarea'
                  data-length='250'></textarea>
                <label htmlFor='tbDocBriefDescription'>Brief Description</label>
              </div>
            </div>

            <div className='row'>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocL1'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document L1'
                />
                <label htmlFor='tbDocL1'>Level 1 (L1)</label>
              </div>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocL2'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document L2'
                />
                <label htmlFor='tbDocL2'>Level 2 (L2)</label>
              </div>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocL3'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document L3'
                />
                <label htmlFor='tbDocL3'>Level 3 (L3)</label>
              </div>
            </div>
            <div className='row'>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocL4'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document L4'
                  data-formkey='md-docform-Investment Category'
                />
                <label htmlFor='tbDocL4'>Level 4 (L4)</label>
              </div>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocL5'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document L5'
                />
                <label htmlFor='tbDocL5'>Level 5 (L5)</label>
              </div>
            </div>
            <div className='row'>
              <div className='input-field col s12'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocCardImage'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document Images'
                />
                <label htmlFor='tbDocCardImage'>Image Relative Path</label>
              </div>
            </div>
            <div className='row'>
              <div className='input-field col s8'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocNameShort'
                  type='text'
                  className='validate'
                  data-length='20'
                />
                <label htmlFor='tbDocNameShort'>Short Name</label>
              </div>

              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocType'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document Type'
                />
                <label htmlFor='tbDocType'>Type of Document</label>
              </div>
            </div>

            <div className='row'>
              <div className='input-field col s8'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocPurpose'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document Purpose'
                />
                <label htmlFor='tbDocPurpose'>Purpose</label>
              </div>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocStatus'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document Status'
                />
                <label htmlFor='tbDocStatus'>Status</label>
              </div>
            </div>

            <div className='row'>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocSortOrder'
                  type='text'
                  className='validate'
                  required=''
                />
                <label htmlFor='tbDocSortOrder'>Sort Order</label>
              </div>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocOwner'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='people-docform-Users'
                />
                <label htmlFor='tbDocOwner'>Owner</label>
              </div>
            </div>

            <div className='row'>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocGroup'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-Document Grouping'
                />
                <label htmlFor='tbDocGroup'>Grouping Tag</label>
              </div>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input
                  id='tbDocPopular'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='md-docform-YesNo'
                />
                <label htmlFor='tbDocPopular'>Popularity Tag</label>
              </div>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>navigate_next</i>
                <input id='tbDocCustomerID' type='text' className='validate' />
                <label htmlFor='tbDocCustomerID'>Customer ID</label>
              </div>
            </div>

            <div className='row'>
              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocWrittenBy'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='people-docform-Users'
                />
                <label htmlFor='tbDocWrittenBy'>Written By</label>
              </div>

              <div className='input-field col s4'>
                <i className='material-icons prefix blue-text'>
                  arrow_drop_down
                </i>
                <input
                  id='tbDocApprovedBy'
                  type='text'
                  className='validate mcttUpdatablev1'
                  data-formkey='people-docform-Business Owners'
                />
                <label htmlFor='tbDocApprovedBy'>Approved By</label>
              </div>
              <div className='input-field col s4'>
                <input disabled id='xxx' type='text' className='validate' />
                <label htmlFor='xxx'>Not Used</label>
              </div>
            </div>
          </form>
          <div className='modal-footer'>
            <button
              title='Shortcut to PDF Printing'
              className='waves-effect waves-light btn-small blue darken-4'
              id='createPRCFromFormForDownLoad'>
              Print PDF
            </button>
            <a
              href='#!'
              title='Save to the database'
              className='btnSaveFormDataDocument waves-effect waves-blue btn-flat'>
              Save
            </a>
            <a
              onClick={closeModal}
              title='Close without saving!'
              className='closeDocForm modal-close waves-effect waves-blue btn-flat'>
              Close
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
