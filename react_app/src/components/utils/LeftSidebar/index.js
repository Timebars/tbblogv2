import React from 'react';

export default function LeftSidebar() {
  return (
    <aside className='sidenav-main nav-expanded nav-lock nav-collapsible sidenav-fixed hide-on-large-only'>
      <div className='brand-sidebar sidenav-light'></div>
      <ul
        className='sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed hide-on-large-only menu-shadow'
        id='slide-out'
        data-menu='menu-navigation'
        data-collapsible='menu-accordion'>
        <li className='navigation-header'>
          <a className='navigation-header-text' href='/#'>Navigation </a>
          <i className='navigation-header-icon material-icons'>more_horiz</i>
        </li>
        <li className='active bold'>
          <a className='collapsible-header waves-effect waves-cyan' href='/#'>
            <i className='material-icons'>pages</i>
            <span className='menu-title' data-i18n=''>
              Pages
            </span>
          </a>
          <div className='collapsible-body'>
            <ul
              className='collapsible collapsible-sub'
              data-collapsible='accordion'>
              <li className='active'>
                <a className='collapsible-body active' href='#!' data-i18n=''>
                  <i className='material-icons'>radio_button_unchecked</i>
                  <span className='launchTbBlogHomePage'>HomePage</span>
                </a>
              </li>

              <li className='launchAboutTimebarsPage' data-menu=''>
                <a href='/#'>
                  <i className='material-icons'>radio_button_unchecked</i>About
                  Timebars
                </a>
              </li>
              <li className='launchWhyTimebarsPage' data-menu=''>
                <a href='/#'>
                  <i className='material-icons'>radio_button_unchecked</i>Why
                  Timebars
                </a>
              </li>
              <li className='launchTryTimebarsPage' data-menu=''>
                <a href='/#'>
                  <i className='material-icons'>radio_button_unchecked</i>Try
                  Timebars
                </a>
              </li>
              <li className='launchPurchaseTimebarsPage' data-menu=''>
                <a href='/#'>
                  <i className='material-icons'>radio_button_unchecked</i>
                  Purchase Timebars
                </a>
              </li>
              <li data-menu=''>
                <a className='launchTimebarsLearnMorePage' href='/#'>
                  <i className='material-icons'>radio_button_unchecked</i>Learn
                  More...
                </a>
              </li>
              <li data-menu=''>
                <a className='launchContactMailPage' href='/#'>
                  <i className='material-icons'>radio_button_unchecked</i>
                  Contact and Mail
                </a>
              </li>

              <li data-menu=''>
                <a className='launchTimebarsSupportPage' href='/#'>
                  <i className='material-icons'>radio_button_unchecked</i>
                  Timebars Support
                </a>
              </li>

              {/* <!-- add more page links here --> */}
            </ul>
          </div>
        </li>

        <li className='bold'>
          <a className='collapsible-header waves-effect waves-cyan ' href='/#'>
            <i className='material-icons'>photo_filter</i>

            {/* <!-- add more apps in accordion here --> */}
            <span className='menu-title' data-i18n=''>
              Apps
            </span>
          </a>
          <div className='collapsible-body'>
            <ul
              className='collapsible collapsible-sub'
              data-collapsible='accordion'>
              <li>
                <a
                  className='launchBlogArticles collapsible-body'
                  href='/#'
                  data-i18n=''>
                  <i className='material-icons'>radio_button_unchecked</i>
                  <span>Articles</span>
                </a>
              </li>
              <li>
                <a
                  className='collapsible-body collapsible-header waves-effect waves-cyan'
                  href='/#'
                  data-i18n=''>
                  <i className='material-icons'>radio_button_unchecked</i>
                  <span>Timebars Apps</span>
                </a>
                <div className='collapsible-body'>
                  <ul className='collapsible' data-collapsible='accordion'>
                    <li>
                      <a className='collapsible-body' href='/#' data-i18n=''>
                        <i className='material-icons'>radio_button_unchecked</i>
                        <span id='launchBulkMailerLSN'>Bulk Mailer</span>
                      </a>
                    </li>

                    <li className='launchMakePDFSiebelSimulationPage'>
                      <a className='collapsible-body' href='/#' data-i18n=''>
                        <i className='material-icons'>radio_button_unchecked</i>
                        <span>Make PDF</span>
                      </a>
                    </li>

                    <li className='launchDropifyMain'>
                      <a className='collapsible-body' href='/#' data-i18n=''>
                        <i className='material-icons'>radio_button_unchecked</i>
                        <span>Upload Subscribers</span>
                      </a>
                    </li>
                    <li className='launchDropifyXlsx'>
                      <a className='collapsible-body' href='/#' data-i18n=''>
                        <i className='material-icons'>radio_button_unchecked</i>
                        <span>Import Excel Data</span>
                      </a>
                    </li>
                    <li className='launchTbTags'>
                      <a className='collapsible-body' href='/#' data-i18n=''>
                        <i className='material-icons'>radio_button_unchecked</i>
                        <span>Tags</span>
                      </a>
                    </li>
                    <li className='launchTbResources'>
                      <a className='collapsible-body' href='/#' data-i18n=''>
                        <i className='material-icons'>radio_button_unchecked</i>
                        <span>User Profiles</span>
                      </a>
                    </li>
                    <li id='xxx'>
                      <a
                        className='collapsible-body launchTbBlogGrid'
                        href='/#'
                        data-i18n=''>
                        <i className='material-icons'>radio_button_unchecked</i>
                        <span>Documents</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </div>
        </li>
        <li>
          <a href='#!'>
            <i className='material-icons'>import_contacts</i>
            <span className='launchContactUs'>Contact Us</span>
          </a>
        </li>
        <li className='bold'>
          <a className='waves-effect waves-cyan ' href='#!'>
            <i className='material-icons'>help_outline</i>
            <span className='menu-title' data-i18n=''>
              Support
            </span>
          </a>
        </li>
      </ul>
      <div className='navigation-background'></div>
      <a
        className='sidenav-trigger btn-floating btn-medium waves-effect waves-light hide-on-large-only'
        href='/#'
        data-target='slide-out'>
        <i className='material-icons'>menu</i>
      </a>
    </aside>
  );
}
