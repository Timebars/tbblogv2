import React from 'react';
import $ from 'jquery';
import 'datatables.net-dt/css/jquery.dataTables.css';
import 'datatables.net';

export default class DataTable extends React.Component {
  datatable = null;

  componentDidMount() {
    const { data = [], columns, options } = this.props;
    this.$el = $(this.el);
    this.dataTable = this.$el.DataTable({
      data,
      columns,
      ...options,
    });
  }

  componentWillUnmount() {
    this.dataTable.destroy(true);
  }

  search = value => {
    this.dataTable.search(value).draw();
  };

  render() {
    return <table ref={el => (this.el = el)} />;
  }
}
