import React from 'react';
import ReactDOM from 'react-dom';

import 'assets/css/themes/horizontal-menu-template/materialize.min.css';
import 'assets/css/themes/horizontal-menu-template/style.min.css';
import 'assets/css/layouts/style-horizontal.min.css';
import 'assets/css/custom/custom.css';
import 'assets/css/pages/page-contact.min.css';
import 'assets/fonts/fontawesome/css/all.min.css';
import 'assets/vendors/vendors.min.css';
import 'assets/vendors/dropify/css/dropify.min.css';
import 'assets/vendors/flag-icon/css/flag-icon.min.css';
import 'assets/vendors/data-tables/css/select.dataTables.min.css';
import 'assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css';
import 'assets/vendors/data-tables/css/jquery.dataTables.min.css';

import App from './components/App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
