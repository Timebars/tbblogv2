

The data files in this folder was used to create APEX database for Ords API.
There are 6 rows of timebars and metadata for fake hierarchy.

Three fields removed and two renamed compared to timebars indexed db
Removed tbMDOther13, tbMDOther14, tbMDOther16
Renamed tbMDProgramActivityAlignment to tbMDProgActivityAlignment
renamed tbMDConstraintsAndAssumptions to tbMDConstraintsAssumptions

The AdminPanel pk field changed from tbID to apID on oracle side, kept tbID as is for indexed db.

Will have to update indexed to suit.


