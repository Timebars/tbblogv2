// was the todo app from noob coder
// was doc datatableuniversal
// refactored to use tb and idb
// was also focd documents

/* idb code */
import {
  openIDBGetStore,
  returnOneItemUniversal,
  deleteRowFromIDBUniversal,
  displayFailureMessageDB,
  displaySuccessMessageDB,
} from './tbdatabase';

import {
  tbBlogChipsHTMLComponent,
  tbBlogPageHTMLComponent,
  tbBlogCardHTMLComponentNew,
  tbBlogBreadCrumbHTMLComponent,
  docFormHTMLComponent,
  dtBlogTableHeaderHTMLComponent,
} from './htmlblogcomponents';

const fileSaver = require('file-saver');
const json2csv = require('json2csv').parse;

const STORE_ADMINPANEL = 'tbAdminPanel';
const STORE_RESOURCES = 'tbResources';
const STORE_TAGS = 'tbTags';
const STORE_DOCS = 'tbDocuments';

// global variables
let displayDT = $('#displayDtDocuments');
//const message = $("#msgReporting");

const API_URL = 'https://api.rlan.ca/api/v1';

// draw blog cards
export function drawTbBlogCardsNew(l3Value) {
  let pageName = 'Timebars Blog';
  let targetDiv = '#divTbPagesL1';
  let L1 = '1';
  let L2 = '2';
  let L3 = '3';
  tbBlogBreadCrumbHTMLComponent(targetDiv, pageName, L1, L2, L3);

  let topHtml = `<div id="divTbBlogCardsL1" class="col s12">
    <div id="card-panel-type" class="section"></div>`;
  let td = $('#divTbPagesL1');
  td.append(topHtml);

  fetch(`${API_URL}/tbdocuments`)
    .then(res => res.json())
    .then(res => {
      const approvedDocs = res.data.filter(
        doc => doc.tbDocStatus.toLowerCase() === 'approved',
      );
      if (!approvedDocs.length) return;
      console.log('drawTbBlogCardsNew', { approvedDocs });
      approvedDocs.forEach(doc => {
        let tbDocID = doc.tbDocID;
        let tbDocName = doc.tbDocName;
        let tbDocBriefDescription = doc.tbDocBriefDescription;
        let tbDocType = doc.tbDocType;
        let tbDocNameShort = doc.tbDocNameShort;
        let tbDocGroup = doc.tbDocGroup;
        let tbDocPurpose = doc.tbDocPurpose;
        let tbDocOwner = doc.tbDocOwner;
        let tbDocPopular = doc.tbDocPopular;
        let tbDocSortOrder = doc.tbDocSortOrder;
        let tbDocLastModified = doc.tbDocLastModified;
        let tbDocApprovedBy = doc.tbDocApprovedBy;
        let tbDocStatus = doc.tbDocStatus;
        let tbDocWrittenBy = doc.tbDocWrittenBy;
        let tbDocCardImage = doc.tbDocCardImage;
        let tbDocL1 = doc.tbDocL1;
        let tbDocL2 = doc.tbDocL2;
        let tbDocL3 = doc.tbDocL3;
        let tbDocL4 = doc.tbDocL4;
        let tbDocL5 = doc.tbDocL5;
        let targetDiv = '#divTbPagesL1';
        //  console.log("tbDocName: " + tbDocName + " tbDocType: " + tbDocType + " tbDocGroup: " + tbDocGroup)
        tbBlogCardHTMLComponentNew(
          targetDiv,
          tbDocCardImage,
          tbDocID,
          tbDocName,
          tbDocBriefDescription,
          tbDocOwner,
          tbDocStatus,
          tbDocPurpose,
        );
      });
      let bottomhtml = `</div></div>`;
      td.append(bottomhtml);
    })
    .catch(console.error);
}

// fill blog card form
export function fillDocumentsFormFromBlogCardsNew(tbDocID) {
  fetch(`${API_URL}/tbdocuments/tbdocid/${tbDocID}`)
    .then(res => res.json())
    .then(({ data }) => {
      console.log('fillDocumentsFormFromBlogCardsNew', { tbDocID, data });
      if (!data) return alert('not a valid key, check for string');
      const {
        tbDocID,
        tbDocName,
        tbDocBriefDescription,
        tbDocHTMLContent,
      } = data;

      // this is where we fill form
      $('#tbDocID_blog').val(tbDocID);
      $('#tbDocName_blog').html(tbDocName);

      $('#tbDocBriefDescription_blog').html(tbDocBriefDescription);
      $('#tbDocHTMLContent_blog').html(tbDocHTMLContent);

      //     $("#tbDocType").val(data.tbDocType);
      //     $("#tbDocNameShort").val(data.tbDocNameShort);
      //     //  getEditorDataContentMain(data.tbDocHTMLContent)
      //    $("#tbDocHTMLContent").html(data.tbDocHTMLContent);
      //     $("#tbDocGroup").val(data.tbDocGroup);
      //     $("#tbDocPurpose").val(data.tbDocPurpose);
      //     $("#tbDocOwner").val(data.tbDocOwner);
      //     $("#tbDocPopular").val(data.tbDocPopular);
      //     $("#tbDocSortOrder").val(data.tbDocSortOrder);
      //     $("#tbDocLastModified").val(data.tbDocLastModified);
      //     $("#tbDocApprovedBy").val(data.tbDocApprovedBy);
      //     $("#tbDocStatus").val(data.tbDocStatus);
      //     $("#tbDocWrittenBy").val(data.tbDocWrittenBy);
      //     $("#tbDocL1").val(data.tbDocL1);
      //     $("#tbDocL2").val(data.tbDocL2);
      //     $("#tbDocL3").val(data.tbDocL3);
      //     $("#tbDocL4").val(data.tbDocL4);
      //     $("#tbDocL5").val(data.tbDocL5);
      //     $("#tbDocCardImage").val(data.tbDocCardImage);

      M.updateTextFields();
    })
    .catch(() => alert('there was error getting store id'));
}

// use this for the support page

export function drawContentPage(rowID) {
  $('#divTbPagesL1').empty();

  //let rowID = "16"
  let storeName = 'tbDocuments';
  let indexField = 'tbDocID';

  returnOneItemUniversal(rowID, storeName, indexField, function(callbackItem) {
    let data = callbackItem;
    let htmlContent = data.tbDocHTMLContent;
    // first remove any existing pages
    $('#divTbPagesL1').css('visibility', 'visible');
    //   $("#divTbPagesL1").empty()
    let pageName = data.tbDocName;
    // console.log("launced home")
    let targetDiv = '#divTbPagesL1';
    let L1 = data.tbDocL1;
    let L2 = data.tbDocL2;
    let L3 = data.tbDocL3;
    tbBlogBreadCrumbHTMLComponent(targetDiv, pageName, L1, L2, L3);

    targetDiv = '#divTbPagesL1';
    tbBlogPageHTMLComponent(targetDiv, htmlContent);
  });
}

// dont need index name because we get the item by idb built in key
function setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal) {
  console.log('setTbidBasedOnKeyUniversal - uid ' + uid);
  console.log('setTbidBasedOnKeyUniversal - storeName ' + storeName);

  /* fetch section begins */
  // Needs API Support

  // fetch(`${apiUrl}`).catch(() => {
  //   alert('No matching record found, or other error');
  // });
  /* fetch section ends */

  openIDBGetStore(storeName, 'readwrite', function(store) {
    var request = store.get(Number(uid));
    request.onerror = function(event) {
      alert('No matching record found, or other error');
    };
    request.onsuccess = function(event) {
      // // note.innerHTML += '<li> opened store tbTimebars from CRUD.js</li>';
      var data = request.result;

      data[fieldName] = newVal;

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function(event) {
        alert('there was error updating data');
      };
      requestUpdate.onsuccess = function(event) {
        console.log(
          'succes updating fieldNamed: ' +
            fieldName +
            ' on tbDocID: ' +
            data.tbDocID,
        );
      };
    };
  });
}

// post json data to idb from the Excel sheet
export function xlsxJsonDataPopulator(jsonData, storeName) {
  openIDBGetStore(storeName, 'readwrite', function(store) {
    let request;
    let i;
    try {
      // add rows
      for (i = 0; i < jsonData.length; i++) {
        request = store.put(jsonData[i]);
      }
    } catch (e) {
      displayFailureMessageDB(
        'Some Problem While adding data, if data already exists, this will happen, clear data out then try again',
      );
      throw e;
    }
    request.onsuccess = function(evnt) {
      displaySuccessMessageDB(`App data added to store: ${storeName}.<br> `);
      console.log(`Data added to store: ${storeName} `);
    };
    request.onerror = function() {
      displayFailureMessageDB(
        'data must exist already, will not overwite it. to check hit F12 key > Application Tab > IndexedDB ' +
          this.error,
      );
      console.log('Error Default data must exist already! ' + this.error);
    };
  });
}

/* START  CHIPS Blog Components */

export function drawBlogChips() {
  returnAllItems('tbDocuments', function(data) {
    // get Unique L3 values
    var L3Array = data.reduce(function(a, d) {
      if (a.indexOf(d.tbDocL3) === -1) {
        a.push(d.tbDocL3);
      }
      return a;
    }, []);
    console.log('L3 Values: ' + L3Array);

    // fill article side menu chips count quantity articles by L3 (blog and TB)

    for (let item in L3Array) {
      let l3Item = L3Array[item];
      let chipName = L3Array[item];
      //  console.log("each2: " + l3Item)

      const qtyPagesByL3 = data
        .filter(items => items.tbDocL3 === l3Item)
        .reduce((acc, score) => Number(acc) + 1, 0);

      // let targetDiv = '#divChipHTMLComponent'
      let targetDiv = '#divTbPagesL1';

      let chipDivID = '';
      tbBlogChipsHTMLComponent(targetDiv, chipName, qtyPagesByL3, chipDivID);
    }
  });
}

function returnAllItems(storeName, callbackItems) {
  /* fetch section begins */
  fetch(`${API_URL}/${storeName.toLowerCase()}`)
    .then(res => res.json())
    .then(res => {
      const { data } = res;
      callbackItems(data);
    })
    .catch(console.error);
  /* fetch section ends */

  // openIDBGetStore(storeName, 'readwrite', function(store) {
  //   var request = store.getAll();
  //   request.onsuccess = function(evt) {
  //     var items = evt.target.result;
  //     // console.log(data)
  //     callbackItems(items);
  //   };
  // });
}

// END chips on blog

// appends to dataTableDocuments
const generateDatatableDocuments = (Item, ids) => {
  return `<tr id="${ids.listItemID}">
    <td><a title="Edit this document" id="${ids.editID}"><i class="material-icons tiny">edit</i></a></td>
        <td id="tbDocID-${Item.tbDocID}"><div>${Item.tbDocID}</div></td>
        <td id="tbDocName-${Item.tbDocID}"><div style="width:200px">${Item.tbDocName}</div></td>

        <td id="tbDocL1-${Item.tbDocID}"><div>${Item.tbDocL1}</div></td>
        <td id="tbDocL2-${Item.tbDocID}"><div>${Item.tbDocL2}</div></td>
        <td id="tbDocL3-${Item.tbDocID}"><div>${Item.tbDocL3}</div></td>

        <td id="tbDocPurpose-${Item.tbDocID}"><div style="width:75px">${Item.tbDocPurpose}</div></td>
        <td id="tbDocOwner-${Item.tbDocID}"><div style="width:75px">${Item.tbDocOwner}</div></td>
        <td id="tbDocStatus-${Item.tbDocID}"><div style="width:50px">${Item.tbDocStatus}</div></td>

        <td id="tbDocSortOrder-${Item.tbDocID}"><div style="width:50px">${Item.tbDocSortOrder}</div></td>
        <td id="tbDocPopular-${Item.tbDocID}"><div style="width:40px">${Item.tbDocPopular}</div></td>
        <td id="tbDocWrittenBy-${Item.tbDocID}"><div style="width:75px">${Item.tbDocWrittenBy}</div></td>

        <td><button title="Delete this Document" type="button" class="btn-flat" id="${ids.deleteID}"><i class="material-icons">delete_forever</i></button></td>
        </tr>`;
};
// Fields removed July 27 2019
// <td id="tbDocApprovedBy-${Item.tbDocID}"><div style="width:75px">${Item.tbDocApprovedBy}</div></td>
// <td id="tbDocNameShort-${Item.tbDocID}"><div style="width:150px">${Item.tbDocNameShort}</div></td>
// <td><button title="Generate PDF Document" class="btn-flatxxx" id="${ids.printID}"><i class="material-icons">picture_as_pdf</i></button></td>
// <td id="batchPrintSelector-${Item.tbDocID}"> <label title="Select many and print in bulk" ><input  id="${ids.bpselectorID}"  type="checkbox" ${Item.batchPrintSelector} /><span></span></label> </td>
// <td id="tbDocBriefDescription-${Item.tbDocID}"><div class="truncate showhide" style="width:200px">${Item.tbDocBriefDescription}</div> </td>

// new code get all items and display them in grid awesome!
export const getDocuments = () => {
  returnAllDocuments(displayDocuments);
};

// master very simple function to get all rows in a table store  // later send store name as var
function returnAllDocuments(callbackDocuments) {
  fetch(`${API_URL}/tbdocuments`)
    .then(res => res.json())
    .then(({ data }) => {
      if (!data) return;
      callbackDocuments(data);
    })
    .catch(console.error);
}

// need to modify to store tbDocID from the default id
export function createDocument(docname) {
  // let tbDocID = $('#tbDocID').val();  // is auto generated by idb
  let tbDocName = docname; //$("#tbDocName").val()
  let tbDocCustomerID = $('#tbDocCustomerID').val();
  let tbDocBriefDescription = $('#tbDocBriefDescription').val();
  let tbDocType = $('#tbDocType').val();
  let tbDocNameShort = $('#tbDocNameShort').val();

  /* account for CK Editor */
  let tbDocHTMLContent = 'This is your content editor'; //$("#tbDocHTMLContent").html()//getEditorData()
  //console.log("thedata" + tbDocHTMLContent)

  let tbDocGroup = $('#tbDocGroup').val();
  let tbDocPurpose = $('#tbDocPurpose').val();
  let tbDocOwner = $('#tbDocOwner').val();
  let tbDocPopular = $('#tbDocPopular').val();
  let tbDocSortOrder = $('#tbDocSortOrder').val();
  let tbDocLastModified = $('#tbDocLastModified').val();
  let tbDocApprovedBy = $('#tbDocApprovedBy').val();
  let tbDocStatus = $('#tbDocStatus').val();
  let tbDocWrittenBy = $('#tbDocWrittenBy').val();

  let tbDocL1 = $('#tbDocL1').val();
  let tbDocL2 = $('#tbDocL2').val();
  let tbDocL3 = $('#tbDocL3').val();
  let tbDocL4 = $('#tbDocL4').val();
  let tbDocL5 = $('#tbDocL5').val();
  let tbDocCardImage = $('#tbDocCardImage').val();

  // let nameCheck = $("#tbDocName").val()
  // console.log("Name check " + nameCheck)
  // if (nameCheck == '' || nameCheck == ' ') {
  //     alert("You must enter a Document Name!")
  //     return
  // }

  /* build up an object */
  var obj = {
    tbDocName,
    tbDocCustomerID,
    tbDocBriefDescription,
    tbDocType,
    tbDocNameShort,
    tbDocHTMLContent,
    tbDocGroup,
    tbDocPurpose,
    tbDocOwner,
    tbDocPopular,
    tbDocSortOrder,
    tbDocLastModified,
    tbDocApprovedBy,
    tbDocStatus,
    tbDocWrittenBy,
    tbDocL1,
    tbDocL2,
    tbDocL3,
    tbDocL4,
    tbDocL5,
    tbDocCardImage,
  };

  /* The fetch section begins here */

  //   fetch(`${apiUrl}/tbdocuments`, {
  //     method: 'POST',
  //     body: JSON.stringify(obj),
  //   })
  //     .then(res => {
  //       return res.json();
  //     })
  //     .then(res => {
  //       if (!res.success) return displayFailureMessageDB(res.error);
  //       console.log('Create DOC', { res });
  //     })
  //     .catch(err =>
  //       displayFailureMessageDB(
  //         'May be duplicate document name. System error: ' + err.message,
  //       ),
  //     );

  /* The fetch section ends here */

  let newItemId = '';
  /*  add the Document */
  openIDBGetStore('tbDocuments', 'readwrite', function(store) {
    var request;
    try {
      request = store.add(obj);
    } catch (e) {
      displaySuccessMessageDB(
        'Some Problem While adding. System error: ' + e.error,
      );
      throw e;
    }
    request.onsuccess = function(e) {
      // return the id of the newly added item
      newItemId = request.result;

      // now fetch the new record, and generate template stuff
      var request2 = store.get(newItemId);
      request2.onsuccess = function(e) {
        var data = e.target.result;

        //  console.log("data " + JSON.stringify(data))

        let ids = buildIdsDocuments(data.id);
        displayDT = $('#displayDtDocuments');

        displayDT.append(generateDatatableDocuments(data, ids));
        fillDocumentsForm(data, ids.documentID, ids.editID); //not sure why this was here ??, maybe for insterting new into dom
        //   deleteDocument(data, ids.listItemID, ids.deleteID);
        // update tbDocID field to match indexedb key id
        let fieldName = 'tbDocID';
        let newVal = 'new' + String(data.id);
        let uid = Number(data.id); // keys are numbers in idb, we find the new record by key and update the tbid
        setTbidBasedOnKeyUniversal('tbDocuments', uid, fieldName, newVal);

        console.log('ids ' + JSON.stringify(ids));
        displaySuccessMessageDB(
          'createDocument Result: ' + JSON.stringify(newVal) + ' added!',
        );

        var myform = document.getElementById('launchDocumentsForm');
        var myform = myform.setAttribute('data-tbid', newVal);

        $('#tbDocID').val(newVal);
        $('#tbDocName').val(data.tbDocName);
        // activate the labels on the form
        M.updateTextFields();
      };
    };
    request.onerror = function() {
      displayFailureMessageDB(
        'May be duplicate document name. System error: ' + this.error,
      );
    };
  });
  launchTbBlogStandardL1New();
}

// do not delete rows from here, they get deleted if deleting timebars rows
export const deleteDocument = (document, listItemID, deleteID) => {
  // note may have to modify this to suit documentid as string for now tbDocID and built in id must match
  //  console.log("document.tbDocID: " + document.tbDocID + " check!")
  var indexName = 'tbDocID';
  var documentid = document.tbDocID;
  // console.log("documentid " + documentid)

  let deleteBtn = $(`#${deleteID}`);
  deleteBtn.click(() => {
    deleteRowFromIDBUniversal('tbDocuments', indexName, documentid);

    $(`#${listItemID}`).remove();
    displaySuccessMessageDB(
      'deleteDocument Result: ' + listItemID + ' deleted!',
    );
  });
};

//  get one fill form first, put show details button, then edit button
export const fillDocumentsForm = (document, documentID, editID) => {
  //  console.log("fillDocumentsForm - document.tbDocID " + document.tbDocID)  // returns the id without underscore
  //  console.log("fillDocumentsForm - documentID " + documentID)  // document_70
  // console.log("fillDocumentsForm - editID " + editID)  //edit_70

  // draw the form first as component
  let targetDiv = '#divTbPagesL1';
  docFormHTMLComponent(targetDiv);

  let editBtn = $(`#${editID}`);
  editBtn.click(() => {
    if (!document.tbDocID) return alert('Not a valid key, check for string');
    /* fetch section begins */
    fetch(`${API_URL}/tbdocuments/tbdocid/${document.tbDocID}`)
      .then(res => res.json())
      .then(res => {
        if (res.success === false) return alert(res.error);

        const { data } = res;
        if (!data) return alert('Not a valid key, check for string');

        $('#tbDocID').val(data.tbDocID);
        $('#tbDocName').val(data.tbDocName);
        $('#tbDocCustomerID').val(data.tbDocCustomerID);
        $('#tbDocBriefDescription').val(data.tbDocBriefDescription);
        $('#tbDocType').val(data.tbDocType);
        $('#tbDocNameShort').val(data.tbDocNameShort);

        getEditorDataContentMain(data.tbDocHTMLContent);
        //  $("#tbDocHTMLContent").html(data.tbDocHTMLContent);

        $('#tbDocGroup').val(data.tbDocGroup);
        $('#tbDocPurpose').val(data.tbDocPurpose);
        $('#tbDocOwner').val(data.tbDocOwner);
        $('#tbDocPopular').val(data.tbDocPopular);
        $('#tbDocSortOrder').val(data.tbDocSortOrder);
        $('#tbDocLastModified').val(data.tbDocLastModified);
        $('#tbDocApprovedBy').val(data.tbDocApprovedBy);
        $('#tbDocStatus').val(data.tbDocStatus);
        $('#tbDocWrittenBy').val(data.tbDocWrittenBy);
        $('#tbDocL1').val(data.tbDocL1);
        $('#tbDocL2').val(data.tbDocL2);
        $('#tbDocL3').val(data.tbDocL3);
        $('#tbDocL4').val(data.tbDocL4);
        $('#tbDocL5').val(data.tbDocL5);
        $('#tbDocCardImage').val(data.tbDocCardImage);

        //  console.log("data sent to form: " + JSON.stringify(data))
        displaySuccessMessageDB('Form opened, ready for editing!');

        launchDocumentsForm(documentid);

        // activate the labels on the form
        M.updateTextFields();
        // open or resize textarea fields
        M.textareaAutoResize($('#tbDocBriefDescription'));
        // to make this work, simply add class data-length="250" to text area or input
        $(
          'input#tbDocNameShort, textarea#tbDocBriefDescription',
        ).characterCounter();
      })
      .catch(() => alert('There was an error getting store id'));
    /* fetch section ends */

    // openIDBGetStore('tbDocuments', 'readonly', function(store) {
    //   // var request = store.get(document.tbDocID);
    //   var documentid = String(document.tbDocID);

    //   var index = store.index('tbDocID');
    //   var request = index.get(documentid);

    //   request.onerror = function(event) {
    //     alert('there was error getting store id');
    //   };
    //   request.onsuccess = function(event) {
    //     // here we are returning data to the form
    //     var data = request.result;
    //     if (!data) {
    //       alert('not a valid key, check for string');
    //       return;
    //     }
    //     // populate the form fields

    //     $('#tbDocID').val(data.tbDocID);
    //     $('#tbDocName').val(data.tbDocName);
    //     $('#tbDocCustomerID').val(data.tbDocCustomerID);
    //     $('#tbDocBriefDescription').val(data.tbDocBriefDescription);
    //     $('#tbDocType').val(data.tbDocType);
    //     $('#tbDocNameShort').val(data.tbDocNameShort);

    //     getEditorDataContentMain(data.tbDocHTMLContent);
    //     //  $("#tbDocHTMLContent").html(data.tbDocHTMLContent);

    //     $('#tbDocGroup').val(data.tbDocGroup);
    //     $('#tbDocPurpose').val(data.tbDocPurpose);
    //     $('#tbDocOwner').val(data.tbDocOwner);
    //     $('#tbDocPopular').val(data.tbDocPopular);
    //     $('#tbDocSortOrder').val(data.tbDocSortOrder);
    //     $('#tbDocLastModified').val(data.tbDocLastModified);
    //     $('#tbDocApprovedBy').val(data.tbDocApprovedBy);
    //     $('#tbDocStatus').val(data.tbDocStatus);
    //     $('#tbDocWrittenBy').val(data.tbDocWrittenBy);
    //     $('#tbDocL1').val(data.tbDocL1);
    //     $('#tbDocL2').val(data.tbDocL2);
    //     $('#tbDocL3').val(data.tbDocL3);
    //     $('#tbDocL4').val(data.tbDocL4);
    //     $('#tbDocL5').val(data.tbDocL5);
    //     $('#tbDocCardImage').val(data.tbDocCardImage);

    //     //  console.log("data sent to form: " + JSON.stringify(data))
    //     displaySuccessMessageDB('Form opened, ready for editing!');

    //     launchDocumentsForm(documentid);

    //     // activate the labels on the form
    //     M.updateTextFields();
    //     // open or resize textarea fields
    //     M.textareaAutoResize($('#tbDocBriefDescription'));
    //     // to make this work, simply add class data-length="250" to text area or input
    //     $(
    //       'input#tbDocNameShort, textarea#tbDocBriefDescription',
    //     ).characterCounter();
    //   };
    // });
  });
};

export function getEditorDataContentMain(editorData) {
  ClassicEditor.create(document.querySelector('#tbDocHTMLContent'), {})
    .then(editorContentMain => {
      editorContentMain.setData(editorData);
      window.editorContentMain = editorContentMain;
      editorContentMain.model.document.on('change:data', () => {
        console.log('The data has changed!');
        let formName = 'blog';
        let fieldName = 'tbDocHTMLContent';
        let fieldID = '#tbDocHTMLContent';
        let latestText = window.editorContentMain.getData();
        UpdatableStd(formName, fieldName, latestText);
      });
    })
    .catch(error => {
      console.error(error);
    });
}

// univ launch form after drawn from my HTMLComponent with materialize javascript
function launchDocumentsForm(documentid) {
  // set data-tbid on form for later actions
  var myform = document.getElementById('launchDocumentsForm');
  var myform = myform.setAttribute('data-tbid', documentid);

  var elem = document.querySelector('#launchDocumentsForm');
  var options = {
    dismissible: false,
  };
  var instance = M.Modal.init(elem, options);
  instance.open();
  //  console.log("launchEmptyDocumentsForm finished")
}

// save the edit - only certain fields are editable, not all
export const saveFormDataDocument = () => {
  // first get form data
  let tbDocID = $('#tbDocID').val();

  let tbDocName = $('#tbDocName').val();
  let tbDocCustomerID = $('#tbDocCustomerID').val();
  let tbDocBriefDescription = $('#tbDocBriefDescription').val();
  let tbDocType = $('#tbDocType').val();
  let tbDocNameShort = $('#tbDocNameShort').val();

  //  let tbDocHTMLContent = $("#tbDocHTMLContent").html() //getEditorData()

  let tbDocGroup = $('#tbDocGroup').val();
  let tbDocPurpose = $('#tbDocPurpose').val();
  let tbDocOwner = $('#tbDocOwner').val();
  let tbDocPopular = $('#tbDocPopular').val();
  let tbDocSortOrder = $('#tbDocSortOrder').val();
  let tbDocLastModified = $('#tbDocLastModified').val();
  let tbDocApprovedBy = $('#tbDocApprovedBy').val();
  let tbDocStatus = $('#tbDocStatus').val();
  let tbDocWrittenBy = $('#tbDocWrittenBy').val();

  let tbDocL1 = $('#tbDocL1').val();
  let tbDocL2 = $('#tbDocL2').val();
  let tbDocL3 = $('#tbDocL3').val();
  let tbDocL4 = $('#tbDocL4').val();
  let tbDocL5 = $('#tbDocL5').val();
  let tbDocCardImage = $('#tbDocCardImage').val();

  openIDBGetStore('tbDocuments', 'readwrite', function(store) {
    var documentid = $('#tbDocID').val();
    console.log('tbDocIDzz: ' + documentid);
    documentid = String(documentid);

    var index = store.index('tbDocID');
    var request = index.get(documentid);
    request.onerror = function(event) {
      // Handle errors!
      alert('there was error getting store id');
    };
    request.onsuccess = function(event) {
      // Get the exist value that we want to update
      var data = request.result;
      //    console.log("request.result " + JSON.stringify(data))

      try {
        // apply editable data fields on form to the data object
        data.tbDocID = tbDocID;
        data.tbDocName = tbDocName;
        data.tbDocCustomerID = tbDocCustomerID;
        data.tbDocBriefDescription = tbDocBriefDescription;
        data.tbDocType = tbDocType;
        data.tbDocNameShort = tbDocNameShort;
        //  data.tbDocHTMLContent = tbDocHTMLContent
        data.tbDocGroup = tbDocGroup;
        data.tbDocPurpose = tbDocPurpose;
        data.tbDocOwner = tbDocOwner;
        data.tbDocPopular = tbDocPopular;
        data.tbDocSortOrder = tbDocSortOrder;
        data.tbDocLastModified = tbDocLastModified;
        data.tbDocApprovedBy = tbDocApprovedBy;
        data.tbDocStatus = tbDocStatus;
        data.tbDocWrittenBy = tbDocWrittenBy;

        data.tbDocL1 = tbDocL1;
        data.tbDocL2 = tbDocL2;
        data.tbDocL3 = tbDocL3;
        data.tbDocL4 = tbDocL4;
        data.tbDocL5 = tbDocL5;
        data.tbDocCardImage = tbDocCardImage;
      } catch (e) {
        console.log('saveFormDataDocument - fail');
        throw e;
      }
      // Put this updated object back into the database.
      var requestUpdate = store.put(data);
      requestUpdate.onerror = function(event) {
        // Do something with the error
        alert('there was error updating progress in idb');
      };
      requestUpdate.onsuccess = function(event) {
        console.log('Success - Form is updated!');

        // $("#tbid").text(tbDocID);  // universal input on all forms used on focdcommon
        $(`#tbDocID-${documentid}`).text(tbDocID);
        $(`#tbDocName-${documentid}`).text(tbDocName);
        $(`#tbDocCustomerID-${documentid}`).text(tbDocCustomerID);
        $(`#tbDocBriefDescription-${documentid}`).text(tbDocBriefDescription);
        $(`#tbDocType-${documentid}`).text(tbDocType);
        $(`#tbDocNameShort-${documentid}`).text(tbDocNameShort);
        ///   $(`#tbDocHTMLContent-${documentid}`).text(tbDocHTMLContent);
        $(`#tbDocGroup-${documentid}`).text(tbDocGroup);
        $(`#tbDocPurpose-${documentid}`).text(tbDocPurpose);
        $(`#tbDocOwner-${documentid}`).text(tbDocOwner);
        $(`#tbDocPopular-${documentid}`).text(tbDocPopular);
        $(`#tbDocSortOrder-${documentid}`).text(tbDocSortOrder);
        $(`#tbDocLastModified-${documentid}`).text(tbDocLastModified);
        $(`#tbDocApprovedBy-${documentid}`).text(tbDocApprovedBy);
        $(`#tbDocStatus-${documentid}`).text(tbDocStatus);
        $(`#tbDocWrittenBy-${documentid}`).text(tbDocWrittenBy);

        $(`#tbDocL1-${documentid}`).text(tbDocL1);
        $(`#tbDocL2-${documentid}`).text(tbDocL2);
        $(`#tbDocL3-${documentid}`).text(tbDocL3);
        $(`#tbDocL4-${documentid}`).text(tbDocL4);
        $(`#tbDocL5-${documentid}`).text(tbDocL5);
        $(`#tbDocCardImage-${documentid}`).text(tbDocCardImage);
      };
    }; // end onsuccess
  });
};

export const resetDocumentsInput = () => {
  $('#tbDocID').val();
  $('#tbDocID').val();
  $('#tbDocName').val();
  $('#tbDocCustomerID').val();
  $('#tbDocBriefDescription').val();
  $('#tbDocType').val();
  $('#tbDocNameShort').val();
  $('#tbDocHTMLContent').val();
  $('#tbDocGroup').val();
  $('#tbDocPurpose').val();
  $('#tbDocOwner').val();
  $('#tbDocPopular').val();
  $('#tbDocSortOrder').val();
  $('#tbDocLastModified').val();
  $('#tbDocApprovedBy').val();
  $('#tbDocStatus').val();
  $('#tbDocWrittenBy').val();
  $('#tbDocL1').val();
  $('#tbDocL2').val();
  $('#tbDocL31').val();
  $('#tbDocL4').val();
  $('#tbDocL5').val();
  $('#tbDocCardImage').val();
};

const buildIdsDocuments = itemid => {
  return {
    editID: 'edit_' + itemid,
    deleteID: 'delete_' + itemid,
    listItemID: 'listItem_' + itemid,
    documentID: 'document_' + itemid,
    bpselectorID: 'bpselector_' + itemid,
  };
};

const displayDocuments = data => {
  data.forEach(Item => {
    let ids = buildIdsDocuments(Item.tbDocID);
    //  console.log("displayDocuments ids: " + JSON.stringify(ids))
    // looks like this ids: {"editID":"edit_83","deleteID":"delete_83","listItemID":"listItem_83","documentID":"document_83","bpselectorID":"bpselector_83"}
    displayDT = $('#displayDtDocuments');
    displayDT.append(generateDatatableDocuments(Item, ids));
    fillDocumentsForm(Item, ids.documentID, ids.editID);
    deleteDocument(Item, ids.listItemID, ids.deleteID);
  });
};

function UpdatableStd(formName, fieldName, theText) {
  // this is standard pattern how to get current bar id (data-tbid value is set when form opens)

  var mybar = document.getElementById('launchDocumentsForm');
  var tbID = mybar.getAttribute('data-tbid');

  console.log('tbID mdAdv ' + tbID);

  if (fieldName.startsWith('tbMD') || fieldName.startsWith('tbmd')) {
    updateOneMetadataField(tbID, fieldName, theText);

    console.log('update one md filed ' + theText);
  } else if (fieldName.startsWith('tbDoc') || fieldName.startsWith('tbdoc')) {
    console.log('updateOneDocumentField ' + fieldName);

    updateOneDocumentField(tbID, fieldName, theText);
  } else {
    updateOneFieldTimebarIDB(tbID, fieldName, theText);
  }
}

function updateOneMetadataField(tbMDID, fieldName, theText) {
  var tbMDID = String(tbMDID);
  openIDBGetStore('tbMetaData', 'readwrite', function(store) {
    console.log('updateOneMetadataField tbMDID ' + tbMDID);

    // var request = store.get(key);
    var index = store.index('tbMDID');
    var request = index.get(tbMDID);

    request.onerror = function(event) {
      alert('No matching record found, or other error');
    };
    request.onsuccess = function(event) {
      // // note.innerHTML += '<li> opened store tbTimebars from CRUD.js</li>';
      var data = request.result;

      // data['tbMDDescription'] = "newValue1;"
      data[fieldName] = theText;
      var requestUpdate = store.put(data);

      requestUpdate.onerror = function(event) {
        alert('there was error updating data');
      };
      requestUpdate.onsuccess = function(event) {
        console.log('successfully Updated ' + data.tbMDID);
      };
    };
  });
}

function updateOneDocumentField(tbDocID, fieldName1, newValue1) {
  //alert("key " + key)
  console.log('tbDocID ' + tbDocID);
  var tbDocID = String(tbDocID);
  var newValue1 = String(newValue1);

  openIDBGetStore('tbDocuments', 'readwrite', function(store) {
    // var request = store.get(key);
    var index = store.index('tbDocID');
    var request = index.get(tbDocID);

    request.onerror = function(event) {
      alert('No matching record found, or other error');
    };
    request.onsuccess = function(event) {
      // // note.innerHTML += '<li> opened store tbTimebars from CRUD.js</li>';
      var data = request.result;

      data[fieldName1] = newValue1;

      //   console.log("tbDocID  " + data.tbDocID)

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function(event) {
        alert('there was error updating data');
      };
      requestUpdate.onsuccess = function(event) {
        //  // note.innerHTML += '<li>successfully Updated </li>';
        console.log(
          'succes updating fieldNamed: ' +
            fieldName1 +
            ' on tbDocID: ' +
            data.tbDocID,
        );
      };
    };
  });
}

// used to delete data in idb so we can import excel data
export function clearStoreTable(storeName) {
  console.log('clearStoreTable - storeName', storeName);

  /* fetch section begins */
  fetch(`${API_URL}/${storeName.toLowerCase()}`, {
    method: 'DELETE',
    headers: {
      Authorization: 'Here goes the token gained from login',
    },
  })
    .then(res => {
      if (res.ok) displaySuccessMessageDB(`Cleared all: ${storeName}. <br> `);
    })
    .catch(err =>
      displayFailureMessageDB(
        'Delete from: ' + storeName + ' failed ' + err.message,
      ),
    );
  /* fetch section ends */

  // openIDBGetStore(storeName, 'readwrite', function(store) {
  //   var req = store.clear();
  //   req.onsuccess = function(evnt) {
  //     console.log('done clearing: ' + storeName);
  //     displaySuccessMessageDB(`Cleared all: ${storeName}. <br> `);
  //   };
  //   req.onerror = function(evnt) {
  //     console.log('Did not clear: ' + storeName);
  //     displayFailureMessageDB(
  //       'Delete from: ' + storeName + ' failed ' + this.error,
  //     );
  //   };
  // });
}

// step 2 draw or make mctt MultiColumnTagTable Table

export function makeMcttMetaDataOrRes(
  tagGroup,
  tbID,
  formName,
  fieldName,
  leftpx,
  toppx,
  sourceclass,
) {
  console.log('makeMcttMetaDataOrRes ' + tbID);
  // sourceclass = 'resource'
  // draw Datatable header
  var strVar = '';
  strVar += '    <div id="mcttHeader"><h4>Tag Table </h4>';
  strVar += '    <div>';
  strVar +=
    '        <input class="waves-effect waves-light btn-small blue darken-4 tbButtonSmall FloatRight" type="button" id="btnCloseMultiColumnTagTable" value="Close" />';
  strVar += '    </div>';
  strVar += '    <p>Tap to assign value!</p></div>';
  strVar += '    <div id="mcttTagListDiv"></div>';
  var mcttContainer = $('#mcttContainer');
  mcttContainer.empty();
  mcttContainer.append(strVar);
  strVar = '';

  // generate special id that we can split later to get tbid and field id
  var tagFormID = tbID + '-' + fieldName;
  console.log('tagFormID ' + tagFormID);

  // draw datatable mctt
  var list = $('#mcttTagListDiv');
  list.empty();
  var $table = $(
    '<table class="display compact mcttTapRow " id="' + tagFormID + '">',
  );
  // $table.append('<thead><tr"><th>Class</th><th>Name</th></tr></thead><tbody>') // uncomment for two columsn in tag table
  $table.append('<thead><tr"><th></th><th>Name</th></tr></thead><tbody>');

  // want to be able toget tag values from the tag table or the res table based on source class
  if (sourceclass == 'md' || sourceclass == 'metadata') {
    var table = 'tbTags';
    var indexName = 'tbTagGroup';
    var filterValue = tagGroup;
    openIDBGetStore(table, 'readonly', function(store) {
      var index = store.index(indexName);
      var range = IDBKeyRange.only(filterValue);
      index.openCursor(range).onsuccess = function(event) {
        var cursor = event.target.result;
        if (cursor) {
          $table.append(
            '<tr id="mcttKey' +
              cursor.value.tbTagID +
              '"><td>' +
              '' +
              '</td><td>' +
              cursor.value.tbTagName +
              '</td></tr>',
          );
          cursor.continue();
        } else {
          $table.append('</tbody></table>');
          list.append($table);

          makeMcttMetaDataOrResDataTable(tagFormID);
        }
      };
    }); // end on success of db // end on success of db
  } else if (sourceclass == 'people' || sourceclass == 'resource') {
    var table = 'tbResources';
    var indexName = 'tbResResourceType';
    var filterValue = 'Human';

    openIDBGetStore(table, 'readonly', function(store) {
      var index = store.index(indexName);
      var range = IDBKeyRange.only(filterValue);
      index.openCursor(range).onsuccess = function(event) {
        var cursor = event.target.result;
        if (cursor) {
          $table.append(
            '<tr id="mcttKey' +
              cursor.value.tbTagID +
              '"><td>' +
              cursor.value.tbResDepartment +
              '</td><td>' +
              cursor.value.tbResName +
              '</td></tr>',
          );

          cursor.continue();
        } else {
          $table.append('</tbody></table>');
          list.append($table);

          makeMcttMetaDataOrResDataTable(tagFormID);
        }
        // can add functions to run here after building objects
      };
    }); // end on success of db // end on success of db
  }
  // position the pick list near place clicked

  $('#mcttContainer').css('left', leftpx);
  $('#mcttContainer').css('top', toppx);
}

// step 3 mctt make it a datatable
export function makeMcttMetaDataOrResDataTable(tagFormID) {
  //console.log("how many times")
  var tagFormID = '#' + tagFormID;
  $(tagFormID).DataTable({
    // "dom": '<"top"i>rt<"bottom"flp><"clear">',
    dom: '<"top"f>rt<"bottom"ilp>',

    /*         "paging": false,
                "ordering": true,
                "order": [
                    [1, "asc"]
                ],
                "info": false,
                "scrollY": "400px",
               "scrollCollapse": true, */
  });
  //"searching": false,
}

// START export to csv files

export function createCsvFile(storeOrTable) {
  returnAllStoreRowsForBackupToCSV(storeOrTable, function(fnCallbackJSON) {
    const fields = ['tbSelfKey2', 'tbName'];
    let fileName = storeOrTable + '.csv';
    let myjsondata = fnCallbackJSON;
    let myJsonarray = '[' + myjsondata + ']';
    //let myJsonarray = '{"' + storeOrTable + '":[' + myjsondata + ']}'
    // console.log(myJsonarray);

    myJsonarray = JSON.parse(myJsonarray);

    try {
      var mycsvdata = json2csv(myJsonarray, fields);
      // console.log(myJsonarray);
      displaySuccessMessageDB(
        'Successfully converted local data to CSV for tbTimebars Table',
      );
    } catch (err) {
      // Errors are thrown for bad options, or if the data is empty and no fields are provided.
      // Be sure to provide fields if it is possible that your data array will be empty.
      console.error(err);
      displayFailureMessageDB(
        'Failed to convert local data to CSV for tbTimebars Table, Error: ' +
          err,
      );
    }
    //  save to download folder
    let blob = new Blob([mycsvdata], { type: 'text/plain' });
    saveAs(blob, fileName);
  });
}

function returnAllStoreRowsForBackupToCSV(storeOrTable, fnCallbackJsonAll) {
  console.log('returnAllStoreRowsForBackupToCSV - storortable', storeOrTable);
  // let jsonString = ''; // this will by my array of json objects from idb
  // let jsonStrings = '';

  /* fetch section begins */
  fetch(`${API_URL}/${storeOrTable.toLowerCase()}`)
    .then(res => res.json())
    .then(({ data }) => {
      if (!data) return console.warn('Some error while fetching data');

      let jsonString = '';
      data.forEach((ele, index) => {
        delete ele._id;
        delete ele.__v;
        if (index === data.length - 1) jsonString += JSON.stringify(ele);
        else jsonString += JSON.stringify(ele) + ',';
      });

      fnCallbackJsonAll(jsonString);
    })
    .catch(console.error);
  /* fetch section ends */

  // openIDBGetStore(storeOrTable, 'readonly', function(store) {
  //   store.openCursor().onsuccess = function(event) {
  //     let cursor = event.target.result;
  //     if (cursor) {
  //       jsonString = JSON.stringify(cursor.value);
  //       jsonStrings = jsonStrings += jsonString;
  //       cursor.continue();
  //       jsonStrings = jsonStrings += ',';
  //     } else {
  //       // remove the trailing comma
  //       jsonStrings = jsonStrings.replace(/,$/, '');
  //       // console.log("done looping through object store" + jsonStrings)
  //       fnCallbackJsonAll(jsonStrings);
  //     }
  //   }; // end open curso
  // }); // end get store
}

// now create the files for each store
export function exportAllDataAsCSV() {
  createCsvFile(STORE_ADMINPANEL);
  createCsvFile(STORE_DOCS);
  createCsvFile(STORE_RESOURCES);
  createCsvFile(STORE_TAGS);
}
