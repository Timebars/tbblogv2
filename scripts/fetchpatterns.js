
const url = '/'


//fetch on our API can result in one of the following four outcomes:

//Network error
//Parsing error (Unreadable JSON from the server)
//Business logic error (Readable JSON error message from the server)
//Success

// the problem that the response and json objects aren't available in the same context. Does Promise.all() fix that?.
// Network- and parsing errors causes exceptions and it would be nice to unify our error handling. 
// Throw an Error when you get a business logic error message from the server. That way all types of errors end up in the catch-method.
// Business logic error messages will be provided by the server but the network- and parsing error messages must be provided by us. 
// Note that network- and parsing error messages are provided in place and that business logic error messages 
//  are grabbed from the exception which in turn was created using the message provided by the server.




// START POST Create Timebars


export function PostTimebars(storeOrTable, fnCallbackJsonAll) {

    // this is how i was thinking about bult creating data in the mongo db.
    openIDBGetStore(storeOrTable, 'readonly', function (store) {
        store.openCursor().onsuccess = function (event) {
            let cursor = event.target.result;
            if (cursor) {
                cursor.continue();

                //      console.log("tbname: " + cursor.value.tbName)

                let tbID = cursor.value.tbID
                let tbName = cursor.value.tbName
                let tbL1 = cursor.value.tbL1
                let tbL2 = cursor.value.tbL2
                let tbL3 = cursor.value.tbL3


                PostTimebarsStep2(tbID, tbName, tbL1, tbL2, tbL3)

            } else {

            }
        } // end open curso
    }); // end get store
};

function PostTimebarsStep2(tbID, tbName, tbL1, tbL2, tbL3) {

    console.log("called createNewTimebarFromNew: " + tbName)

    try {
        let response = fetch(`${url}/tbcore`, {
            method: 'POST',
            headers: {
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                tbname: tbName,
                tbid: tbID,
                tbl1: tbL1,
                tbl2: tbL2,
                tbl3: tbL3
            })
        })

            .catch(handleError) // handle network issues
            .then(checkStatus)
            .then(parseText)
            .catch(error => {
                throw error;
            })
            //  do something with response

            .then((data) => {
                console.log("data ret" + JSON.stringify(data))

                M.toast({
                    html: `data posted ${data}`,
                    classes: 'green darken-3 rounded'
                });

            })



    } catch (err) {
        // err is of type {status /*number*/, ok /*boolean*/, statusText /*string*/, body /*object*/}
        console.log("error " + err)
    }

}


// END POST Create Timebars






// START DELETE Timebars


export function DeleteAllTimebars() {
    try {
        const response = fetch(`${url}/tbcore`, {
            method: 'DELETE'
        })
            .catch(handleError) // handle network issues
            .then(checkStatus)
            .then(parseJSON)
            .catch(error => {
                throw error;
            });
        // do something with response
    } catch (err) {
        // err is of type {status /*number*/, ok /*boolean*/, statusText /*string*/, body /*object*/}
    }
}

// END DELETE Timebars





// START Fetch standard error handling

const checkStatus = response => {
    if (response.status >= 200 && response.status < 300) {
        console.log("status :" + response.status)
        return response;
    }

    return response.text().then(text => {

        console.log("statusText :" + response.statusText)

        M.toast({
            html: `Somthing wrong, likely duplicate data: ${response.statusText}`,
            classes: 'red darken-3 rounded'
        });

        return Promise.reject({
            status: response.status,
            ok: false,
            statusText: response.statusText,
            body: text
        });
    });
};

const parseText = response => {
    if (response.status === 204 || response.status === 205) {
        return null;
    }
    return response.text();
};

const handleError = error => {
    error.response = {
        status: 0,
        statusText:
            "Cannot connect. Please make sure you are connected to internet."
    };
    throw error;
};

// END Fetch std error

