
// was the todo app from noob coder
// was doc datatableuniversal
// refactored to use tb and idb


/* idb code */
import { displayFailureMessageDB, displaySuccessMessageDB, handleJSONFileDropforUpload, openIDBGetStore, setTbidBasedOnKeyUniversal, deleteRowFromIDBUniversal, createJsonFileSimple } from './tbdatabase';





// global variables
let displayDT = "";
//const message = $("#msgReporting");
const storeName = 'tbTags'



$(document).on('click', '#btnBackupTagData', function (e) {
    //  console.log("clicked btnBackupMetadata")
    createJsonFileSimple(storeName)
});


// get and display resources in grid from menus
// replace this with pattern in focddocuments.js
$(document).on('click', '.launchTbTags', function () {

    $("#divTbPagesL1").css("visibility", "visible");
    // $("#divTbPagesL1").empty()
    $("#divTbPagesL1").empty()
    console.log(" here launchTbTags click")
    let targetDiv = '#divTbPagesL1'
    drawTbTags()
});



function drawTbTags() {

    setTimeout(() => {
        let targetDiv = '#divTbPagesL1'
        dtTagTableHeaderHTMLComponent(targetDiv)

    }, 50);

    setTimeout(() => {
        //   console.log("page load get Document")
        getTags()
    }, 200);

    setTimeout(() => {
        //   console.log("page load get Document")
        initDtTags()

        let dropdiv = 'tbTagsComponentL2'
        handleJSONFileDropforUpload(dropdiv)

    }, 400);
};


// set up the column names for data table
export function dtTagTableHeaderHTMLComponent(targetDiv) {

    let td = $(targetDiv);
    let dttaghtml = `
    <!-- ------ START Tags core univ data table area -->
    <div id="tbTagsComponentL2" class="dmDropArea">

       <div style="margin-top: 5px; font-size: 24px;">Tags Listing

          <span style="margin-left: 300px; font-size: 12px;"></span>
       </div>

       <button id="btnLaunchEmptyTagsForm" class="btn-small waves-effect waves-light grey darken-4 right">New
          Item
       </button>
       <button id="btnBackupTagData" style="margin-right: 5px;"
          class="btn-small waves-effect waves-light grey darken-4 right"> Data
          Backup
       </button>

    </div>

    <table id="dataTableTags" class="displayzz" style="width:100%">
       <thead>
          <tr>
             <th></th>
             <th>Id</th>
             <th>Name</th>
             <th>Group</th>
             <th>Short Name</th>
             <th>Purpose</th>
             <!--  <th>Description</th> -->
             <th>Owner</th>
             <th>Popularity</th>
             <th>Sort</th>
             <!--     <th>Cust ID</th> -->
             <!--    <th>Ext ID</th> -->
             <th>Delete</th>
          </tr>
       </thead>
       <tbody id="displayDtTags">
       </tbody>
       </tfoot>
    </table>
    <!-- -------- END Tags data table area -->
          `
    td.append(dttaghtml)

};




// ************************ moved to database Drag and drop upload ***************** //

// function handleJSONFileDropforUpload(targetDiv) {

//     console.log(" div " + targetDiv)

//     let dmDropArea = document.getElementById(targetDiv);
//     // Prevent default drag behaviors

//     ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(item => {
//         dmDropArea.addEventListener(item, preventDefaults, false)
//         document.body.addEventListener(item, preventDefaults, false)
//     });
//     // Highlight drop area when item is dragged over it

//     ['dragenter', 'dragover'].forEach(item => {
//         dmDropArea.addEventListener(item, highlight(dmDropArea), false)
//     });

//     ['dragleave', 'drop'].forEach(item => {
//         dmDropArea.addEventListener(item, unhighlight(dmDropArea), false)
//     });


//     // Handle dropped files
//     dmDropArea.addEventListener('drop', handleDmDrop, false);
// }


// function preventDefaults(e) {
//     e.preventDefault()
//     e.stopPropagation()
// };

// function highlight(dmDropArea, e) {
//     dmDropArea.classList.add('highlight')
// };

// function unhighlight(dmDropArea, e) {
//     dmDropArea.classList.remove('highlight')

// };

// function handleDmDrop(e) {

//     let dmFiles = e.dataTransfer.files;

//     var selectedFile = ""
//     var storeName = ""

//     //   $("#dataControllerUploadBox").text(dmFiles.length + " file(s) dropped, data uploaded and backup taken!");
//     //  $("#tbCanvas").text(dmFiles.length + " file(s) dropped, data uploaded and backup taken!");
//     //  $("#msg").text(dmFiles.length + " file(s) dropped, data uploaded and backup taken!");

//     // for each file, get storname and populate idb
//     for (var j = 0; j < dmFiles.length; j++) {

//         var fname = (dmFiles[j].name).substr(0, 6);
//         console.log("fname : " + fname)

//         if (fname == "later") {
//             storeName = "later"
//         } else if (fname == "tbTime") {
//             storeName = "tbTimebars"
//         } else if (fname == "tbTags") {
//             storeName = "tbTags"
//         } else if (fname == "tbReso") {
//             storeName = "tbResources"
//         } else if (fname == "tbMeta") {
//             storeName = "tbMetaData"
//         } else if (fname == "tbAdmi") {
//             storeName = "tbAdminPanel"
//         } else if (fname == "tbResC") {
//             storeName = "tbResCalcs"
//         } else if (fname == "tbDocu") {
//             storeName = "tbDocuments"
//         } else if (fname == "tbAddT") {
//             storeName = "tbAddTimebars"
//         } else {
//             storeName = "NA"
//         }

//         selectedFile = dmFiles[j];
//         console.log("filename : " + fname)

//         OnDragProcessJSONFiles(selectedFile, storeName)

//     };
//     // 
// }

// /* END drag drop to upload */


// appends to dataTableTags
const generateDatatableTags = (Item, ids) => {
    return `<tr id="${ids.listItemID}">
    <td><a title="Edit this Tag" id="${ids.editID}"><i class="material-icons tiny">edit</i></a></td>
        <td id="tbTagID-${Item.tbTagID}"><div>${Item.tbTagID}</div></td>
        <td id="tbTagName-${Item.tbTagID}"><div style="width:200px">${Item.tbTagName}</div></td>
        <td id="tbTagGroup-${Item.tbTagID}"><div style="width:150px">${Item.tbTagGroup}</div></td>
        <td id="tbTagNameShort-${Item.tbTagID}"><div style="width:100px">${Item.tbTagNameShort}</div></td>
        <td id="tbTagPurpose-${Item.tbTagID}"><div style="width:200px">${Item.tbTagPurpose}</div></td>
        
        <td id="tbTagOwner-${Item.tbTagID}">${Item.tbTagOwner}</div></td>
        <td id="tbTagPopular-${Item.tbTagID}">${Item.tbTagPopular}</div></td>
        <td id="tbTagSortOrder-${Item.tbTagID}">${Item.tbTagSortOrder}</div></td>

        <td><button title="Delete this print job" type="button" class="btn-flat" id="${ids.deleteID}"><i class="material-icons">delete_forever</i></button></td>
        </tr>`;
}

// feields removed July 27 2019
// <td id="tbTagDescription-${Item.tbTagID}"><div style="width:150px">${Item.tbTagDescription}</div></td>
//         <td id="tbTagCustomerID-${Item.tbTagID}">${Item.tbTagCustomerID}</div></td>
// <td id="tbTagAzureID-${Item.tbTagID}">${Item.tbTagAzureID}</div></td> 

// new code get all items and display them
const getTags = () => {

    returnAllTags(function (data) {

        displayTags(data);
    })
}

// master very simple function to get all rows in a table store  // later send store name as var
function returnAllTags(callbackTags) {

    openIDBGetStore(storeName, 'readwrite', function (store) {
        var request = store.getAll();
        request.onsuccess = function (evt) {
            var tb = evt.target.result
            // console.log(data)
            callbackTags(tb)
        };
    })
};


// univ form materialize javascript
function launchTagsForm(tagid) {

    // set data-tbid on form for later actions
    var myform = document.getElementById('launchTagsForm')
    var myform = myform.setAttribute('data-tbid', tagid)


    var elem = document.querySelector('#launchTagsForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    //  console.log("launchEmptyTagsForm finished")

}

export function tagFormHTMLComponent(targetDiv) {
    let td = $(targetDiv);
    let tagformhtml = `
    <!-- ---- Start Tags Form  move to component later   -->
    <div id="launchTagsForm" class="modal" data-tbid="">
       <div class="modal-content" data-backdrop="static" data-keyboard="false">
          <!-- Left Half -->
          <div class="containerxxx">
             <form class="yyy">
                <!-- start header row -->
                <div class="row">
                   <div class="input-field col s6">
                      <h4>Tags Item </h4>
                   </div>
                   <div class="input-field col s2">
                      <input disabled id="tbid" type="text" class="validate">
                      <label for="tbid">Unique ID</label>
                   </div>
                   <div class="input-field col s2">
                      <input id="tbTagCustomerID" type="text" class="validate">
                      <label for="tbTagCustomerID">Customer ID</label>
                   </div>
                   <div class="input-field col s2">
                      <a title="Close without saving!" id="closeEditorTagsTop"
                         class="modal-close waves-effect waves-blue btn-flat">Close</a>
                   </div>
                </div>
                <!-- end first hdr row -->
                <div class="row">
                   <div class="input-field col s8">
                      <input id="tbTagName" type="text" class="validate" required="">
                      <label for="tbTagName">Tag Name:</label>
                      <span class="helper-text" data-error="You must enter a value!" data-success="ok"></span>
                   </div>
                   <div class="input-field col s4">
                      <input id="tbTagGroup" type="text" class="validate"
                         data-formkey="md-tagform-NA">
                      <label for="tbTagGroup">Tag Group</label>
                   </div>
                </div>
                <div class="row">

                   <div class="input-field col s8">
                      <input id="tbTagDescription" type="text" class="validate">
                      <label for="tbTagDescription">Description</label>
                   </div>

                   <div class="input-field col s4">
                      <input id="tbTagPurpose" type="text" class="validate">
                      <label for="tbTagPurpose">Tag Purpose</label>
                   </div>
                </div>
                <div class="row">

                   <div class="input-field col s4">
                      <input id="tbTagNameShort" type="text" class="validate" required="">
                      <label for="tbTagNameShort">Tag Name Short</label>
                   </div>
                   <div class="input-field col s4">
                      <input id="tbTagPopular" type="text" class="validate">
                      <label for="tbTagPopular">Popularity</label>
                   </div>
                   <div class="input-field col s2">
                      <input id="tbTagSortOrder" type="text" class="validate">
                      <label for="tbTagSortOrder">Order</label>
                   </div>
                   <div class="input-field col s2">
                      <input id="tbTagAzureID" type="text" class="validate">
                      <label for="tbTagAzureID">Ext ID</label>
                   </div>

                </div>
             </form>
          </div>
          <div class="modal-footer">
             <a id="btnSaveFormDataTag" href="#!" title="Save to the database"
                class="waves-effect waves-light btn-small blue darken-4">Save</a>
             <a id="btnCreateTag" href="#!" title="Create new Tag from data in this form!"
                class="waves-effect waves-blue btn-flat">Create New</a>
             <a id="closeEditorTags" title="Close without saving!"
                class="modal-close waves-effect waves-blue btn-flat">Close</a>

          </div>
       </div>
    </div>
    <!--  ----END Tags Form  -->
          `
    td.append(tagformhtml)
};


// univ form
$(document).on('click', '#btnLaunchEmptyTagsForm', function (e) {
    //$('#btnLaunchEmptyTagsForm').on('click', function () {
    // open modal first

    launchEmptyTagsForm()
});

// univ form materialize javascript
function launchEmptyTagsForm() {


    var elem = document.querySelector('#launchTagsForm');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    setTimeout(() => {
        resetTagsInput()
    }, 300);

}

$(document).on('click', '#btnCreateTag', function (e) {
    //$('#btnCreateTag').on('click', function () {

    confirm("Are you sure, this will create a new Tag. It will be a duplicate.")
    createTag()
});


/*   ----------  START Revised CRUD Operations   ----------- */

function createTag() {
    // let tbTagID = $('#tbTagID').val();
    let tbTagName = $('#tbTagName').val();
    let tbTagGroup = $('#tbTagGroup').val();
    let tbTagNameShort = $('#tbTagNameShort').val();
    let tbTagPurpose = $('#tbTagPurpose').val();
    let tbTagDescription = $('#tbTagDescription').val();

    let tbTagOwner = $('#tbTagOwner').val();
    let tbTagPopular = $('#tbTagPopular').val();
    let tbTagSortOrder = $('#tbTagSortOrder').val();
    let tbTagCustomerID = $('#tbTagCustomerID').val();
    let tbTagAzureID = $('#tbTagAzureID').val();



    let nameCheck = $("#tbTagName").val()
    console.log("Name check " + nameCheck)
    if (nameCheck == '' || nameCheck == ' ') {
        alert("You must enter a Tag Name!")
        return
    }

    /* build up an object */
    var obj = {
        //  tbTagID: tbTagID,
        tbTagName: tbTagName,
        tbTagGroup: tbTagGroup,
        tbTagNameShort: tbTagNameShort,
        tbTagPurpose: tbTagPurpose,
        tbTagDescription: tbTagDescription,

        tbTagOwner: tbTagOwner,
        tbTagPopular: tbTagPopular,
        tbTagSortOrder: tbTagSortOrder,
        tbTagCustomerID: tbTagCustomerID,
        tbTagAzureID: tbTagAzureID,
    };

    let newItemId = ""
    /*  add the Tag */
    openIDBGetStore(storeName, 'readwrite', function (store) {

        var request;
        try {
            request = store.add(obj);
        } catch (e) {
            displayFailureMessage("Some Problem While adding");
            throw e;
        }
        request.onsuccess = function (e) {

            // return the id of the newly added item
            newItemId = request.result

            // now fetch the new record, and generate template stuff
            var request2 = store.get(newItemId);
            request2.onsuccess = function (e) {

                var data = e.target.result;

                console.log("data " + JSON.stringify(data))

                let ids = buildIdsTags(data.id);
                displayDT = $("#displayDtTags")
                displayDT.append(generateDatatableTags(data, ids));
                fillTagsForm(data, ids.tagID, ids.editID); //not sure why this was here ??, maybe for insterting new into dom
                deleteTag(data, ids.listItemID, ids.deleteID);
                // update tbTagID field to match indexedb key id
                let fieldName = "tbTagID"
                let newVal = String(data.id)
                let uid = Number(data.id)  // keys are numbers in idb, we find the new record by key and update the tbid
                setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal)

                // console.log("ids " + JSON.stringify(ids))
                displaySuccessMessageDB("createTag Result: " + JSON.stringify(tbTagName) + " added!");

            }
        };
        request.onerror = function () {

            displaySuccessMessageDB("createTag Result: " + this.error);
        };
    });

}


const deleteTag = (tag, listItemID, deleteID) => {
    // note may have to modify this to suit tagid as string for now tbTagID and built in id must match
    //  console.log("tag.tbTagID: " + tag.tbTagID + " check!")
    var indexName = "tbTagID"
    var tagid = tag.tbTagID
    // console.log("tagid " + tagid)

    let deleteBtn = $(`#${deleteID}`);
    deleteBtn.click(() => {

        deleteRowFromIDBUniversal(storeName, indexName, tagid)

        $(`#${listItemID}`).remove();
        displaySuccessMessageDB("deleteTag Result: " + listItemID + " deleted!");

    });
}

//  get one fill form first, put show details button, then edit button
const fillTagsForm = (tag, tagID, editID) => {

    //  console.log("fillTagsForm - tag.tbTagID " + tag.tbTagID)  // returns the id without underscore
    //  console.log("fillTagsForm - tagID " + tagID)  // tag_70
    // console.log("fillTagsForm - editID " + editID)  //edit_70

    let targetDiv = '#divTbPagesL1'
    tagFormHTMLComponent(targetDiv)

    let editBtn = $(`#${editID}`);
    editBtn.click(() => {

        openIDBGetStore(storeName, 'readonly', function (store) {

            // var request = store.get(tag.tbTagID);
            var tagid = String(tag.tbTagID)

            var index = store.index("tbTagID");
            var request = index.get(tagid);

            request.onerror = function (event) {
                alert("there was error getting store id");
            };
            request.onsuccess = function (event) {

                // here we are returning data to the form
                var data = request.result;
                if (!data) {
                    alert("not a valid key, check for string");
                    return;
                }
                /// here for data
                console.log("tbTagName: " + data.tbTagName)

                // populate the form fields

                $("#tbid").val(data.tbTagID);  // universal input on all forms used on focdcommon
                $("#tbTagName").val(data.tbTagName);
                $("#tbTagGroup").val(data.tbTagGroup);
                $("#tbTagNameShort").val(data.tbTagNameShort);
                $("#tbTagPurpose").val(data.tbTagPurpose);
                $('#tbTagPopular').val(data.tbTagPopular);
                $('#tbTagSortOrder').val(data.tbTagSortOrder);
                $('#tbTagDescription').val(data.tbTagDescription);
                $('#tbTagOwner').val(data.tbTagOwner);
                $('#tbTagCustomerID').val(data.tbTagCustomerID);
                $('#tbTagAzureID').val(data.tbTagAzureID);



                //  console.log("data sent to form: " + JSON.stringify(data))
                displaySuccessMessageDB("Form opened, ready for editing!");
                launchTagsForm(tagid)
                // activate the labels on the form
                M.updateTextFields()
                // open or resize textarea fields

                // to make this work, simply add class data-length="250" to text area or input
                $('input#zzz, textarea#zzz').characterCounter();

            };
        });
    });


}

$(document).on('click', '#btnSaveFormDataTag', function (e) {
    //$('#btnSaveFormDataTag').on('click', function () {

    saveFormDataTag()
});

// save the edit - only certain fields are editable, not all
const saveFormDataTag = () => {
    // first get form data
    let tbTagName = $("#tbTagName").val()
    let tbTagGroup = $("#tbTagGroup").val()
    let tbTagNameShort = $("#tbTagNameShort").val()
    let tbTagPurpose = $("#tbTagPurpose").val()
    let tbTagDescription = $("#tbTagDescription").val()
    let tbTagOwner = $("#tbTagOwner").val()
    let tbTagPopular = $("#tbTagPopular").val()
    let tbTagSortOrder = $("#tbTagSortOrder").val()
    let tbTagCustomerID = $("#tbTagCustomerID").val()
    let tbTagAzureID = $("#tbTagAzureID").val()

    openIDBGetStore(storeName, 'readwrite', function (store) {

        var tagid = $("#tbid").val()
        console.log("tagid: " + tagid)
        tagid = String(tagid)

        var index = store.index("tbTagID");
        var request = index.get(tagid);
        request.onerror = function (event) {
            // Handle errors!
            alert("there was error getting store id")
        };
        request.onsuccess = function (event) {
            // Get the exist value that we want to update
            var data = request.result;
            //    console.log("request.result " + JSON.stringify(data))

            try {
                // apply editable data fields on form to the data object
                data.tbTagName = tbTagName;
                data.tbTagGroup = tbTagGroup;
                data.tbTagNameShort = tbTagNameShort;
                data.tbTagPurpose = tbTagPurpose;

                data.tbTagDescription = tbTagDescription;
                data.tbTagOwner = tbTagOwner;
                data.tbTagPopular = tbTagPopular;
                data.tbTagSortOrder = tbTagSortOrder;
                data.tbTagCustomerID = tbTagCustomerID;
                data.tbTagAzureID = tbTagAzureID;


            } catch (e) {
                console.log("saveFormDataTag - fail")

                throw e;
                //  return;
            }
            // Put this updated object back into the database.
            var requestUpdate = store.put(data);
            requestUpdate.onerror = function (event) {
                // Do something with the error

                // throw event;
                alert("there was error updating progress in idb")
            };
            requestUpdate.onsuccess = function (event) {

                console.log("Success - Form is updated!")

                // $("#tbid").text(tbTagID);  // universal input on all forms used on focdcommon
                $(`#tbTagName-${tagid}`).text(tbTagName);
                $(`#tbTagGroup-${tagid}`).text(tbTagGroup);
                $(`#tbTagNameShort-${tagid}`).text(tbTagNameShort);
                $(`#tbTagPurpose-${tagid}`).text(tbTagPurpose);
                $(`#tbTagDescription-${tagid}`).text(tbTagDescription);
                $(`#tbTagOwner-${tagid}`).text(tbTagOwner);
                $(`#tbTagPopular-${tagid}`).text(tbTagPopular);
                $(`#tbTagSortOrder-${tagid}`).text(tbTagSortOrder);
                $(`#tbTagCustomerID-${tagid}`).text(tbTagCustomerID);
                $(`#tbTagAzureID-${tagid}`).text(tbTagAzureID);

            };
        }; // end onsuccess
    });
};



function initDtTags() {
    $('#dataTableTags').DataTable({
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',  // f = search 
        "dom": '<"top"f>rt<"bottom"Bilp>',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        select: true,
        colReorder: true,  // change what columns show first, second etc.
        responsive: true,
        fixedHeader: true,
        destroy: true,

    });

};


$(document).on('click', '#clearForm', function (e) {
    //$('#clearForm').on('click', function () {
    console.log("clicked reset tag")
    resetTagsInput()
});



/* show or hide brief descriptn because it is truncated */
setTimeout(() => {
    $('.showhide').on('dblclick', function () {

        console.log("clicked showhide")

        if ($('.showhide').hasClass('truncate')) {
            $('.showhide').removeClass('truncate');
            $('.showhide').addClass('truncated');
        } else {
            $('.showhide').removeClass('truncated');
            $('.showhide').addClass('truncate');
        }
    });
}, 500);



const resetTagsInput = () => {

    $('#tbTagID').val();
    $("#tbTagName").val('');
    $("#tbTagGroup").val('');
    $("#tbTagNameShort").val('');
    $("#tbTagPurpose").val('');
    $('#tbTagDescription').val('');
    $('#tbTagOwner').val('');
    $('#tbTagPopular').val('');
    $('#tbTagSortOrder').val('');
    $('#tbTagCustomerID').val('');
    $('#tbTagAzureID').val('');

}

const buildIdsTags = (itemid) => {
    return {
        editID: "edit_" + itemid,
        deleteID: "delete_" + itemid,
        listItemID: "listItem_" + itemid,
        tagID: "tag_" + itemid,
        bpselectorID: "bpselector_" + itemid
    }
}




const displayTags = (data) => {


    data.forEach((Item) => {

        //   console.log("displayTags tbTagID: " + Item.tbTagID)
        //   console.log("displayTags Item: " + JSON.stringify(Item))

        let ids = buildIdsTags(Item.tbTagID);

        //   console.log("displayTags ids: " + JSON.stringify(ids))
        // looks like this ids: {"editID":"edit_83","deleteID":"delete_83","listItemID":"listItem_83","tagID":"tag_83","bpselectorID":"bpselector_83"}
        displayDT = $("#displayDtTags")
        displayDT.append(generateDatatableTags(Item, ids));
        fillTagsForm(Item, ids.tagID, ids.editID);
        deleteTag(Item, ids.listItemID, ids.deleteID);

    });
}


