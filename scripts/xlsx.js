
import XLSX from 'xlsx'


//Drag-and-drop uses the HTML5 FileReader API.
export function handleDrop(e) {
    console.log("dropped: ")
    e.stopPropagation(); e.preventDefault();
    var files = e.dataTransfer.files, f = files[0];
    var reader = new FileReader();
    reader.onload = function (e) {
        var data = new Uint8Array(e.target.result);
        var workbook = XLSX.read(data, { type: 'array' });

        /* DO SOMETHING WITH workbook HERE */

        console.log("dropped: " + JSON.stringify(workbook))
    };
    reader.readAsArrayBuffer(f);
}