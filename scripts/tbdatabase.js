// import the default json app data and limited demo data
import {
  getAdminPanelJsonData,
  getResourcesJsonData,
  getDocumentJsonData,
  getTagsJsonData,
} from './tbdata';

const fileSaver = require('file-saver');
const DB_NAME = 'TimebarsBlogIDB';
const DB_VERSION = 12;
const STORE_ADMINPANEL = 'tbAdminPanel';
const STORE_RESOURCES = 'tbResources';
const STORE_TAGS = 'tbTags';
const STORE_DOCS = 'tbDocuments';
const API_URL = 'https://api.rlan.ca/api/v1';

// App startup - init database
export function openDB() {
  // console.log("got here idb")

  let indexedDB =
    window.indexedDB ||
    window.webkitIndexedDB ||
    window.mozIndexedDB ||
    window.msIndexedDB;

  var openRequest = indexedDB.open(DB_NAME, DB_VERSION);

  // triggered if the client had no database
  openRequest.onupgradeneeded = function(evt) {
    displaySuccessMessageDB(
      'onupgradeneeded Creted the Cache First Database. <br> ',
    );

    let db = openRequest.result;

    db.onerror = function(event) {
      console.log('Error loading database.');
      displayFailureMessageDB('upgrading problem: ' + this.error);
    };

    console.log('fired onUpgradeNeeded, got to upgrade new and re-open db');

    // Create Docs Table
    let store = db.createObjectStore(STORE_DOCS, {
      keyPath: 'id',
      autoIncrement: true,
    });
    store.createIndex('tbDocID', 'tbDocID', { unique: true });
    store.createIndex('tbDocName', 'tbDocName', { unique: false });
    store.createIndex('tbDocCustomerID', 'tbDocCustomerID', { unique: false });
    store.createIndex('tbDocBriefDescription', 'tbDocBriefDescription', {
      unique: false,
    });
    store.createIndex('tbDocType', 'tbDocType', { unique: false });
    store.createIndex('tbDocNameShort', 'tbDocNameShort', { unique: false });
    store.createIndex('tbDocHTMLContent', 'tbDocHTMLContent', {
      unique: false,
    });
    store.createIndex('tbDocGroup', 'tbDocGroup', { unique: false });
    store.createIndex('tbDocPurpose', 'tbDocPurpose', { unique: false });
    store.createIndex('tbDocOwner', 'tbDocOwner', { unique: false });
    store.createIndex('tbDocPopular', 'tbDocPopular', { unique: false });
    store.createIndex('tbDocSortOrder', 'tbDocSortOrder', { unique: false });
    store.createIndex('tbDocLastModified', 'tbDocLastModified', {
      unique: false,
    });
    store.createIndex('tbDocApprovedBy', 'tbDocApprovedBy', { unique: false });
    store.createIndex('tbDocStatus', 'tbDocStatus', { unique: false });
    store.createIndex('tbDocWrittenBy', 'tbDocWrittenBy', { unique: false });
    store.createIndex('tbDocL1', 'tbDocL1', { unique: false });
    store.createIndex('tbDocL2', 'tbDocL2', { unique: false });
    store.createIndex('tbDocL3', 'tbDocL3', { unique: false });
    store.createIndex('tbDocL4', 'tbDocL4', { unique: false });
    store.createIndex('tbDocL5', 'tbDocL5', { unique: false });
    store.createIndex('tbDocCardImage', 'tbDocCardImage', { unique: false });

    //Create admin panel table (maybe see if key path can be changed to apID??
    store = db.createObjectStore(STORE_ADMINPANEL, {
      keyPath: 'id',
      autoIncrement: true,
    });
    store.createIndex('apID', 'apID', { unique: true });
    store.createIndex('apCustomerID', 'apCustomerID', { unique: false });
    store.createIndex('apAzureID', 'apAzureID', { unique: false });
    store.createIndex('ap_name', 'ap_name', { unique: false });
    store.createIndex('apTS_WeeklyOrMonthly', 'apTS_WeeklyOrMonthly', {
      unique: false,
    });
    store.createIndex('apTS_Start', 'apTS_Start', { unique: false });
    store.createIndex('apTS_Finish', 'apTS_Finish', { unique: false });
    store.createIndex('apTS_MonthlyPxFactor', 'apTS_MonthlyPxFactor', {
      unique: false,
    });
    store.createIndex('apTS_WeeklyPxFactor', 'apTS_WeeklyPxFactor', {
      unique: false,
    });
    store.createIndex('apHelpCoordTop', 'apHelpCoordTop', { unique: false });
    store.createIndex('apHelpCoordLeft', 'apHelpCoordLeft', { unique: false });
    store.createIndex('apStatusDate', 'apStatusDate', { unique: false }); // added to forms July 30
    store.createIndex('ap_HolidayDate', 'ap_HolidayDate', { unique: false }); // not added to forms
    store.createIndex('ap_HolidayName', 'ap_HolidayName', { unique: false }); // not added to forms
    store.createIndex('totalNumberOfPeople', 'totalNumberOfPeople', {
      unique: false,
    }); // not added to forms
    store.createIndex('numberOfPeoplePerLine', 'numberOfPeoplePerLine', {
      unique: false,
    }); // not added to forms
    store.createIndex('pxPerPerson', 'pxPerPerson', { unique: false }); // not added to forms
    store.createIndex('apFilteredFrom', 'apFilteredFrom', { unique: false }); // not added to forms
    store.createIndex('apFilteredFromTbID', 'apFilteredFromTbID', {
      unique: false,
    }); // not added to forms
    store.createIndex('apSchMethod', 'apSchMethod', { unique: false }); // not added to forms
    store.createIndex('apTbVisibleLevel', 'apTbVisibleLevel', {
      unique: false,
    }); // not added to forms
    store.createIndex('hideCompletedBarsYN', 'hideCompletedBarsYN', {
      unique: false,
    }); // not added to forms

    // Create Resource Table tbResEmail, tbResPasswordHash
    store = db.createObjectStore(STORE_RESOURCES, {
      keyPath: 'id',
      autoIncrement: true,
    });
    store.createIndex('tbResID', 'tbResID', { unique: true });
    store.createIndex('tbResCustomerID', 'tbResCustomerID', { unique: false });
    store.createIndex('tbResAzureID', 'tbResAzureID', { unique: false });
    store.createIndex('tbResName', 'tbResName', { unique: true });
    store.createIndex('tbResNameShort', 'tbResNameShort', { unique: false });
    store.createIndex('tbResEmail', 'tbResEmail', { unique: false });
    store.createIndex('tbResStatus', 'tbResStatus', { unique: false });
    store.createIndex('tbResDepartment', 'tbResDepartment', { unique: false });
    store.createIndex('tbResManager', 'tbResManager', { unique: false });
    store.createIndex('tbResPayRate', 'tbResPayRate', { unique: false });
    store.createIndex('tbResCoordTop', 'tbResCoordTop', { unique: false });
    store.createIndex('tbResCoordLeft', 'tbResCoordLeft', { unique: false });
    store.createIndex('tbResTeam', 'tbResTeam', { unique: false });
    store.createIndex('tbResLocation', 'tbResLocation', { unique: false });
    store.createIndex('tbResCostCode', 'tbResCostCode', { unique: false });
    store.createIndex('tbResTeamLeader', 'tbResTeamLeader', { unique: false });
    store.createIndex('tbResSupervisor', 'tbResSupervisor', { unique: false });
    store.createIndex('tbResPasswordHash', 'tbResPasswordHash', {
      unique: false,
    });
    store.createIndex('tbResResourceType', 'tbResResourceType', {
      unique: false,
    });
    store.createIndex('tbResResourceClass', 'tbResResourceClass', {
      unique: false,
    });
    store.createIndex('tbResResourceCalendar', 'tbResResourceCalendar', {
      unique: false,
    });
    store.createIndex('tbResPIN', 'tbResPIN', { unique: false });
    store.createIndex('tbResExtSystemResID', 'tbResExtSystemResID', {
      unique: false,
    });
    store.createIndex('tbResSortOrder', 'tbResSortOrder', { unique: false });
    store.createIndex('tbResLastModified', 'tbResLastModified', {
      unique: false,
    });
    store.createIndex('tbResLabourType', 'tbResLabourType', { unique: false });

    // 23 fields now, done in forms and tested

    // Create Tags Table
    store = db.createObjectStore(STORE_TAGS, {
      keyPath: 'id',
      autoIncrement: true,
    });
    store.createIndex('tbTagID', 'tbTagID', { unique: true });
    store.createIndex('tbTagCustomerID', 'tbTagCustomerID', { unique: false });
    store.createIndex('tbTagAzureID', 'tbTagAzureID', { unique: false });
    store.createIndex('tbTagGroup', 'tbTagGroup', { unique: false });
    store.createIndex('tbTagName', 'tbTagName', { unique: false });
    store.createIndex('tbTagNameShort', 'tbTagNameShort', { unique: false });
    store.createIndex('tbTagPurpose', 'tbTagPurpose', { unique: false });
    store.createIndex('tbTagDescription', 'tbTagDescription', {
      unique: false,
    });
    store.createIndex('tbTagOwner', 'tbTagOwner', { unique: false });
    store.createIndex('tbTagPopular', 'tbTagPopular', { unique: false });
    store.createIndex('tbTagSortOrder', 'tbTagSortOrder', { unique: false });
    store.createIndex('tbTagLastModified', 'tbTagLastModified', {
      unique: false,
    });
    // these 3 new tags are done and testind in forms

    // 83 fields  + 10 below 83 total

    setTimeout(() => {
      populateMandatoryData();
    }, 500);
  }; // end upgrade needed

  openRequest.onsuccess = function(evt) {
    let db = openRequest.result;
    db.onversionchange = function() {
      db.close();
      alert('Database is being rebuild, please reload page twice.');
    };
    // console.log("DONE initializing db");
    // displaySuccessMessageDB("Initialized the Cache First Database. <br>");
  };
  openRequest.onerror = function(evt) {
    console.log(
      'Failure openDb:' + evt.target.errorCode + 'err- ' + this.error,
    );
    displayFailureMessageDB(this.error);
  };

  openRequest.onblocked = function() {
    // there's another open connection to same database
    // and it wasn't closed after db.onversionchange triggered for them
    alert('Database is blocked');
  };
}

/* Start drag drop to upload */
export function handleJSONFileDropforUpload(targetDiv) {
  console.log(' div for upload: ' + targetDiv);

  let dmDropArea = document.getElementById(targetDiv);
  // Prevent default drag behaviors

  ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(item => {
    dmDropArea.addEventListener(item, preventDefaults, false);
    document.body.addEventListener(item, preventDefaults, false);
  });
  // Highlight drop area when item is dragged over it

  ['dragenter', 'dragover'].forEach(item => {
    dmDropArea.addEventListener(item, highlight(dmDropArea), false);
  });

  ['dragleave', 'drop'].forEach(item => {
    dmDropArea.addEventListener(item, unhighlight(dmDropArea), false);
  });

  // Handle dropped files
  dmDropArea.addEventListener('drop', handleDmDrop, false);
}

function preventDefaults(e) {
  e.preventDefault();
  e.stopPropagation();
}

function highlight(dmDropArea, e) {
  dmDropArea.classList.add('highlightDrop');
}

function unhighlight(dmDropArea, e) {
  dmDropArea.classList.remove('highlightDrop');
}

function handleDmDrop(e) {
  let dmFiles = e.dataTransfer.files;

  var selectedFile = '';
  var storeName = '';

  //   $("#dataControllerUploadBox").text(dmFiles.length + " file(s) dropped, data uploaded and backup taken!");
  //  $("#tbCanvas").text(dmFiles.length + " file(s) dropped, data uploaded and backup taken!");
  //  $("#msg").text(dmFiles.length + " file(s) dropped, data uploaded and backup taken!");

  // for each file, get storname and populate idb
  for (var j = 0; j < dmFiles.length; j++) {
    var fname = dmFiles[j].name.substr(0, 6);
    console.log('fname : ' + fname);

    if (fname == 'later') {
      storeName = 'later';
    } else if (fname == 'tbTime') {
      storeName = 'tbTimebars';
    } else if (fname == 'tbTags') {
      storeName = 'tbTags';
    } else if (fname == 'tbReso') {
      storeName = 'tbResources';
    } else if (fname == 'tbMeta') {
      storeName = 'tbMetaData';
    } else if (fname == 'tbAdmi') {
      storeName = 'tbAdminPanel';
    } else if (fname == 'tbResC') {
      displaySuccessMessageDB('Processing Resource JSON Files');
      storeName = 'tbResCalcs';
    } else if (fname == 'tbDocu') {
      displaySuccessMessageDB('Processing Document JSON Files');
      storeName = 'tbDocuments';
    } else if (fname == 'tbAddT') {
      storeName = 'tbAddTimebars';
    } else {
      alert('did not work, check file name and content');
      storeName = 'NA';
    }

    selectedFile = dmFiles[j];
    console.log('filename : ' + fname);

    OnDragProcessJSONFiles(selectedFile, storeName);
  }
  //
}

export function OnDragProcessJSONFiles(selectedFile, storeName) {
  console.log('storeName : ' + storeName);
  var reader = new FileReader();
  reader.onload = function(e) {
    // var result = JSON.parse(this.result);

    //  console.log("jsonData" + this.result)

    // backup the data before deleting it
    createJsonFileSimple(storeName);
    // delete all data in the store so we can fill with new data from the drop evnet
    clearObjectStore(storeName);

    let jsonData = this.result;
    universalJsonDataPopulator(jsonData, storeName);
  }; // end read onload
  reader.readAsText(selectedFile);
}

/* END drag drop to upload */

// always use whenever opening store
export function openIDBGetStore(storeName, mode, callback) {
  //  window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
  //  window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
  //  window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

  // In the following line, you should include the prefixes of implementations you want to test.

  // ********    note, i had to comment out this next line, not sure why?????? could be problem later

  // window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
  // DON'T use "var indexedDB = ..." if you're not in a function.
  // Moreover, you may need references to some window.IDB* objects:
  window.IDBTransaction = window.IDBTransaction ||
    window.webkitIDBTransaction ||
    window.msIDBTransaction || { READ_WRITE: 'readwrite' }; // This line should only be needed if it is needed to support the object's constants for older browsers
  window.IDBKeyRange =
    window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
  // (Mozilla has never prefixed these objects, so we don't need window.mozIDB*)

  if (!window.indexedDB) {
    alert("Sorry!Your browser doesn't support IndexedDB");
  }
  var db;
  var request = window.indexedDB.open(DB_NAME, DB_VERSION);
  //Handle error On db open
  request.onerror = function(event) {
    console.log(event.target.errorCode);
  };
  request.onsuccess = function(event) {
    // Handle success On db open, get the db object, then open store
    db = this.result;

    var tx = db.transaction(storeName, mode);
    let store = tx.objectStore(storeName);
    callback(store);
  };
}

// std success fail message for db related tasks
export function displaySuccessMessageDB(msg) {
  $('#msgDB').append('<span class="actionSuccess">Success: ' + msg + '</span>');

  M.toast({
    html: msg,
    classes: 'orange darken-3 rounded',
  });
}

export function displayFailureMessageDB(msg) {
  $('#msgDB').append('<span class="actionFailure">Error: ' + msg + '</span>');
  M.toast({
    html: `<div style="color:pink;">Error: ${msg}</div>`,
  });
}

//   end common functions

//********* start bulk data operations     ******************

export function populateMandatoryData() {
  console.log('called populateMandatoryData');

  let apdata = getAdminPanelJsonData();
  apdata = JSON.stringify(apdata);
  universalJsonDataPopulator(apdata, STORE_ADMINPANEL);

  let tagdata = getTagsJsonData();
  tagdata = JSON.stringify(tagdata);
  universalJsonDataPopulator(tagdata, STORE_TAGS);

  let resdata = getResourcesJsonData();
  resdata = JSON.stringify(resdata);
  universalJsonDataPopulator(resdata, STORE_RESOURCES);

  let docdata = getDocumentJsonData();
  docdata = JSON.stringify(docdata);
  universalJsonDataPopulator(docdata, STORE_DOCS);
}

export function universalJsonDataPopulator(jsonData, storeName) {
  // event.preventDefault
  // console.log("hit the fn populateMandatoryData")
  // let configData = JSON.stringify(jsonData)
  let configData = JSON.parse(jsonData);

  openIDBGetStore(storeName, 'readwrite', function(store) {
    var request;
    var i;
    try {
      // add rows
      for (i = 0; i < configData[storeName].length; i++) {
        request = store.put(configData[storeName][i]);
      }
    } catch (e) {
      displayFailureMessageDB(
        'Some Problem While adding data, if data already exists, this will happen, clear data out then try again',
      );
      throw e;
    }
    request.onsuccess = function(evnt) {
      displaySuccessMessageDB(`App data added: ${storeName}.<br> `);
      console.log(`App data added: ${storeName} `);
    };
    request.onerror = function() {
      displayFailureMessageDB(
        'data must exist already, will not overwite it. to check hit F12 key > Application Tab > IndexedDB ' +
          this.error,
      );
      console.log('Error Default data must exist already! ' + this.error);
    };
  });
}

export function deleteDB() {
  console.log('called delete');
  var request = indexedDB.deleteDatabase(DB_NAME);
  request.onsuccess = function(evt) {
    console.log('delete  db done');
    // location.reload();
    displaySuccessMessageDB(`Deleted IndexedDB: ${DB_NAME}. <br> `);
  };
  request.onerror = function(evt) {
    alert('error deleting db');
  };
}

export function clearStoreTable(storeName) {
  /* fetch section begins */
  fetch(`${API_URL}/${storeName.toLowerCase()}`, {
    method: 'DELETE',
    headers: {
      Authorization: 'Here goes the token gained from login',
    },
  })
    .then(res => {
      if (res.ok) displaySuccessMessageDB(`Cleared all: ${storeName}. <br> `);
    })
    .catch(err =>
      displayFailureMessageDB(
        'Delete from: ' + storeName + ' failed ' + err.message,
      ),
    );
  /* fetch section ends */

  //   openIDBGetStore(storeName, 'readwrite', function(store) {
  //     var req = store.clear();
  //     req.onsuccess = function(evnt) {
  //       console.log('done clearing: ' + storeName);
  //       displaySuccessMessageDB(`Cleared all: ${storeName}. <br> `);
  //     };
  //     req.onerror = function(evnt) {
  //       console.log('Did not clear: ' + storeName);
  //       displayFailureMessageDB(
  //         'Delete from: ' + storeName + ' failed ' + this.error,
  //       );
  //     };
  //   });
}

export function deleteRowFromIDBUniversal(storeName, indexName, tbID) {
  /* fetch section begins */
  // DELETE http://{{host}}/api/v1/tbresources/tbresid/700
  storeName = storeName.toLowerCase();
  indexName = indexName.toLowerCase();

  fetch(`${API_URL}/${storeName}/${indexName}/${tbID}`, { method: 'DELETE' })
    .then(res => res.json())
    .then(res => {
      if (res.ok) console.log('successfully deleted timebar: ' + tbID);
      else console.log("could't delete timebar: " + tbID);
    })
    .catch(console.error);
  /* fetch section ends */
  //   var tbID = String(tbID);

  //   console.log('tbID: ' + JSON.stringify(tbID));
  //   openIDBGetStore(storeName, 'readwrite', function(store) {
  //     var index = store.index(indexName);
  //     var request = index.get(tbID);

  //     request.onsuccess = function(evnt) {
  //       var data = request.result;

  //       //   console.log("data: " + JSON.stringify(data))

  //       store.delete(data.id);
  //       console.log('successfully deleted timebar: ' + tbID);
  //     };
  //     request.onerror = function(evnt) {
  //       console.log('error deleting timebar');
  //     };
  //   });
}

export function clearObjectStore(tbStoreName) {
  /* fetch section begins */
  fetch(`${API_URL}/${tbStoreName.toLowerCase()}`, {
    method: 'DELETE',
    headers: {
      Authorization: 'Here goes the token gained from login',
    },
  })
    .then(res => {
      if (res.ok) console.log('done: ' + tbStoreName);
    })
    .catch(console.error);
  /* fetch section ends */

  //   var tbStoreName = tbStoreName;
  //   openIDBGetStore(tbStoreName, 'readwrite', function(store) {
  //     var req = store.clear();
  //     req.onsuccess = function(evnt) {
  //       console.log('done: ' + tbStoreName);
  //       //displayResCalcs();
  //     };
  //     req.onerror = function(evnt) {
  //       console.log('Did not clear: ' + tbStoreName);
  //     };
  //   });
}

export function returnOneItemUniversal(
  rowID,
  storeName,
  indexField,
  callbackItem,
) {
  /* fetch section begins */
  storeName = storeName.toLowerCase();
  indexField = indexField.toLowerCase();

  fetch(`${API_URL}/${storeName}/${indexField}/${rowID}`)
    .then(res => res.json())
    .then(({ data }) => callbackItem(data || null))
    .catch(console.error);
  /* fetch section ends */

  // parentID = "18"
  //   openIDBGetStore(storeName, 'readonly', function(store) {
  //     var index = store.index(indexField);
  //     var request = index.get(rowID);
  //     request.onsuccess = function(event) {
  //       var data = request.result;
  //       // var tbID = data.tbID
  //       // var skey = data.tbSelfKey2
  //       callbackItem(data);
  //       //  console.log("parentID/curid/curselfkey " + parentID + " / " + tbID + " / " + skey)
  //     };
  //   }); // end openIDBGetStore
}

export function exportAllData() {
  createJsonFileSimple(STORE_ADMINPANEL);
  createJsonFileSimple(STORE_DOCS);
  createJsonFileSimple(STORE_RESOURCES);
  createJsonFileSimple(STORE_TAGS);
}

export function createJsonFileSimple(storeOrTable) {
  returnAllStoreRowsForBackup(storeOrTable, function(fnCallbackJSON) {
    var fileName = storeOrTable + '.js';
    // assemble the json objects into an array like so [{},{}]
    var myJsonFile = fnCallbackJSON; //.trimRight("\W") // myJsonRow

    myJsonFile = '{"' + storeOrTable + '":[' + myJsonFile + ']}';

    //  save to download folder
    var blob = new Blob([myJsonFile], { type: 'text/plain' });
    saveAs(blob, fileName);
  });
}

function returnAllStoreRowsForBackup(storeOrTable, fnCallbackJsonAll) {
  var storeOrTable = storeOrTable; //"tbTimebars"
  // alert(storeOrTable)

  //   var jsonString = ''; // this will by my array of json objects from idb
  //   var jsonStrings = '';

  /* fetch section begins */
  fetch(`${API_URL}/${storeOrTable.toLowerCase()}`)
    .then(res => res.json())
    .then(({ data }) => {
      if (!data) return console.warn('Some error while fetching data');

      let jsonString = '';
      data.forEach((ele, index) => {
        delete ele._id;
        delete ele.__v;
        if (index === data.length - 1) jsonString += JSON.stringify(ele);
        else jsonString += JSON.stringify(ele) + ',';
      });

      fnCallbackJsonAll(jsonString);
    })
    .catch(console.error);
  /* fetch section ends */

  //   openIDBGetStore(storeOrTable, 'readonly', function(store) {
  //     store.openCursor().onsuccess = function(event) {
  //       var cursor = event.target.result;
  //       if (cursor) {
  //         jsonString = JSON.stringify(cursor.value);
  //         jsonStrings = jsonStrings += jsonString;
  //         cursor.continue();
  //         // alert(cursor.value.tbID)
  //         jsonStrings = jsonStrings += ',';
  //       } else {
  //         // remove the trailing comma
  //         jsonStrings = jsonStrings.replace(/,$/, '');
  //         // alert("done looping through object store" + jsonStrings)
  //         fnCallbackJsonAll(jsonStrings);
  //       }
  //     }; // end open curso
  //   }); // end get store
}

export function updateOneFieldUniversal(
  storeName,
  indexName,
  tbid,
  fieldName,
  newVal,
) {
  console.log('updateOneFieldUniversal - tbid ' + tbid);

  openIDBGetStore(storeName, 'readwrite', function(store) {
    // var request = store.get(key);
    var index = store.index(indexName);
    var request = index.get(tbid);
    // var request = store.get(tbid);
    request.onerror = function(event) {
      alert('No matching record found, or other error');
    };
    request.onsuccess = function(event) {
      // // note.innerHTML += '<li> opened store tbTimebars from CRUD.js</li>';
      var data = request.result;

      data[fieldName] = newVal;

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function(event) {
        alert('there was error updating data');
      };
      requestUpdate.onsuccess = function(event) {
        console.log(
          'succes updating fieldNamed: ' +
            fieldName +
            ' on tbDocID: ' +
            data.tbDocID,
        );
      };
    };
  });
}

export function setTbidBasedOnKeyUniversal(storeName, uid, fieldName, newVal) {
  console.log('setTbidBasedOnKeyUniversal - uid ' + uid);

  openIDBGetStore(storeName, 'readwrite', function(store) {
    var request = store.get(Number(uid));
    request.onerror = function(event) {
      alert('No matching record found, or other error');
    };
    request.onsuccess = function(event) {
      // // note.innerHTML += '<li> opened store tbTimebars from CRUD.js</li>';
      var data = request.result;

      data[fieldName] = newVal;

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function(event) {
        alert('there was error updating data');
      };
      requestUpdate.onsuccess = function(event) {
        console.log(
          'succes updating fieldNamed: ' +
            fieldName +
            ' on tbDocID: ' +
            data.tbDocID,
        );
      };
    };
  });
}

export function updateOneDocumentField(tbDocID, fieldName1, newValue1) {
  //alert("key " + key)
  console.log('tbDocID ' + tbDocID);
  var tbDocID = String(tbDocID);
  var newValue1 = String(newValue1);

  openIDBGetStore('tbDocuments', 'readwrite', function(store) {
    // var request = store.get(key);
    var index = store.index('tbDocID');
    var request = index.get(tbDocID);

    request.onerror = function(event) {
      alert('No matching record found, or other error');
    };
    request.onsuccess = function(event) {
      // // note.innerHTML += '<li> opened store tbTimebars from CRUD.js</li>';
      var data = request.result;

      data[fieldName1] = newValue1;

      //   console.log("tbDocID  " + data.tbDocID)

      var requestUpdate = store.put(data);

      requestUpdate.onerror = function(event) {
        alert('there was error updating data');
      };
      requestUpdate.onsuccess = function(event) {
        //  // note.innerHTML += '<li>successfully Updated </li>';
        console.log(
          'succes updating fieldNamed: ' +
            fieldName1 +
            ' on tbDocID: ' +
            data.tbDocID,
        );
      };
    };
  });
}

// Copyright © 2018 Timebars Ltd.  All rights reserved.
