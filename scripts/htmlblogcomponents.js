import $ from "jquery";

import { handleDrop } from './xlsx';

//const backendurl = 'http://localhost:3001'
const frontendurl = 'http://localhost:1234'

const backendurl = 'http://localhost:3003'

// template literals awesomeness!!!
// pattern template
export function xxx(targetDiv) {
    let td = $(targetDiv);
    let tbBreadCrumbHTML = `
        `
    td.append(tbBreadCrumbHTML)
};

$(document).on('click', '.zzz', function (e) {
    //here
});




export function DropifyXlsxModalHTMLComponent(targetDiv) {

    let td = $(targetDiv);
    let tbdropifyHTML = `
  <!-- Modal dropify management area -->

  <div id="DropifyXlsxModal" class="modal">
     <div class="modal-content">
        <!-- START Dropify -->

        <div class="row section" id="dropArea">
           <div class="col s12 m4 l3">
           <p>Import Excel data into the Blog Database.</p>   
           <p>Excel file must conform to our Spec. </p>
             
           </div>
           <div class="col s12 m8 l9">
                <form action="${backendurl}/xlsx/upload" method="POST"
                 enctype="multipart/form-data">

                 <input id="fileevents" type="file" class="dropify" name="recfile" />
              </form>
           </div>
        </div>
        <!-- END Dropify -->

        <div class="container">
        <h3>Notes:</h3>
<p>After the system is finished processing your Excel file, refresh the browser and that is it.
the old data is deleted and replaced by all data in your excel sheets.
</p>

<p>Please back up your data to CSV files first just in case you need to restore that data back.</p>
  
<p><a title="Backup all data to 8 individual CSV files, you will be prompted 8 times to save locally."
id="btnExportAlltoCSV" class="zzz" href="#!"><i
    class="material-icons">file_download</i>Perform Data Backup to Excel
(CSV)</a></p>
       
    </div>   

     </div>
  </div>
        `
    td.append(tbdropifyHTML)

    // document.getElementById("DropifyXlsxModal").addEventListener('drop', handleDrop, false);


};






export function tbBlogHomePageHTMLComponent(targetDiv) {

    let td = $(targetDiv);
    let tbHomePageHTML = `
   <div class="tbHomePage">
   <div class="parallax-container">
       <div class="parallax"><img src="images/TimebarsMainNewColors.png" alt="Unsplashed background img 1">
       </div>
   </div>
   <div class="container">
       <div class="section">
           <!--   Icon Section   -->
           <div class="row">
               <div class="col s12 m4">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">flash_on</i></h2>
                       <h5 class="center">What is it?</h5>
                       <p class="light">Advanced browser based planning & scheduling tools with a proper
                           scheduling engine,
                           designed to automate and simplify planning and scheduling scenarios.</p>
                       <p>Our products fill the gaps left behind by ERP & PPM enterprise systems and related
                           desktop applications such as MS Project.
                       </p>
                       <h5>See 10 minute introductory presentation. </h5><a href="https://youtu.be/BcZebcMJHjM"
                           target="_blank">Watch on Youtube!</a>
                   </div>
               </div>
               <div class="col s12 m4">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">group</i></h2>
                       <h5 class="center">What problems do we solve? </h5>
   
                       <p class="light">Eliminates end user frustration caused by hidden rules baked into
                           legacy
                           planning and
                           scheduling software.</p>
                       <p class="light">Saves you time: touch, drag, drop, to plan and schedule your
                           initiatives.
                           Its a new
                           approach,
                           without forms to fill in, with a scheduling engine calculating correct costs and
                           hours.
                       </p>
                       <p class="light">Use your favourite spreadsheet program for data entry, but schedule
                           your
                           work in Timebars.
                       </p>
   
                       <p class="light">
                           You drag bars on the timescale and when you drop them,the scheduling
                           engine calculates actual & forecast dates, actual & forecast work, actual & forecst
                           costs, and then
                           calculates resource supply and demand.</p>
   
                       <p class="light">
                           Timebars will send your scheduling results back to the spreadsheet where you gain
                           back
                           control of your
                           data.
                       </p>
   
                   </div>
               </div>
   
               <div class="col s12 m4">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">settings</i></h2>
                       <h5 class="center">No software to install!</h5>
   
                       <p class="light">
                           <strong>Do not depend on the IT Department!</strong> <br>
                           Become instantly productive with our tools without training and without IT support.
   
                           <p class="light">Timebars is a web page, its as simple to use as use as paying a
                               bill on your banking web site.</p>
                       </p>
                   </div>
               </div>
           </div>
   
       </div>
   </div>
   <div class="parallax-container valign-wrapper">
       <div class="section no-pad-bot">
           <div class="container">
               <div class="row center">
                   <h5 class="header col s12 light">A modern Scheduling Tool</h5>
               </div>
           </div>
       </div>
       <div class="parallax"><img src="images/background21.jpg" alt="Unsplashed background img 2">
       </div>
   </div>
   <div class="container">
       <div class="section">
   
           <!-- jim start 1 -->
           <!--   Icon Section   -->
           <div class="row">
               <div class="col s6">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">fiber_new</i></h2>
                       <h5 class="center">Try Timebars!</h5>
   
                       <p class="center light">
                           We make planning and scheduling fun again with dramatic productivy improvements!
                       </p>
   
                       <p class="center dark">Try the public version free! <a
                               href="https://www.timebars.com/GettingStarted.html"> www.timebars.com</a></p>
                   </div>
               </div>
   
               <div class="col s6">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">flag</i></h2>
                       <h5 class="center">Digital transformation is taking too long!</h5>
                       <p class="center light"> Speed up with Timebars Ltd, project controls specialists and
                           with
                           <a href="http://creativeis.ca/">Creative IS</a>, visionary software developers.</p>
                       <!-- <h4 class="center">Contact Us now!</h4> -->
   
                       <div class="launchContactUs waves-effect waves-light btn-large">
                           Contact Us now!
                           <i class="material-icons large">arrow_forward</i></div>
                   </div>
               </div>
           </div>
           <!-- jim 1 end -->
       </div>
   </div>
   <!-- jim 2 start -->
   <div class="parallax-container valign-wrapper">
       <div class="section no-pad-bot">
           <div class="container">
               <div class="row center">
                   <!-- <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5> -->
               </div>
           </div>
       </div>
       <!-- <div class="parallax"><img src="images/background31.jpg" alt="Unsplashed background img 3"></div> -->
       <div class="parallax"><img src="images/TimebarsMainNewColorsMedium.png" alt="Unsplashed background img 3">
       </div>
   </div>
   <div class="container">
       <div class="section">
           <!--   Icon Section   -->
           <div class="row">
               <div class="col s12 m4">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">timeline</i></h2>
                       <h5 class="center">Resource Planner & Scheduler </h5>
   
                       <p class="light">This tool is for you if: You currently use a Spreadsheet to track your
                           people. You wish it could figure out who is over allocated, who can take on more
                           work, and advise if need to
                           hire. </p>
                       <p>You are a manager of people, the organization has demand for their talent and you
                           are unsure of their availability for committments to the demand.
                           You wish or have the need to visualize this demand on a dynamic timescale and
                           perform what if scenarios.
                       </p>
                   </div>
               </div>
   
               <div class="col s12 m4">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">timelapse</i></h2>
                       <h5 class="center">Sprint Planner & Scheduler</h5>
   
                       <p class="light">This tool is for you if: Your organization invested heavily in ERP,
                           PPM and ticketing systems to manage work.
                           You know Agile development is the way forward for you to manage work and you are
                           stuck,your Teams productivity is down. You feel there must be a simple tool to pull
                           in a backlog, plan a sprint, track
                           progress and produce burn-down charts from data in these systems.</p>
   
                   </div>
               </div>
   
               <div class="col s12 m4">
                   <div class="icon-block">
                       <h2 class="center black-text"><i class="material-icons">access_time</i></h2>
                       <h5 class="center">Agile Kanban Board</h5>
                       <p class="light">
                           You are managing several smaller initiatives and have adhoc stand up meetings with
                           the
                           team using a white
                           board but would like to begin using a software approach. You would like to swap out
                           your methodology mid
                           stream. You wish you could
                           change between Kanban, Sprints or Waterfall seamlessly in one tool.
                           <p>
   
                   </div>
               </div>
           </div>
       </div>
   </div>
   <!-- jim 3 end -->
   <div class="parallax-container valign-wrapper">
       <div class="section no-pad-bot">
           <div class="container">
               <div class="row center">
                   <!-- <h5 class="header col s12 light">A modern responsive front-end framework based on Material Design</h5> -->
               </div>
           </div>
       </div>
       <div class="parallax"><img src="images/background31.jpg" alt="Unsplashed background img 3">
       </div>
   
   </div>
   </div>
        `
    td.append(tbHomePageHTML)
};





export function tbBlogCardHTMLComponentNew(targetDiv, tbDocCardImage, tbDocID, tbDocName, tbDocBriefDescription, tbDocOwner, tbDocStatus, tbDocPurpose) {

    // console.log("targetDiv: " + targetDiv)
    let td = $(targetDiv);
    let blogcardHTML = `
      <div class="col s12 m6 l4 card-width tbcards">
          <div class="card-panel border-radius-6 mt-10 card-animation-1">
              <img class="responsive-img border-radius-8 z-depth-4 image-n-margin"
                  src="${tbDocCardImage}" alt="${tbDocBriefDescription}" /><p></p>
              <a data-tbid="${tbDocID}" href="#!" class="launchTbBlogCardFormNew mt-5">${tbDocName}</a>
              <p>${tbDocBriefDescription}</p>
              <div class="row mt-4">
                  <div class="col s2">
                      <a href="#"><img src="app-assets/images/user/9.jpg" alt="" width="40" alt="fashion"
                              class="circle responsive-img mr-3" /></a>
                  </div>
                  <a href="#">
                      <div class="col s3 p-0 mt-1"><span class="pt-2">${tbDocOwner}</span></div>
                  </a>
                  <div class="col s7 mt-1 right-align">
                      <a href="#"><span class="material-icons">favorite_border</span></a>
                      <span class="ml-3 vertical-align-top">340</span>
                      <a href="#"><span class="material-icons ml-10">chat_bubble_outline</span></a>
                      <span class="ml-3 vertical-align-top">80</span>
                  </div>
              </div>
          </div>
      </div>
        `
    td.append(blogcardHTML)

};




export function tbBlogChipsHTMLComponent(targetDiv, chipName, qty, chipID) {
    let td = $(targetDiv);
    let tbchip = `
  <div style="margin-top: 2px; cursor: pointer;"  id="${chipID}" class="chip hoverable" data-chipname="${chipName}">
  ${chipName} (${qty})
</div>
        `
    td.append(tbchip)
};




// use for Support page standard how to make a page from HTML Content
export function tbBlogPageHTMLComponent(targetDiv, htmlContent) {

    let td = $(targetDiv);
    let tbpage = htmlContent
    let containerOpeningDiv = `<div id="trythis" class="container"></div> `
    let containerClosingDiv = ``
    td.append(containerOpeningDiv)
    let td2 = $("#trythis");
    td2.append(tbpage)

};



// set up the column names and custom non dt header buttons
export function tbBlogFabHTMLComponent(targetDiv) {

    console.log(" ran blogFabHTMLComponent ")

    let td = $(targetDiv);
    let dtblogfabhtml = `
  <div id="blogTitleAndFab">
     <!-- blue action button -->
 
       <div id="tbBlogFab" style="top: 68px; right: 19px;" class="fixed-action-btn direction-bottom"><a
           class="btn-floating btn-large"><i
              class="material-icons">menu</i></a>
        <ul>
        <li><a title="Export Blog Items"href="#!" class="btnBackupDocumentData btn-floating red"><i class="material-icons">folder_open</i></a></li>
           <li><a title="Launch Blog Standard Grid View" href="#!" id="launchTbBlogStandardL1FAB" class="btn-floating blue"><i class="material-icons">grid_on</i></a></li>
           <li><a title="Launch Blog Grouped" Grid View" href="#!" class="launchTbBlogGroupedL1 btn-floating green"><i class="material-icons">format_line_spacing</i></a></li>
           <li><a title="Launch Blog Card View" href="#!" id="launchTbBlogCardsL1FABxxx" class="launchBlogArticles btn-floating blue"><i class="material-icons">widgets</i></a></li>
           <li><a title="Create new Blog Item"href="#!" id="newBlogItem" class="btn-floating amber"><i class="material-icons">add</i></a>
           </li>
        </ul>
     </div>
        `
    td.append(dtblogfabhtml)

};



export function tbBlogBreadCrumbHTMLComponent(targetDiv, pageName, L1, L2, L3) {

    let td = $(targetDiv);
    let tbBreadCrumbHTML = `
    <div class="pt-0 pb-1" id="breadcrumbs-wrapper">    
    <!-- Search for small screen-->
    <div class="container">
       <div class="row">
          <div class="col s12 m6 l6">
             <h5 class="breadcrumbs-title">${pageName} </h5>
          </div>
          <div class="col s12 m6 l6 right-align-md">
             <ol class="breadcrumbs mb-0">
                <li class="breadcrumb-item"><a href="#!">${L1}</a>
                </li>
                <li class="breadcrumb-item"><a href="#!">${L2}</a>
                </li>
                <li class="breadcrumb-item"><a href="#!">${L3}</a>
                </li>
             </ol>
          </div>
       </div>
    </div>
 </div>
        `
    td.append(tbBreadCrumbHTML)
};



export function tbBlogContactUsHtmlComponent(targetDiv) {

    console.log("targetDiv: " + targetDiv)

    let td = $(targetDiv);
    let contactUsHTML = `
    <div id="contactUsModal" class="modal modal-fixed-footer">
     <div id="contact-us" class="modal-content">
    <div class="app-wrapper">
        <!-- Start Header Contact us-->
        <div class="contact-header">
            <div class="row contact-us">
                <div class="col s12 m12 l4 sidebar-title">
                    <h5 class="m-0"><i class="material-icons contact-icon vertical-text-top">mail_outline</i>
                        Contact Us</h5>
                    <p class="m-0 font-weight-500 mt-6 hide-on-med-and-down">
                        Looking to get started? Click here to start using Timebars!</p>
                    <span class="social-icons hide-on-med-and-down"><i class="fab fa-behance"></i> <i
                            class="fab fa-dribbble ml-5"></i>
                        <i class="fab fa-facebook-f ml-5"></i> <i class="fab fa-instagram ml-5"></i></span>
                </div>
                <div class="col s12 m12 l8 form-header">
                    <h6 class="form-header-text"><i class="material-icons"> mail_outline </i>
                        Write us a few words about your
                        needs.</h6>
                </div>
            </div>
        </div>
<!-- END Header Contact us-->

        <div id="sidebar-list" class="row contact-sidenav">
            <!-- START Left Side contact us-->
            <div class="col s12 m12 l4">
                <div class="sidebar-left sidebar-fixed">
                    <div class="sidebar">
                        <div class="sidebar-content">
                            <div class="sidebar-menu list-group position-relative">
                                <div class="sidebar-list-padding app-sidebar contact-app-sidebar" id="contact-sidenav">
                                    <ul class="contact-list display-grid">
                                        <li>
                                            <p class="mt-5 line-height">But if you want a customized solution, contact us for assistance and a quote.</p>
                                        </li>
                                        <li>
                                           
                                        </li>
                                    </ul>
                                    <div class="row">
                                        <!-- Place -->
                                        <div class="col s12 place mt-4 p-0">
                                            <div class="col s2 m2 l2"><i class="material-icons">
                                                    place </i></div>
                                            <div class="col s10 m10 l10">
                                                <p class="m-0">180 Portrush Ave, <br> Ottawa, Ontario, K2J 5K1
                                                    </p>
                                            </div>
                                        </div>
                                        <!-- Phone -->
                                        <div class="col s12 phone mt-4 p-0">
                                            <div class="col s2 m2 l2"><i class="material-icons"> call
                                                </i></div>
                                            <div class="col s10 m10 l10">
                                                <p class="m-0">(613) 255-5374</p>
                                            </div>
                                        </div>
                                        <!-- Mail -->
                                        <div class="col s12 mail mt-4 p-0">
                                            <div class="col s2 m2 l2"><i class="material-icons">
                                                    mail_outline </i></div>
                                            <div class="col s10 m10 l10">
                                                <p class="m-0">jcox@tbcox.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- END Left Side Contact us -->
            
            <!-- START Form right side Contact us-->
            <div class="col s12 m12 l8 contact-form margin-top-contact">
                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input id="name" type="text" class="validate">
                                <label for="name">Your Name</label>
                            </div>
                            <div class="input-field col m6 s12">
                                <input id="email" type="text" class="validate">
                                <label for="email">Your e-mail</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col m6 s12">
                                <input id="company" type="text" class="validate">
                                <label for="company">Company</label>
                            </div>
                            <div class="input-field col m6 s12">
                                <input id="budget" type="text" class="validate">
                                <label for="budget">App budget</label>
                            </div>
                            <div class="input-field col s12 width-100">
                                <textarea id="textarea1" class="materialize-textarea"></textarea>
                                <label for="textarea1">Textarea</label>
                                <a class="waves-effect waves-light btn">Send</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- END Form right side Contact us -->

        </div>
    </div>
</div>
<div class="modal-footer">
<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat ">Close</a>
</div>
</div>
<!-- END contact page -->

    `
    td.append(contactUsHTML)
};


// new User Facing page pretty readable page
export function drawDocFormHTMLComponentUserFacingNew(targetDiv) {

    let td = $(targetDiv);
    let tbdocformHTML = `
    <!-- ---- Start Documents Form  -->
    <div id="launchDocFormUserFacing" class="modal" data-tbid="">
       <div class="modal-content" data-backdrop="static" data-keyboard="false">
          <!-- Left Half -->
          <div class="container">
             <form class="zzz">
                <div class="row">
                   <div class="col s10" style="margin-bottom: 20px">
                      <h5 class="valign-wrapper"> <i
                            class="material-icons indigo-text valign-wrapper">description</i>
                            <div id="tbDocName_blog"></div>
                      </h5>
                   </div>
                   <div class="col s2">
                      <a title="Close without saving!"
                         class="closeDocForm modal-close waves-effect waves-blue btn-flat">Close</a>
                   </div>
                </div>
                <div class="row" style="margin-bottom: 20px" ;>
                   <div class="col s12">
                      <div id="tbDocBriefDescription_blog" style="margin-bottom: 30px"
                         data-length="250"></div>
                   </div>
                   <div class="col s12">
                      <div id="tbDocHTMLContent_blog" data-formkey="md-blog-na">
                         
                      </div>
                   </div>
                </div>
                <div class="input-field col s2">
                  <input disabled id="tbDocID_blog" type="text" class="validate">
                 <label for="tbDocID_blog">Unique ID</label>
               </div>
             </form>
          </div>
       </div>
    </div>
      `
    td.append(tbdocformHTML)
};




// set up the column names for data table 
export function dtBlogTableHeaderHTMLComponent(targetDiv) {

    let td = $(targetDiv);
    let dtblogstdhtml = `
        <div id="dataTableDocumentsBlogL1">
    <div id="docUploadDropArea" style="margin-top: 5px; font-size: 24px;">Blog Article Listing
        <span style="margin-left: 300px; font-size: 12px;">Find and Edit Blog Articles</span>
    </div>
    <!--
    <button id="btnkkk" style="margin-right: 5px;"
        class="btn-small waves-effect waves-light grey darken-4 left"> New Items
    </button>
   
    <button style="margin-right: 5px;"
        class="btnBackupDocumentData btn-small waves-effect waves-light grey darken-4 left"> Data Backup
    </button>
   table headers area -->
    <table id="dataTableDocuments" class="display" style="width:100%">
        <thead>
            <tr>
                <th></th>
                <th>Id</th>
                <th>Document Name</th>
                <th>L1</th>
                <th>L2</th>
                <th>L3</th>
                <th>Purpose</th>
                <th>Owner</th>
                <th>Status</th>
                <th>Sort</th>
                <th>Popular</th>
                <th>Written By</th>
                <th>Delete</th>
            </tr>
        </thead>
        <tbody id="displayDtDocuments">
        </tbody>
        </tfoot>
    </table>
</div> `
    td.append(dtblogstdhtml)

};


// Document Purpose
// Document Status
// Document Type
// State
// Document L1
// Document L2
// Document L3
// Document L4
// Document L5
// Document Group
// Document Popularity
// Document Written By
// Document Approved By
// Document Image Path



// main or legacy form
export function docFormHTMLComponent(targetDiv) {
    let td = $(targetDiv);
    let docformhtml = `


<!-- ---- Start Documents Form  -->
<div id="launchDocumentsForm" class="modal" data-tbid="">
   <div class="modal-content" data-backdrop="static" data-keyboard="false">
      <!-- Left Half -->
      <div class="containerzzz">
         <form class="zzz">
            <div class="row">
               <div class="input-field col s8">
                  <h5 class="valign-wrapper"> <i class="material-icons medium indigo-text valign-wrapper">control_point</i>Timebars CMS Form</h5>
               </div>
               <div class="input-field col s2">
                  <input disabled id="tbDocID" type="text" class="validate">
                  <label for="tbDocID">Unique ID</label>
               </div>
               <div class="input-field col s2">
               <a href="#!" title="Save to the database"
               class="btnSaveFormDataDocument waves-effect waves-blue btn-flat">Save</a>
                  <a title="Close without saving!"
                     class="closeDocForm modal-close waves-effect waves-blue btn-flat">Close</a>
               </div>
               </div>
             
               <div class="row">
               <div class="input-field col s12">
                  <i class="material-icons prefix blue-text">navigate_next</i>
                  <input id="tbDocName" type="text" class="validate" required="">
                  <label for="tbDocName">Document Name:</label>
                  <span class="helper-text" data-error="You must enter a value!" data-success="ok"></span>
               </div>
            </div>
            <div class="row" style="margin-bottom: 20px" ;>
               <div class="col s12">
                  <div id="tbDocHTMLContent" class="updatableStd" data-formkey="md-blog-na">
                  </div>

               </div>
            </div>

            <div class="row">
               <div class="input-field col s12">
                  <i class="material-icons prefix blue-text">navigate_next</i>
                  <textarea id="tbDocBriefDescription" class="materialize-textarea"
                     data-length="250"></textarea>
                  <label for="tbDocBriefDescription">Brief Description</label>
               </div>
            </div>

<div class="row">
    <div class="input-field col s4">
       <i class="material-icons prefix blue-text">arrow_drop_down</i>
       <input id="tbDocL1" type="text" class="validate mcttUpdatablev1"
          data-formkey="md-docform-Document L1">
       <label for="tbDocL1">Level 1 (L1)</label>
    </div>
    <div class="input-field col s4">
       <i class="material-icons prefix blue-text">navigate_next</i>
       <input id="tbDocL2" type="text" class="validate mcttUpdatablev1" data-formkey="md-docform-Document L2">
       <label for="tbDocL2">Level 2 (L2)</label>
    </div>
    <div class="input-field col s4">
       <i class="material-icons prefix blue-text">navigate_next</i>
       <input id="tbDocL3" type="text" class="validate mcttUpdatablev1" data-formkey="md-docform-Document L3">
       <label for="tbDocL3">Level 3 (L3)</label>
    </div>
 </div>
 <div class="row">
    <div class="input-field col s4">
       <i class="material-icons prefix blue-text">arrow_drop_down</i>
       <input id="tbDocL4" type="text" class="validate mcttUpdatablev1 data-formkey="md-docform-Document L4"
          data-formkey="md-docform-Investment Category">
       <label for="tbDocL4">Level 4 (L4)</label>
    </div>
    <div class="input-field col s4">
       <i class="material-icons prefix blue-text">navigate_next</i>
       <input id="tbDocL5" type="text" class="validate mcttUpdatablev1" data-formkey="md-docform-Document L5">
       <label for="tbDocL5">Level 5 (L5)</label>
    </div>
 </div>
 <div class="row">
    <div class="input-field col s12">
       <i class="material-icons prefix blue-text">navigate_next</i>
       <input id="tbDocCardImage" type="text" class="validate mcttUpdatablev1" data-formkey="md-docform-Document Images">
       <label for="tbDocCardImage">Image Relative Path</label>
    </div>
 </div>
            <div class="row">
               <div class="input-field col s8">
                  <i class="material-icons prefix blue-text">navigate_next</i>
                  <input id="tbDocNameShort" type="text" class="validate" data-length="20">
                  <label for="tbDocNameShort">Short Name</label>
               </div>

               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">arrow_drop_down</i>
                  <input id="tbDocType" type="text" class="validate mcttUpdatablev1"
                     data-formkey="md-docform-Document Type">
                  <label for="tbDocType">Type of Document</label>
               </div>
            </div>

            <div class="row">
               <div class="input-field col s8">
                  <i class="material-icons prefix blue-text">arrow_drop_down</i>
                  <input id="tbDocPurpose" type="text" class="validate mcttUpdatablev1"
                     data-formkey="md-docform-Document Purpose">
                  <label for="tbDocPurpose">Purpose</label>
               </div>
               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">arrow_drop_down</i>
                  <input id="tbDocStatus" type="text" class="validate mcttUpdatablev1"
                     data-formkey="md-docform-Document Status">
                  <label for="tbDocStatus">Status</label>
               </div>
            </div>

            <div class="row">
               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">navigate_next</i>
                  <input id="tbDocSortOrder" type="text" class="validate" required="">
                  <label for="tbDocSortOrder">Sort Order</label>
               </div>
               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">arrow_drop_down</i>
                  <input id="tbDocOwner" type="text" class="validate mcttUpdatablev1"
                     data-formkey="people-docform-Users">
                  <label for="tbDocOwner">Owner</label>
               </div>
            </div>


            <div class="row">
               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">arrow_drop_down</i>
                  <input id="tbDocGroup" type="text" class="validate mcttUpdatablev1" data-formkey="md-docform-Document Grouping">
                  <label for="tbDocGroup">Grouping Tag</label>
               </div>
               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">navigate_next</i>
                  <input id="tbDocPopular" type="text" class="validate mcttUpdatablev1" data-formkey="md-docform-YesNo">
                  <label for="tbDocPopular">Popularity Tag</label>
               </div>
               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">navigate_next</i>
                  <input id="tbDocCustomerID" type="text" class="validate">
                  <label for="tbDocCustomerID">Customer ID</label>
               </div>
            </div>

            <div class="row">
               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">arrow_drop_down</i>
                  <input id="tbDocWrittenBy" type="text" class="validate mcttUpdatablev1"
                     data-formkey="people-docform-Users">
                  <label for="tbDocWrittenBy">Written By</label>
               </div>

               <div class="input-field col s4">
                  <i class="material-icons prefix blue-text">arrow_drop_down</i>
                  <input id="tbDocApprovedBy" type="text" class="validate mcttUpdatablev1"
                     data-formkey="people-docform-Business Owners">
                  <label for="tbDocApprovedBy">Approved By</label>
               </div>
               <div class="input-field col s4">
                  <input disabled id="xxx" type="text" class="validate">
                  <label for="xxx">Not Used</label>
               </div>
            </div>
         </form>
         <div class="modal-footer">
            <button title="Shortcut to PDF Printing"
               class="waves-effect waves-light btn-small blue darken-4"
               id="createPRCFromFormForDownLoad">Print PDF</button>
            <a href="#!" title="Save to the database"
               class="btnSaveFormDataDocument waves-effect waves-blue btn-flat">Save</a>
            <a title="Close without saving!"
               class="closeDocForm modal-close waves-effect waves-blue btn-flat">Close</a>

         </div>
      </div>
   </div>
</div>
<!--  ----END Document Form  --></div>

          `
    td.append(docformhtml)
};
