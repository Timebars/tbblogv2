/* Login and Account and Profile section */


import { reloadPage, clearProjectDataTbBlMD, exportAllData, populateMandatoryData, openDB, openIDBGetStore, displayFailureMessageDB, displaySuccessMessageDB, deleteDB, populateDemoData } from './tbdatabase';
import { PWDManagementHTMLComponent, manageAccountHTMLComponent, tbBlogBreadCrumbHTMLComponent } from './htmlblogcomponents';


import * as jwt_decode from "jwt-decode";


const url = 'http://localhost:3001'
const mytoken = []


// get login status
function logInStatus() {

    let tokenp = localStorage.getItem('jtoken')
    // console.log("no token: " + tokenp)
    if (tokenp === null) {
        M.toast({
            html: "You are not logged in",
            classes: 'orange darken-3 rounded'
        });
    } else {
        M.toast({
            html: "You are still logged in.",
            classes: 'orange darken-3 rounded'
        });
    }
}
logInStatus()


/* START Register and login with tokens */

$(document).on('click', '#forgotPwdEmail', function () {
    forgotPwdEmail()
});
function forgotPwdEmail() {
    console.log("here changePWD")
    let jemail = document.getElementById('emailChangePWD').value;


    console.log("jemail: " + jemail)

    fetch(`${url}/api/v1/auth/forgotpassword`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            email: jemail,
        })
    })

        .then((res) => res.json())
        .catch((err1) => {

            M.toast({
                html: `Server is down! Try to log in again. Error: ${err1}}`,
                classes: 'orange darken-3 rounded'
            });

            return
        })
        .then((data) => {
            // console.log(data)
            if (data.success === false) {
                console.log("Error:" + data.error)
                M.toast({
                    html: data.error,
                    classes: 'orange darken-3 rounded'
                });
                return
            } else {
                console.log("200 Successfully send email for password change")
                M.toast({
                    html: `Successfully send email for password change to: ${jemail}`,
                    classes: 'orange darken-3 rounded'
                });
            }
        });
}



// change password  see PWDManagementHTMLComponent

$(document).on('click', '#changePWD', function () {
    changePWD()
});
function changePWD() {
    let output = '';

    console.log("here changePWD")
    let jemail = document.getElementById('emailChangePWD').value;
    let jpassword = document.getElementById('passwordChangePWD').value;
    let jpassword2 = document.getElementById('password2ChangePWD').value;

    console.log(" jpass: " + jpassword2)

    fetch(`${url}/api/users/changepwd`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            email: jemail,
            password: jpassword,
            password2: jpassword2
        })
    })

        .then((res) => res.json())
        .catch((err1) => {

            M.toast({
                html: `Server is down! Try to log in again. Error: ${err1}}`,
                classes: 'orange darken-3 rounded'
            });

            return
        })
        .then((data) => {
            // console.log(data)
            if (data.success === false) {
                console.log("Error:" + data.error)
                M.toast({
                    html: data.error,
                    classes: 'orange darken-3 rounded'
                });
                return
            } else {
                console.log("200 Successfully changed password")
                M.toast({
                    html: `Successfully changed password for: ${jemail}`,
                    classes: 'orange darken-3 rounded'
                });
            }
        });
}





// register a new user
$(document).on('click', '.registerNewUser', function () {
    registerNewUser()
});
function registerNewUser() {
    let output = '';

    console.log("here")
    let jname = document.getElementById('userNameRegForm').value;
    let jemail = document.getElementById('emailRegForm').value;
    let jpassword = document.getElementById('passwordRegForm').value;
    let jpassword2 = document.getElementById('password2RegForm').value;

    console.log(" jpass: " + jpassword2)

    fetch(`${url}/api/v1/auth/register`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type': 'application/json'
        },
        body: JSON.stringify({
            name: jname,
            email: jemail,
            password: jpassword,
            password2: jpassword2
        })
    })

        .then((res) => res.json())
        .catch((err1) => {

            M.toast({
                html: `Server is down! Try to log in again. Error: ${err1}}`,
                classes: 'orange darken-3 rounded'
            });

            return
        })
        .then((data) => {
            // console.log(data)
            if (data.success === false) {
                console.log("Error:" + data.error)
                M.toast({
                    html: data.error,
                    classes: 'orange darken-3 rounded'
                });
                return
            } else {
                console.log("200 Successfully registered")
                M.toast({
                    html: `Successfully registered: ${jemail}`,
                    classes: 'orange darken-3 rounded'
                });
            }
        });
}


// use this for auth, send email and pwd
// login the new user, this returns the token if log in success, pass token around in other calls
$(document).on('click', '#loginNowFromManageAccountFrom', function (e) {

    let jemail = $('#emailManageAccountForm').val()
    console.log("jemail: " + jemail)
    let jpassword = $('#pwdManageAccountForm').val()
    logMeIn(jemail, jpassword, e)
});

$(document).on('click', '.performLogin', function (e) {

    let jemail = $('#email2').val()
    console.log("jemail2: " + jemail)
    let jpassword = $('#password2').val()

    logMeIn(jemail, jpassword, e)
});


function logMeIn(jemail, jpassword, e) {
    e.preventDefault();
    // const mytoken = []
    // let jemail = 'jc@me.com'
    // let jpassword = '111111'
    //let jemail = document.getElementById('emailManageAccountForm').value;
    //let jpassword = document.getElementById('pwdManageAccountForm').value;

    fetch(`${url}/api/v1/auth/login`, {
        //fetch(`${url}/api/users/login`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-type': 'application/json'
        },
        body: JSON.stringify({

            email: jemail,
            password: jpassword,

        })
    })
        .catch((err) => {
            document.getElementById('showLoginInfo').innerHTML += `<br />Server may be down! Try to log in again or call support. <br /> ${err}}`

            M.toast({
                html: 'Server may be down! Try to log in again or call support.',
                classes: 'orange darken-3 rounded'
            });

            return
        })
        .then((res) => res.json())
        .then((data) => {

            // console.log(`i logged again ${data.token}`)
            var jtoken = data.token
            //  console.log("fresh token: " + data)

            if (jtoken) {
                mytoken.push(jtoken)

                if (typeof (Storage) !== "undefined") {
                    localStorage.setItem("jtoken", jtoken);
                } else {

                    M.toast({
                        html: 'Sorry! No Web Storage support...',
                        classes: 'orange darken-3 rounded'
                    });
                }
                //  console.log("mytoken 0: " + mytoken[0])
                document.getElementById('curLoggedInUser').innerHTML = `Current logged in user: ${jemail}`;

                M.toast({
                    html: `You are now logged in ${jemail}!`,
                    classes: 'orange darken-3 rounded'
                });

            } else {

                // document.getElementById('showLoginInfo').innerHTML += `<ul><li>User name or password not correct.</li></ul> `;
                M.toast({
                    html: 'User name or password not correct.',
                    classes: 'orange darken-3 rounded'
                });
            }
        })
}
// <ul><li></li></ul>


// /current get token show i am logged in
$(document).on('click', '#showNameFromManageAccountForm', function (e) {
    showMe()
});
function showMe() {
    // 'x-auth-token': mytoken[0],
    let output = '';
    let jtoken = localStorage.getItem('jtoken')
    jtoken = `Bearer ${jtoken}`

    // uses token in memory or local storage
    fetch(`${url}/api/v1/auth/me`, {

        method: 'GET',
        headers: {
            'Authorization': jtoken,
            'Accept': 'application/json, text/plain, */*',
            'Content-type': 'application/json'
        },
    })
        .then((res) => res.json())
        .catch((err1) => {
            //   document.getElementById('showLoginInfo').innerHTML += `<ul><li>Server is down! Try to log in again. Error: ${err1}}</li></ul>`

            M.toast({
                html: `Server is down! Try to log in again. Error: ${err1}}`,
                classes: 'orange darken-3 rounded'
            });

            return
        })
        .then((data) => {
            // console.log(data)
            if (data.success === false) {
                console.log("Error:" + data.error)
                M.toast({
                    // html: `Unauthorized: Response Status: ${res.status}, password may be wrong or you are not logged in, try to log in again.}`,
                    html: data.error,
                    classes: 'orange darken-3 rounded'
                });
                return
            } else {
                console.log("200 Authorized")
                M.toast({
                    html: `Logged in:${JSON.stringify(data.data.name)}`,
                    classes: 'orange darken-3 rounded'
                });
            }
        });
}

//  get email from local token, not from server - show i am logged in
$(document).on('click', '#showEmailFromManageAccountForm', function (e) {
    showEmailFromManageAccountForm()
});
function showEmailFromManageAccountForm() {
    let output = '';
    let tokenp = []
    let jtoken = localStorage.getItem('jtoken')

    console.log("j token: " + jtoken)

    let t = localStorage.getItem('jtoken')
    tokenp.push(t)

    try {

        tokenp = JSON.parse(atob(tokenp[0].split('.')[1]))
    }
    catch (err) {

        M.toast({
            html: `You are not logged in yet, or server is down! Try to log in again`,
            classes: 'orange darken-3 rounded'
        });
    }
    finally {
        M.toast({
            html: `Current Logged in User ID: ${tokenp.id}  and email: ${tokenp.id}`,
            classes: 'orange darken-3 rounded'
        });

    }
    // we shall use current token in memory to display login info
}


//  get email from local token, not from server - show i am logged in
$(document).on('click', '.performLogout', function (e) {
    performLogoutNew()
});

function performLogoutNew() {

    let jtoken = localStorage.getItem('jtoken')
    jtoken = `Bearer ${jtoken}`

    fetch(`${url}/api/v1/auth/logout`, {

        method: 'GET',
        headers: {
            'Authorization': jtoken,
            'Accept': 'application/json, text/plain, */*',
            'Content-type': 'application/json'
        },
    })
        .catch((err) => {

            M.toast({
                html: 'Server may be down! Try to log in again or call support.',
                classes: 'orange darken-3 rounded'
            });

            return
        })
        .then((res) => res.json())
        .then((data) => {

            // console.log(data)
            if (data.success === false) {
                console.log("Error:" + data.error)
                M.toast({
                    html: data.error,
                    classes: 'orange darken-3 rounded'
                });
                return
            } else {
                console.log("Logged out")
                M.toast({
                    html: `Logged Out?: ${JSON.stringify(data.success)}`,
                    classes: 'orange darken-3 rounded'
                });
            }


        })

    performLogoutOld()
}




function performLogoutOld() {

    let output = '';
    let tokenp = localStorage.getItem('jtoken')
    console.log("no token: " + tokenp)

    if (tokenp === null) {
        M.toast({
            html: "You are not logged in",
            classes: 'orange darken-3 rounded'
        });

    } else {

        mytoken[0] = ""
        localStorage.removeItem('jtoken');

        M.toast({
            html: "We Logged out.",
            classes: 'orange darken-3 rounded'
        });


    }
}




/* END Register and login with tokens */










/* START Form launching logic */

// Register form
$(document).on('click', '.launchRegisterForm', function () {

    $("#divLoginSystemHTMLComponents").empty()
    let targetDiv = '#divLoginSystemHTMLComponents'
    RegistHTMLComponent(targetDiv)
    // make the contact form a modal
    var elem = document.querySelector('#RegisterFormModal');
    var options = {
        dismissible: true
    }
    var instance = M.Modal.init(elem, options);
    instance.open();
    M.updateTextFields()

});


// login form
$(document).on('click', '.launchlLoginForm', function () {

    // console.log("here ")
    // show current logged in user
    let token = []
    let t = localStorage.getItem('jtoken')
    token.push(t)

    try {
        token = JSON.parse(atob(token[0].split('.')[1]))
    }
    catch (err) {
        console.log("token not in local cache")
    }
    finally {
        let currentUser = token.name

        $("#divLoginSystemHTMLComponents").empty()
        let targetDiv = '#divLoginSystemHTMLComponents'

        LoginFormHTMLComponent(targetDiv, currentUser)

    }




    var elem = document.querySelector('#LoginFormModal');
    var options = {
        dismissible: true
    }
    var instance = M.Modal.init(elem, options);
    instance.open();
    M.updateTextFields()

});


// profile page
$(document).on('click', '.launchProfilePage', function () {

    launchprofilePage()
});
function launchprofilePage() {
    // first remove any existing pages
    $("#divTbPagesL1").css("visibility", "visible");
    $("#divTbPagesL1").empty()

    let pageName = "Profile"
    // console.log("launced home")
    let targetDiv = '#divTbPagesL1'
    let L1 = "Home"
    let L2 = "Profile"
    let L3 = ""
    tbBlogBreadCrumbHTMLComponent(targetDiv, pageName, L1, L2, L3)
    profilePageHTMLComponent(targetDiv)

    $('.tabs').tabs();
}




// Password management form
$(document).on('click', '.launchPasswordManagement', function () {

    $("#divLoginSystemHTMLComponents").empty()
    let targetDiv = '#divLoginSystemHTMLComponents'
    PWDManagementHTMLComponent(targetDiv)
    // make the contact form a modal
    var elem = document.querySelector('#PWDManagementModal');
    var options = {
        dismissible: true
    }
    var instance = M.Modal.init(elem, options);
    instance.open();
    M.updateTextFields()
});


// Account management form
$(document).on('click', '.launchManageAccount', function () {

    $("#divLoginSystemHTMLComponents").empty()
    let targetDiv = '#divLoginSystemHTMLComponents'
    manageAccountHTMLComponent(targetDiv)
    // make the contact form a modal
    var elem = document.querySelector('#manageAccountModal');
    var options = {
        dismissible: true
    }
    var instance = M.Modal.init(elem, options);
    instance.open();
    M.updateTextFields()
});

/* START END launching logic */




/* START HTML Components */

export function RegistHTMLComponent(targetDiv) {

    let td = $(targetDiv);
    let regform = `
    <div id="RegisterFormModal" class="modal modal-fixed-footer">
    <div class="modal-content">
            <div class="container">
                <div id="register-page" class="row">
                        <form class="login-form">
                            <div class="row">
                             
                                <div class="input-field col s12">
                                    <h5 class="ml-4">Register</h5>
                                    <a class="modal-close right" title="Close without registering!" href="#!"><i
                                        class="material-icons">close </i></a>
                                    <p class="ml-4">Join to our community now !</p>
                                </div>
                            </div>
                            <div class="row margin">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">person_outline</i>
                                    <input id="userNameRegForm" type="text" value="Jimmy">
                                    <label for="userNameRegForm" class="center-align">Username</label>
                                </div>
                            </div>
                            <div class="row margin">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">mail_outline</i>
                                    <input id="emailRegForm" type="email" value="jcox@tbcox.com">
                                    <label for="emailRegForm">Email</label>
                                </div>
                            </div>
                            <div class="row margin">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">lock_outline</i>
                                    <input id="passwordRegForm" type="password" value="11111111">
                                    <label for="passwordRegForm">Password</label>
                                </div>
                            </div>
                            <div class="row margin">
                                <div class="input-field col s12">
                                    <i class="material-icons prefix pt-2">lock_outline</i>
                                    <input id="password2RegForm" type="password" value="11111111">
                                    <label for="password2RegForm">Password again</label>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col s6 m6 l6">
                                <p class="margin medium-small">
                                        <button class="registerNewUser btn indigo waves-effect waves-light">Register
                                                <i class="material-icons right">send</i>
                                            </button>
                                </p>
                            </div>
                            <div class="col s6 m6 l6">
                                <p class="margin right-align medium-small">
                                        <p class="margin medium-small">Already have an account? </p>
                                        <a class="launchlLoginForm" title="Log in from here!" href="#!">Login!</a>
                                </p>
                            </div>
                        </div>

                        </form>
                    </div>
                </div>
            </div>

    </div>
    </div>
    </div>
          `
    td.append(regform)
};



// login
export function LoginFormHTMLComponent(targetDiv, currentUser) {

    let td = $(targetDiv);
    let modalLogin = `
    <div id="LoginFormModal" class="modal">
    <div class="modal-content">
        <form class="login-form">

            <div class="row">
                <div class="col s6 m6 l6">
                    <h5 class="ml-4">Sign in</h5>
                </div>
                <div class="col s6 m6 l6">
                    <a class="modal-close right" title="Close without logging in!" href="#!"><i
                            class="material-icons">close </i></a>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">person_outline</i>
                    <input id="email2" type="text" value="jcox@tbcox.com">
                    <label for="email2" class="center-align">Email Address</label>
                </div>
            </div>
            <div class="row margin">
                <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">lock_outline</i>
                    <input id="password2" type="password" value="88888888">
                    <label for="password2">Password</label>
                </div>
            </div>
            <div class="row">
                <div class="col s12 m12 l12 ml-2 mt-1">
                </div>
            </div>
            <div class="row">
                <div class="input-field col s12">
                    <a id="performLogin" href="!#"
                        class="performLogin btn waves-effect waves-light border-round col s12">Login</a>
                </div>
            </div>
            <div class="row">
            </div>


            <div class="row">
                <div class="input-field col s6 m6 l6">
                    <p class="margin medium-small"><a class="launchRegisterForm" href="#!">Register
                            Now!</a>
                    </p>
                </div>
                <div class="input-field col s6 m6 l6">
                    <p class="margin right-align medium-small"><a class="launchPasswordManagement" href="#!">Forgot
                            password?
                        </a>
                    </p>
                </div>
            </div>
            <div class="row">
                <div id="curLoggedInUser" class="col s6 m6 l6 medium-small">
                    Current logged in user: ${currentUser}
                </div>
                <div class="col s6 m6 l6 right-align">
                    <a href="#!" class="performLogout medium-small">Log Out Now!</a>
                </div>
            </div>
        </form>
    </div>
</div>

          `
    td.append(modalLogin)
};





// three section page template
export function profilePageHTMLComponent(targetDiv) {
    let td = $(targetDiv);
    let profilepg = `
    <!-- START 3 Col Profile Page-->
    <div id="mainxxx">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section" id="user-profile">
                        <div class="row">
                            <!-- Start Left Section -->
                            <div class="col s12 m4 l3 user-section-negative-margin">
    
                                <div class="row">
                                    <div class="col s12">
                                        stuff1
                                    </div>
                                </div>
                                <hr>
                                <div class="row user-projects">
                                    <h6 class="col s12">Projects Section 1</h6>
                                </div>
                                <hr class="mt-5">
                                <div class="row">
                                    <div class="col s12">
                                        <h6>Miscl Section 2</h6>
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    stuff2
                                    <div class="col s9">
                                        stuff3
                                    </div>
                                </div>
                                <div class="row mt-2">
                                    <div class="col s9">
    
                                        stuff4
                                    </div>
                                </div>
                            </div>
                            <!-- END Left Section -->
    
                            <!-- START middle Section -->
                            <div class="col s12 m8 l6">
                                <div class="row">
                                    <div class="card user-card-negative-margin z-depth-0" id="feed">
                                        <div class="card-content card-border-gray">
                                            <div class="row">
                                                <div class="col s12">
                                                    <h5>My Profile Details</h5>
                                                </div>
                                            </div>
                                            <!-- Tab in center top -->
                                            <div class="row">
                                                <div class="col s12">
                                                    <ul class="tabs card-border-gray mt-4">
                                                        <li class="tab col m3 s6 p-0"><a class="active" href="#test1">
                                                                <i
                                                                    class="material-icons vertical-align-middle">crop_portrait</i>
                                                                General
                                                            </a></li>
                                                        <li class="tab col m3 s6 p-0"><a href="#test2">
                                                                <i
                                                                    class="material-icons vertical-align-middle">bookmark_border</i>
                                                                Experience
                                                            </a></li>
                                                        <li class="tab col m3 s6 p-0"><a href="#test3">
                                                                <i
                                                                    class="material-icons vertical-align-middle">date_range</i>
                                                                Education
                                                            </a></li>
                                                        <li class="tab col m3 s6 p-0"><a href="#test4">
                                                                <i
                                                                    class="material-icons vertical-align-middle">attach_file</i>
                                                                Other
                                                            </a></li>
                                                    </ul>
                                                </div>
                                                <div id="test1" class="col s12">
                                                    <div class="row">
                                                        <div class="col s12">
                                                            <h5>General</h5>
                                                        <p>This section will house general information the registered user wishes to share.</p>
                                                        
                                                        <button id="showPostsFromProfile" class="btn indigo">Show Posts!</button>
                                                        <div id="divShowPost" class="sectionxxx">
                                                        <!-- Insert content  here for login form, chg pwd etc-->
                                                     </div>
                                                        
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Experience Fields -->
                                                <div id="test2" class="col s12">
                                                    <div class="col s12">
                                                        <h5>Experience</h5>
    
                                                        <form class="form">
                                                            <div class="form-group">
                                                                <input type="text" placeholder="Job Title" name="title"
                                                                    required />
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" placeholder="Company" name="company"
                                                                    required />
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="text" placeholder="Location" name="location"
                                                                    required />
                                                            </div>
                                                            <div class="form-group">
                                                                <h6>From Date</h6>
                                                                <input type="date" name="from" />
                                                            </div>
                                                            <div class="form-group">
                                                                <input type="checkbox" name="current" value="" />
    
                                                            </div>
                                                            <div class="form-group">
                                                                <h6>To Date</h6>
                                                                <input type="date" name="to" />
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea name="description" cols="30" rows="5"
                                                                    placeholder="Job Description"></textarea>
                                                            </div>
                                                            <input type="submit" class="btn btn-primary my-1" />
                                                            <a class="btn my-1" href="#!">Go
                                                                Back</a>
                                                        </form>
    
                                                    </div>
                                                </div>
                                                <!-- Education Fields -->
                                                <div id="test3" class="col s12">
                                                    <div class="col s12">
                                                        <h5>Education</h5>
                                                        <form class="form">
                                                        <div class="form-group">
                                                          <input
                                                            type="text"
                                                            placeholder="* School or Bootcamp"
                                                            name="school"
                                                            required
                                                          />
                                                        </div>
                                                        <div class="form-group">
                                                          <input
                                                            type="text"
                                                            placeholder="* Degree or Certificate"
                                                            name="degree"
                                                            required
                                                          />
                                                        </div>
                                                        <div class="form-group">
                                                          <input type="text" placeholder="Field Of Study" name="fieldofstudy" />
                                                        </div>
                                                        <div class="form-group">
                                                          <h6>From Date</h6>
                                                          <input type="date" name="from" />
                                                        </div>
                                                        <div class="form-group">
                                                          <p>
                                                            <input type="checkbox" name="current" value="" /> Current School or Bootcamp
                                                          </p>
                                                        </div>
                                                        <div class="form-group">
                                                          <h6>To Date</h6>
                                                          <input type="date" name="to" />
                                                        </div>
                                                        <div class="form-group">
                                                          <textarea
                                                            name="description"
                                                            cols="30"
                                                            rows="5"
                                                            placeholder="Program Description"
                                                          ></textarea>
                                                        </div>
                                                        <input type="submit" class="btn btn-primary my-1" />
                                                        <a class="btn btn-light my-1" href="dashboard.html">Go Back</a>
                                                      </form>
                                                    </div>
                                                </div>
                                                <div id="test4" class="col s12">
                                                    <div class="col s12">
                                                        <h5>Links and other Information</h5>
                                                        <P>Here is a link to xyz</P>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-5">
                                                <div class="col s11">
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END middle section -->
    
                            <!-- Start Right section -->
                            <div class="col s12 m12 l3 hide-on-med-and-down">
                                <div class="row mt-5">
                                    <div class="col s12">
                                        <h6>Miscl Section1 rh side</h6>
                                    </div>
                                </div>
                                <hr class="mt-5">
                                <div class="row mt-5">
                                    <div class="col s12">
                                        <h6>Miscl section2 rh side</h6>
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col s2 pr-0 circle">
    
                                    </div>
                                    <div class="col s9">
    
                                    </div>
                                </div>
                                <hr class="mt-0">
                                <div class="row">
                                    <div class="col s12">
                                        <h6>Miscl section3 rh side</h6>
    
                                    </div>
                                </div>
                            </div>
                            <!-- END Right section -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
        <!-- END 3 Col Profile Page-->

          `
    td.append(profilepg)
};


