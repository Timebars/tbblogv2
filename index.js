// main js file linked to the html page
import "./scripts/import-jquery";
import './app-assets/images/favicon/apple-touch-icon-152x152.png';
import $ from "jquery";
require('datatables.net')(window, $);

import XLSX from 'xlsx'

import { } from './scripts/xlsx';

// js files
import './app-assets/js/plugins';
import './app-assets/js/vendors.min';
import './app-assets/js/scripts/page-contact.js';
import './app-assets/vendors/dropify/js/dropify';
import './app-assets/js/custom/custom-script';
import './app-assets/js/scripts/page-contact'

import { displayFailureMessageDB, displaySuccessMessageDB, updateOneDocumentField, openDB } from './scripts/tbdatabase';

import { drawTbBlogCardsNew, drawBlogChips, fillDocumentsFormFromBlogCardsNew, resetDocumentsInput, saveFormDataDocument, getDocuments, createDocument, fillDocumentsForm, getEditorDataContentMain, makeMcttMetaDataOrRes, clearStoreTable, xlsxJsonDataPopulator, exportAllDataAsCSV } from './scripts/focdblog'

// import { openIDBGetStore, updateOneFieldTimebarIDB, updateOneMetadataField, updateOneDocumentField } from '../scripts/tbdatabase';
import { tbBlogBreadCrumbHTMLComponent, tbBlogHomePageHTMLComponent, drawDocFormHTMLComponentUserFacingNew, dtBlogTableHeaderHTMLComponent, tbBlogFabHTMLComponent, DropifyXlsxModalHTMLComponent } from './scripts/htmlblogcomponents';

import { contactMailPageHTMLComponent } from './scripts/mailerForm';

import { } from './scripts/userAccountLoginProfile';

import { } from './scripts/focdresources';

import { } from './scripts/focdtags';

/* Initialize the App */
openDB()
launchBlogHomePage()


$(document).on('click', '#btnExportAlltoCSV', function () {
    exportAllDataAsCSV()
});


// START XLSX drop component xlsx

$(document).on('click', '.launchDropifyXlsx', function () {
    let targetDiv = '#divDropifyXlsxHTMLComponent'
    DropifyXlsxModalHTMLComponent(targetDiv)

    var elem = document.querySelector('#DropifyXlsxModal');
    var options = {
        dismissible: true
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    initDropifyXlsx()

});

function initDropifyXlsx() {

    //Force back up of files first
    confirm("We are forcing you to back up your data to CSV now!")
    exportAllDataAsCSV()

    console.log("here xlsx")
    // Used events
    //  var drEvent = $('#fileevents').dropify({
    var drEvent = $('.dropify').dropify({
        tpl: {
            wrap: '<div class="dropify-wrapper"></div>',
            loader: '<div class="dropify-loader"></div>',
            message: '<div class="dropify-message"><span class="file-icon" /> <p>{{ default }}</p></div>',
            preview: '<div class="dropify-preview"><span class="dropify-render"></span><div class="dropify-infos"><div class="dropify-infos-inner"><p class="dropify-infos-message">{{ replace }}</p></div></div></div>',
            filename: '<p class="dropify-filename"><span class="file-icon"></span> <span class="dropify-filename-inner"></span></p>',
            clearButton: '<button type="button" class="dropify-clear">{{ remove }}</button>',
            errorLine: '<p class="dropify-error">{{ error }}</p>',
            errorsContainer: '<div class="dropify-errors-container"><ul></ul></div>'
        },

        messages: {
            'default': 'Drop Timebars Excel file here!',
            'replace': 'Drop or Click to replace me!',
            'remove': 'Remove the file',
            'error': 'Ooops, something wrong, refresh page..'
        }


    });

    drEvent.on('dropify.beforeClear', function (event, element) {
        // console.log("here2")
        return confirm("Do you really want to remove \"" + element.filename + "\" ?");
    });

    drEvent.on('dropify.afterClear', function (event, element) {
        alert('File removed');
    });

    drEvent.on('dropify.errors', function (event, element) {
        console.log('dropify Has Errors');
    });
    // end drop content



    // using xlsx npm package import directly into idb from excel
    let drop = document.getElementById('DropifyXlsxModal');

    if (drop.addEventListener) {
        // drop.addEventListener('dragenter', handleDragover, false);
        //  drop.addEventListener('dragover', handleDragover, false);
        drop.addEventListener('drop', handleDrop, false);
    }


    function handleDrop(e) {
        e.stopPropagation();
        e.preventDefault();
        let files = e.dataTransfer.files;
        console.log(`dropped ${JSON.stringify(files[0].name)} on canvas, and will import data it recognizes`)
        let i, f;
        for (i = 0, f = files[i]; i != files.length; ++i) {
            let reader = new FileReader();
            let name = f.name;
            reader.onload = function (e) {
                let storeName = ""
                let data = new Uint8Array(e.target.result);
                let wb = XLSX.read(data, { type: 'array' });
                /* DO SOMETHING WITH wb HERE */
                wb.SheetNames.forEach((sht, i) => {
                    //  console.log(`Sheet #${i + 1}: ${sht}`);

                    storeName = "tbTags"
                    if (sht === "Tags") {
                        let ws = wb.Sheets[sht];
                        let jsondata = XLSX.utils.sheet_to_json(ws, { raw: false, defval: null })
                        clearStoreTable(storeName)
                        xlsxJsonDataPopulator(jsondata, storeName)
                    }
                    storeName = "tbResources"
                    if (sht === "Resources") {
                        let ws = wb.Sheets[sht];
                        let jsondata = XLSX.utils.sheet_to_json(ws, { raw: false, defval: null })
                        clearStoreTable(storeName)
                        xlsxJsonDataPopulator(jsondata, storeName)
                    }
                    storeName = "tbAdminPanel"
                    if (sht === "AdminPanel") {
                        let ws = wb.Sheets[sht];
                        let jsondata = XLSX.utils.sheet_to_json(ws, { raw: false, defval: null })
                        clearStoreTable(storeName)
                        xlsxJsonDataPopulator(jsondata, storeName)
                    }
                    storeName = "tbDocuments"
                    if (sht === "Documents") {
                        let ws = wb.Sheets[sht];
                        let jsondata = XLSX.utils.sheet_to_json(ws, { raw: false, defval: null })
                        clearStoreTable(storeName)
                        xlsxJsonDataPopulator(jsondata, storeName)
                    }
                });
                data = ""
                wb = ""
            };
            reader.readAsArrayBuffer(f);
        }
        files = ""
    }
} // end using initDropifyXlsx xlsx npm package import directly into idb from excel



// END XLSX drop component xlsx




// START univ form materialize javascript

$(document).on('click', '.newBlogItem', function (e) {
    // confirm("Are you sure, this will create a new Document. Must be unique Document Name")
    launchEmptyDocumentsForm()
});

function launchEmptyDocumentsForm() {

    let documentid = ""

    fillDocumentsForm(documentid)

    setTimeout(() => {

        var elem = document.querySelector('#launchDocumentsForm');
        var options = {
            dismissible: false
        }
        var instance = M.Modal.init(elem, options);
        instance.open();
        // clears the form so we can new data
        resetDocumentsInput()
        let editorData = $('#tbDocHTMLContent').html()
        getEditorDataContentMain(editorData)

    }, 200);

    // need to save document, and place tbdocid in the form uid field
    setTimeout(() => {
        let docname = `New Blog Article - ${new Date()}`
        createDocument(docname)

    }, 300);


    console.log("launchEmptyDocumentsForm finished")

}





$(document).on('click', '.btnSaveFormDataDocument', function (e) {

    // want to check if no uid, if true then we create new else save
    let tbdocid = $("#tbDocID").val()

    if (!tbdocid) {

        console.log("no id")
        createDocument()

    } else {
        console.log("is an id")
        saveFormDataDocument()
    }
});





// ///  START blog card launcher univ

$(document).on('click', '.launchBlogArticles', function () {

    $("#divTbPagesL1").empty()

    let targetDiv = '#divTbPagesL1'
    //  tbBlogFabHTMLComponent(targetDiv)
    // initTbBlogFab()

    drawBlogChips()

    setTimeout(() => {
        let l3Value = "xxx"
        launchTbBlogCardsL1New(l3Value)

        //  document.getElementById('newBlogItem').addEventListener('click', launchEmptyDocumentsForm);

    }, 200);
});


$(document).on('click', '.chip', function () {

    $("#divTbPagesL1").empty()

    let targetDiv = '#divTbPagesL1'
    blogFabHTMLComponent(targetDiv)
    initFabBlog()

    drawBlogChips()

    setTimeout(() => {
        var chipName = this.getAttribute('data-chipname')
        console.log("chip name: " + chipName)
        launchTbBlogCardsL1(chipName)
    }, 200);
});





function launchTbBlogCardsL1New(l3Value) {

    let targetDiv = '#divTbPagesL1'
    //  blogFabHTMLComponent(targetDiv)
    //  initFabBlog()
    // let l3Value = "Products"
    drawTbBlogCardsNew(l3Value)

}


// END Blog Card launching



// Start Read Only Page

$(document).on('click', '.launchTbBlogCardFormNew', function (e) {
    $('#launchDocumentsFormUserFacingNew').remove()
    let documentid = $(this).attr('data-tbid')
    console.log(" documentid: " + documentid)
    launchDocumentsFormUserFacingNew(documentid)
});


function launchDocumentsFormUserFacingNew(documentid) {

    console.log("launch Documents user facint Form: " + documentid)
    let targetDiv = '#divTbPagesL1'
    drawDocFormHTMLComponentUserFacingNew(targetDiv)
    // set data-tbid on form for later actions
    var myform = document.getElementById('launchDocFormUserFacing')
    var myform = myform.setAttribute('data-tbid', documentid)

    var elem = document.querySelector('#launchDocFormUserFacing');
    var options = {
        dismissible: true
    }
    var instance = M.Modal.init(elem, options);
    instance.open();

    setTimeout(() => {
        fillDocumentsFormFromBlogCardsNew(documentid)
    }, 200);
}


/// END Readonly Page 






/// START Launch Contact Mail form page

$(document).on('click', '.launchContactUs', function () {

    launchContactMailPage()

    displaySuccessMessageDB("Send us an email")

});


$(document).on('click', '.launchContactMailPage', function () {
    launchContactMailPage()
});


function launchContactMailPage() {
    // first remove any existing pages
    $("#divTbPagesL1").css("visibility", "visible");
    $("#divTbPagesL1").empty()

    let pageName = "Timebars Ltd. Mail Form"
    // console.log("launced home")
    let targetDiv = '#divTbPagesL1'
    let L1 = "Home"
    let L2 = "Mail Form"
    let L3 = ""
    // tbBreadCrumbHTMLComponent(targetDiv, pageName, L1, L2, L3)
    contactMailPageHTMLComponent(targetDiv)
}

/// END Launch Contact Mail form page




// START launch Blog home page


$(document).on('click', '.launchTbBlogHomePage', function () {

    launchBlogHomePage()
});


function launchBlogHomePage() {
    // first remove any existing pages
    $("#divTbPagesL1").css("visibility", "visible");
    $("#divTbPagesL1").empty()

    let pageName = "Blog Home Page"
    // console.log("launced home")
    let targetDiv = '#divTbPagesL1'
    let L1 = "Home"
    let L2 = ""
    let L3 = ""
    tbBlogBreadCrumbHTMLComponent(targetDiv, pageName, L1, L2, L3)

    targetDiv = '#divTbPagesL1'
    tbBlogHomePageHTMLComponent(targetDiv)

}

// END launch Blog home page



// START editor content field 
$(document).on('click', '#EditContentMain', function () {

    let editorData = $('#tbDocHTMLContent').html()
    // console.log("editorData " + editorData)
    getEditorDataContentMain(editorData)
});



$(document).on('click', '#closeEditorContentMain', function () {
    console.log("destroy editor")
    window.editorContentMain.destroy()
        .catch(error => {
            console.log(error);
        });
});


/* END Editor  */



/* START launch Grid std */

$(document).on('click', '.launchTbBlogGrid', function () {
    launchTbBlogGrid()
});

function launchTbBlogGrid() {
    $("#divTbPagesL1").css("visibility", "visible");
    $("#divTbPagesL1").empty()

    console.log(" here launchTbBlogGrid")
    let targetDiv = '#divTbPagesL1'
    //tbBlogFabHTMLComponent(targetDiv)
    // initTbBlogFab()
    drawTbBlogStandard()

}

function drawTbBlogStandard() {

    setTimeout(() => {
        let targetDiv = '#divTbPagesL1'
        dtBlogTableHeaderHTMLComponent(targetDiv)

    }, 100);

    setTimeout(() => {
        //   console.log("page load get Document")
        getDocuments()
    }, 200);

    setTimeout(() => {
        //   console.log("page load get Document")
        initDtBlogStandard()

        let dropdiv = 'divTbPagesL1'
        //  handleJSONFileDropforUpload(dropdiv)

        //   document.getElementById('btnkkk').addEventListener('click', launchEmptyDocumentsForm);
        // document.getElementById('newBlogItem').addEventListener('click', launchEmptyDocumentsForm);

    }, 400);
};

function initDtBlogStandard() {
    $('#dataTableDocuments').DataTable({
        // "dom": '<"top"i>rt<"bottom"flp><"clear">',  // f = search 
        "dom": '<"top"f>rt<"bottom"Bilp>',
        // dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf'
        ],
        select: true,
        colReorder: true,  // change what columns show first, second etc.
        responsive: true,
        fixedHeader: true,
        destroy: true,

    });

};

/* END launch Grid std */



/* START mctt tagging */

// V1 new make tag table by clicking on field adds support for multi forms by adding suffix to id
$(document).on('click', '.mcttUpdatablev1', function (ui) {

    // get form name, then we can get the tbid
    var formId = String($(this).attr('data-formkey')).split('-')
    var sourceclass = formId[0]
    var formName = formId[1]
    var tagGroup = formId[2]
    var fieldName = $(this).attr('Id')
    // use this for positioning mctt near point of click
    var leftpx = Number($(this).offset().left)
    var toppx = Number($(this).offset().top)

    // console.log("fieldName " + fieldName)

    // this is standard pattern how to get current bar id (data-tbid value is set when form opens)
    if (formName === 'tbform') {
        var mybar = document.getElementById('launchTimebarsForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'mdform') {
        var mybar = document.getElementById('launchMetadatasForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'resform') {
        var mybar = document.getElementById('launchResourcesForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'docform') {
        var mybar = document.getElementById('launchDocumentsForm')
        var tbID = mybar.getAttribute('data-tbid')

    } else if (formName === 'tagform') {
        var mybar = document.getElementById('launchTagsForm')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === 'statusForm') {
        var mybar = document.getElementById('statusFormIndicatorTable')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === '2colMdPup') {
        var mybar = document.getElementById('tblTaskMetaDataPopupOffBar')
        var tbID = mybar.getAttribute('data-tbid')
    } else if (formName === 'mdAdv') {
        var mybar = document.getElementById('mdFormAdvL1')
        var tbID = mybar.getAttribute('data-tbid')
    }

    // dont want modal mctt on mdAdv form.
    if (formName === "mdAdv") {

        document.getElementById("mcttContainer").style.visibility = "visible";

        // if user scrolls down, this will ensure the form pops up in viewable area.
        var lastScrollY = window.pageYOffset;
        var lastScrollX = window.pageXOffset;

        toppx = (toppx - 200) - Number(lastScrollY)
        leftpx = (leftpx - 270) - Number(lastScrollX)
        leftpx = leftpx + "px"
        toppx = toppx + "px"

        console.log("lastScrollY " + lastScrollY)
        console.log("toppx " + toppx)

        makeMcttMetaDataOrRes(tagGroup, tbID, formName, fieldName, leftpx, toppx, sourceclass)

    } else {

        document.getElementById("mcttContainer").style.visibility = "visible";

        leftpx = leftpx + 100
        leftpx = leftpx + "px"
        toppx = ""
        makeMcttMetaDataOrRes(tagGroup, tbID, formName, fieldName, leftpx, toppx, sourceclass)

        // make the mctt a modal
        var elem = document.querySelector('#mcttContainer');
        var options = {
            dismissible: false
        }
        var instance = M.Modal.init(elem, options);
        instance.open();
    }

});

// step 2 draw or make mctt MultiColumnTagTable Table,see focdblog.js




// step 4b legacy same as 4a but dblclick to close mctt **** update UI and NOT IDB *****
$(document).on('dblclick', '.mcttTapRow', function (e) {

    var val = $(this).attr('id');
    var arrval = val.split('-')
    var tbID = $("#tbid").val()
    var fieldName = arrval[1]
    var tagValue = e.target.innerHTML
    var fieldToUpate = "#" + fieldName

    $(fieldToUpate).val(tagValue);

    M.updateTextFields()
    closeMultiColumnTagTable()
});


// step 4 V1 update mctt tag table fields, update UI and IDB with vaulues fixed to deal with V1
$(document).on('click', '.mcttTapRow', function (e) {
    // var tagValueOrig = document.getElementById("tbMDSize").innerText

    var val = $(this).attr('id');
    var arrval = val.split('-')
    var tbID = arrval[0]
    var fieldName = arrval[1]
    var tagValue = e.target.innerHTML

    // strip off the field name suffix so we can updated IDB
    fieldName = fieldName.split('_')
    let fieldNamesuffix = fieldName[1]
    fieldName = fieldName[0]

    console.log("val " + val)
    console.log("fieldName " + fieldName)
    console.log("tagValue " + tagValue)

    //  console.log("updateOneDocumentField " + fieldName)

    updateOneDocumentField(tbID, fieldName, tagValue)

    // mctt legacy has no "_" in field name, need logic to deal with it
    if (!fieldNamesuffix) {
        // console.log("fieldNamesuffix with no _: " + fieldNamesuffix)
        var fieldToUpate = "#" + fieldName
    } else {

        var fieldToUpate = "#" + fieldName + "_" + fieldNamesuffix
    }
    // console.log("fieldToUpate " + fieldToUpate)

    document.querySelector(fieldToUpate).innerHTML = tagValue

});


// update non tag table fields idb and form when user moves off editable field
$(document).on('focusout', '.updatableStd', function (e) {
    // get form name, then we can get the tbid
    var formId = String($(this).attr('data-formkey')).split('-')
    var formName = formId[1]
    var val = $(this).attr('id');
    console.log("val:" + val)

    // strip off the field name suffix so we can updated IDB
    let fieldName = val.split('_')
    let fieldNamesuffix = fieldName[1]
    fieldName = fieldName[0]
    let fieldid = "#" + fieldName + "_" + fieldNamesuffix
    // get the text from field
    var theText = $(fieldid).val();

    console.log("theText " + theText)

    UpdatableStd(formName, fieldName, theText)


});


// V1 close modal mctt 
$(document).on('click', '#btnCloseMultiColumnTagTable', function (e) {

    closeMultiColumnTagTable()
});
function closeMultiColumnTagTable() {

    var elem = document.querySelector('#mcttContainer');
    var options = {
        dismissible: false
    }
    var instance = M.Modal.getInstance(elem, options);

    if (instance) {

        instance.close();

    } else {

        $("#multiColumnTagTable").remove()
        $("#mcttTagListDiv").remove()
        document.getElementById("mcttContainer").style.visibility = "hidden";
    }

}


/* END mctt tagging */